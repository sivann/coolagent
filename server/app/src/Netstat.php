<?php

/*

$remote_ip = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
$remote_port = $_SERVER['REMOTE_PORT'];

echo "<pre>";
echo "$remote_ip:$remote_port <br>\n";

$starttime = microtime(true);
$ni = new Netstat(0);
$connection_status = $ni->getStatus($remote_ip,$remote_port);
$endtime = microtime(true);
$timediff = $endtime - $starttime;

echo "Timediff:$timediff\n";

echo "Status:$connection_status";


*/

namespace App;

Class Netstat {
	private $want_external; //use external netstat to acquire connection info

    public function __construct($want_external = false) {

        $this->want_external = $want_external;
    }

	public function getStatus($remote_ip,$remote_port) {
		if (! $this->want_external ) {
			$connection_status = $this->getIPPortStatus4($remote_ip , $remote_port).$this->getIPPortStatus6($remote_ip , $remote_port);
		}
		else {
			$connection_status = $this->getIPPortStatusExt($remote_ip , $remote_port);
		}

		if (strlen($connection_status))
			return $connection_status;
		else
            return "NOTFOUND";
	}


	public function getIPPortStatusExt($remote_ip,$remote_port) {
            $cmd="netstat -tn | fgrep ' $remote_ip:$remote_port '";

            $pfp=popen($cmd,"r");
            if(!$pfp) {
				$e = error_get_last();
                return $e['message'];
            }

            $buf = fgets($pfp, 1024);
            pclose($pfp);

            $buf=preg_replace('!\s+!', ' ', $buf); //remove multiple spaces
            $buf=trim($buf);

            $buf_r=explode(" ",$buf);

            if (count($buf_r)) {
                    $state=$buf_r[count($buf_r)-1];
                    return trim($state);
            }

            return "";

	}

	public function getIPPortStatus4($ip,$port) {
		$state_name=array(
			'01' => 'ESTABLISHED',
			'02' => 'SYN_SENT',
			'03' => 'SYN_RECV',
			'04' => 'FIN_WAIT1',
			'05' => 'FIN_WAIT2',
			'06' => 'TIME_WAIT',
			'07' => 'CLOSE',
			'08' => 'CLOSE_WAIT',
			'09' => 'LAST_ACK',
			'0A' => 'LISTEN',
			'0B' => 'CLOSING'
		);

		$ptcp=file("/proc/net/tcp");
		$nrows=count($ptcp);

		for ($i=1;$i<$nrows;$i++) {
			$r=$ptcp[$i];
			$r=preg_replace('!\s+!', ' ', $r); //remove multiple spaces
			$row=explode(" ",$r);

			$local=explode(":",$row[2]);
			$local_addr=$local[0];
			$local_port=$local[1];
			$local_port_dec=hexdec($local_port);
			$local_addr_ip=$this->hex2ip($local_addr);

			$remote=explode(":",$row[3]);
			$remote_addr=trim($remote[0]);
			$remote_port=trim($remote[1]);
			$remote_port_dec=hexdec($remote_port);
			$remote_addr_ip=$this->hex2ip($remote_addr);

			$state=$state_name[$row[4]];

			//echo ("local: $local_addr_ip : $local_port_dec  remote: $remote_addr_ip : $remote_port_dec $state\n");
			if ($remote_port_dec == $port && $remote_addr ==  $ip ) {
				//echo ("FOUND remote: $remote_addr_ip : $remote_port_dec : $state\n");
				return $state;
			}

		}
		return "";

	}

	public function getIPPortStatus6($ip,$port) {
		$state_name=array(
			'01' => 'ESTABLISHED',
			'02' => 'SYN_SENT',
			'03' => 'SYN_RECV',
			'04' => 'FIN_WAIT1',
			'05' => 'FIN_WAIT2',
			'06' => 'TIME_WAIT',
			'07' => 'CLOSE',
			'08' => 'CLOSE_WAIT',
			'09' => 'LAST_ACK',
			'0A' => 'LISTEN',
			'0B' => 'CLOSING'
		);

		$ptcp=file("/proc/net/tcp6");
		$nrows=count($ptcp);

		for ($i=1;$i<$nrows;$i++) {
			$r=$ptcp[$i];
			$r=preg_replace('!\s+!', ' ', $r); //remove multiple spaces
			$row=explode(" ",$r);

			$remote=explode(":",$row[3]);
			$remote_addr=$remote[0];
			$remote_port=trim($remote[1]);
			$remote_port_dec=hexdec($remote_port);

			$remote_addr_ip=$this->hex2ip6($remote_addr);

			$state=$state_name[$row[4]];

			//echo ("remote: $remote_addr_ip : $remote_port_dec : $state [ip: $ip || ::ffff:$ip] \n");
			if ($remote_port_dec == $port && ($remote_addr_ip ==  $ip || $remote_addr_ip == "::ffff:".$ip )) {
				//echo ("FOUND remote: $remote_addr_ip : $remote_port_dec : $state\n");
				return $state;
			}

		}
		return "";

	}


	private function hex2ip($str) {
		return hexdec (substr($str,6,2)).".".
			hexdec (substr($str,4,2)).".".
			hexdec (substr($str,2,2)).".".
			hexdec (substr($str,0,2));
	}

	//parse ip6 address part of /proc/net/tcp6
	private function hex2ip6($ipex) {
		$in_addr=array();

		if (sscanf($ipex,
			"%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x%2x",
			$in_addr[3], $in_addr[2], $in_addr[1], $in_addr[0],
			$in_addr[7], $in_addr[6], $in_addr[5], $in_addr[4],
			$in_addr[11], $in_addr[10], $in_addr[9], $in_addr[8],
			$in_addr[15], $in_addr[14], $in_addr[13], $in_addr[12]) == 16)
		{

			ksort($in_addr);
			//$ip6=implode(".",($in_addr));

			$ia='';
			for ($i=0;$i<16;$i++)
				$ia.=chr($in_addr[$i]);

			$ip6=inet_ntop($ia);
		
			return $ip6;
		}
	}

}

?>
