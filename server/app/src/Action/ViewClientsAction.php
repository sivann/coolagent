<?php
// NOT USED RIGHT NOW

namespace App\Action;

use Slim\Views\Twig;
use Psr\Log\LoggerInterface;

final class ViewClientsAction
{
    private $view;
    private $logger;

    public function __construct(Twig $view, LoggerInterface $logger, $db)
    {
        $this->view = $view;
        $this->logger = $logger;
		$this->db = $db;
    }

    public function dispatch($request, $response, $args)
    {
        $this->logger->info("ViewClientsAction page action dispatched");
        
        $this->view->render($response, 'viewClients.twig');
		//$this->render('viewClients.php');
        return $response;
    }
}
