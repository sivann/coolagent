<?php

namespace App\Model;

//use Slim\Views\Twig;
use Psr\Log\LoggerInterface;

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;


class Agent {

	private $db;
	private $logger;
	private $agent_id;
	private $settings;
	private $ns;

    public function __construct(LoggerInterface $logger,$db,$settings) {
		$this->logger = $logger;
		$this->db = $db;
		$this->settings = $settings;
		$this->netstat = new \App\Netstat(false);

    }


	/*
	 * Functions to be called by the agent (return X Y Z -type responses)
	 */


	//register a new agent: assign him a new random password and remember it
	//if the agent already has a password, return an error (already registered)
	//if initial==false, re-register a previous registered client which was unregistered (with the unregister call)
	public function register($agent_id,$queryParams,$ip,$initial=true) {
		$this->agent_id=$agent_id;

		if ($this->validateAgentID($agent_id) <0) 
			return "E 0 0\nInvalid agent_id: ($agent_id)";

		$this->logger->debug("Agent::register: agent_id:$agent_id , ip:$ip, initial:$initial");


		//check if already registered
		$password=$this->getPasswordByAgentID($agent_id);

		if (strlen($password)) {
			//this could happen legitimately if agent asks registration and then doesn't wait for response
			$this->logger->error("Agent::register: agent asked for registration by seems already registered (password not empty) agent_id: $agent_id");
			return "E 0 0\nAlready registered, $agent_id";
		}


		//requested getNextCommand but server password was null (probably after 'unregister'), so re-register
		if ($initial)
			$this->fullUpdate($agent_id,$queryParams['agent_info'],$ip);


		$rnd=bin2hex(openssl_random_pseudo_bytes (14)); //generate random password

		$this->logger->info("Agent::register: registering agent, agent_id: $agent_id");

		$sql="UPDATE agent SET password=:password, time_registered=:time_registered WHERE agent_id=:agent_id";
		$this->db->execute($sql,array (
				'agent_id' => $agent_id,
				'password' => $rnd,
				'time_registered' => time(),
				)
		);

		$cmd="R 0 0\n$rnd"; //Register password on next line

		return $cmd;
	}

	//returns password created on registration for this agent
	private function getPasswordByAgentID($agent_id) {
		$sql="SELECT password FROM agent WHERE agent_id=:agent_id ";
		$stmt=$this->db->execute($sql,array('agent_id'=>$agent_id));
		$row=$stmt->fetch(\PDO::FETCH_ASSOC);
		return $row['password'];
	}

	//check if agent_id password matches the DB 
	private function verifyAgentPassword($agent_id,$agent_password) {
		$registered_password=$this->getPasswordByAgentID($agent_id);

		$this->logger->debug("Agent::verifyAgentPassword: agent_id:$agent_id , agent_password:$agent_password, registered_password:$registered_password");

		if ($agent_password != $registered_password) {
			$this->logger->error("Agent::verifyAgentPassword: agent_id:$agent_id , invalid password ($agent_password)");
			return -1;
		}
		return 0;
	}


	//check if agent_id contains valid characters. At least remove chars which need html encoding.
	private function validateAgentID($agent_id) {
		$agent_id=preg_replace("/[^a-zA-Z0-9_@\.#:^-]/", "", trim($agent_id));
		if ($agent_id != $this->agent_id) {
			$this->logger->error("Agent::validateAgentID: invalid agent_id: sanitized ($agent_id) != ({$this->agent_id}");
			return -1;
		}
		return 0;
	}

	private function agentExists($agent_id) {
		$sql="SELECT agent_id FROM agent WHERE agent_id=:agent_id ";
		$stmt=$this->db->execute($sql,array('agent_id'=>$agent_id));
		$row=$stmt->fetch(\PDO::FETCH_ASSOC);

		$this->logger->debug("Agent::agentExists: agent_id:$agent_id, agent_info:".print_r($row,true));

		if (isset($row['agent_id']))
			return true;
		else
			return false;
	}

	//add agent if missing
	private function addAgent($agent_id) {
		$sql="INSERT OR IGNORE INTO agent (agent_id) VALUES (?)";
		$this->db->execute($sql,array ($agent_id));
	}

	//perform a full update, i.e. one containing all agent info
	private function fullUpdate($agent_id,$agent_info,$ip) {
		// make sure agent_id exists before updating
		// add new agent only on full updates
		$this->addAgent($agent_id);

		$agent_info_json=base64_decode($agent_info);
		$agent_info=json_decode($agent_info_json,true);

		$this->logger->info("Agent::fullUpdate: agent_id:$agent_id, agent_info:".print_r($agent_info,true));

		$sql="UPDATE agent SET last_poll_ts=:last_poll_ts, poll_period=:poll_period, longpoll_period=:longpoll_period, ".
			 "info=:info, last_ip=:last_ip WHERE agent_id=:agent_id";
		$this->db->execute($sql,array (
				'agent_id' => $agent_id,
				'last_poll_ts' => time(),
				'poll_period' => $agent_info['poll_period'],
				'longpoll_period' => $agent_info['longpoll_period'],
				'info' => $agent_info_json,
				'last_ip' => $ip,
				)
		);
	}

	private function getRequestPassword() {
		return $_COOKIE["password"];
	}

	/*
	 - add new agent if not exists
	 - update agent info
	 - return pending commands for this agent
	*/
	public function getNextCommand($agent_id,$queryParams,$ip) {
		$this->agent_id=$agent_id;

		if ($this->validateAgentID($agent_id) <0) 
			return "E 0 0\nInvalid agent_id string";


		/* agent does not exit. Can happen if:
		   somebody deleted the agent from the server while it was running
		   the server was down when the agent started for the 1st time
		 */
		if (!$this->agentExists($agent_id)) {
			$this->logger->info("Agent::getNextCommand:Agent does not exist, adding. Agent:".$agent_id);
			$this->addAgent($agent_id);
			//The agent is now unregistered, and will register below. Instruct agent to do a fullupdate next
		}

		if ($this->verifyAgentPassword($agent_id,$this->getRequestPassword()) < 0)  {
			if (!strlen($this->getPasswordByAgentID($agent_id))) { //somebody unregistered the agent
				$this->logger->info("Agent::getNextCommand: agent supplied invalid password, but server has null password. ".
				                    "Trying to re-register. agent_id:$agent_id, agent_info:".print_r($queryParams['agent_info'],true));
				$cmd = $this->register($agent_id,array(),$ip,false) ; //ask agent to register, initial==false
				return $cmd;
			}
			else {
				$this->logger->error("Agent::getNextCommand:Returning 'Incorrect registration password' agent_id:$agent_id, agent_info:".print_r($queryParams['agent_info'],true));
				return "E 0 0\nIncorrect registration password:{$this->getRequestPassword()}";
			}
		}

		// update the data -- full update
		if (isset($queryParams['agent_info'])) {
			$this->fullUpdate($agent_id,$queryParams['agent_info'],$ip);
			$this->logger->info("Agent::getNextCommand:fullupdate: agent_id:$agent_id, agent_info:".print_r($queryParams['agent_info'],true));
		}
		//short update, just timestamp,ip
		else { 
			$this->logger->debug("Agent::getNextCommand:short update ".$agent_id);
			$sql="UPDATE agent SET last_poll_ts=:last_poll_ts, last_ip=:last_ip WHERE agent_id=:agent_id";
			$this->db->execute($sql,array (
					'agent_id' => $agent_id,
					'last_poll_ts' => time(),
					'last_ip' => $ip,
					)
			);
		}

		// LONGPOLL
		$longpoll_period=$this->getLongpollperiodByAgentID($agent_id);

		//Loop and check for new commands
		session_write_close();
		set_time_limit(3600);

		if ($longpoll_period) {
			//ignore_user_abort(false); //doesn't work
			$loopsleep=$this->settings['timers']['db_poll_delay'];

			if (!is_numeric($loopsleep)) {
				$this->logger->error("Agent::getNextCommand:db_poll_delay not set($loopsleep) ".print_r($this->settings,true));
				$loopsleep=5;
			}

			for ( $loopcounter=0 ; $loopcounter<($longpoll_period/$loopsleep) ; $loopcounter++ ) {
				$this->logger->debug("Agent::getNextCommand:internal DB long poll loop for agent_id: ".$agent_id." loopcount:".$loopcounter."/".($longpoll_period/$loopsleep));
				//resend unreturned commands (in case coolagent crashed/restarted before returning)
				$commands=$this->getPendingSentCommands($agent_id);

				//if no pending sent, send a new comand
				if (!count($commands))
					$commands=$this->getNewCommands($agent_id);

				if (count($commands)) 
					break;

				if (connection_status()!=0) {
					$this->logger->warning("getNextCommand:Connection aborted , agent_id: ".$agent_id." loopcount:".$loopcounter);
					break;
				}
				sleep($loopsleep);

				$con_status=$this->getConnectionStatus();
				if ($con_status != "ESTABLISHED") {
					$this->logger->warning("getNextCommand:Connection lost, ($con_status) , agent_id: ".$agent_id." loopcount:".$loopcounter);
					break;
				}
			}
		}
		else {
			// No Longpoll
			//resend unreturned commands (in case coolagent crashed/restarted before returning) (what about cmd still running?)
			$commands=$this->getPendingSentCommands($agent_id);

			//if no pending sent, send a new comand
			if (!count($commands))
				$commands=$this->getNewCommands($agent_id);
		}

		if (!count($commands))
			return "N 0 0\n"; //NOOP

		/*just return the oldest command to the agent */
		$commands0=$commands[0];
		/*
		$agent_info=$this->getAgentInfo($agent_id);
		$commands0=replaceCommandPlaceholders($commands0,$agent_info);
		*/

		$txt=$this->command2txt($commands0);
		$this->markCommunicated($commands0['id'],$agent_id);
		$this->logger->info("Agent::getNextCommand:agent_id: ".$agent_id.", sent command:($txt)");
		return $txt;
	}

	//return json encoded info column
	private function getAgentInfo($agent_id) {
		$sql="SELECT info FROM agent WHERE agent_id=:agent_id ";
		$stmt=$this->db->execute($sql,array('agent_id'=>$agent_id));
		$row=$stmt->fetch(\PDO::FETCH_ASSOC);
		$info = $row['info'];
		$info_r = json_decode($info,true);
		return $info_r;
	}

	private function getLongpollperiodByAgentID($agent_id) {
		$sql="SELECT longpoll_period FROM agent WHERE agent_id=:agent_id ";
		$stmt=$this->db->execute($sql,array('agent_id'=>$agent_id));
		$row=$stmt->fetch(\PDO::FETCH_ASSOC);

		$longpoll_period=$row['longpoll_period'];
		if (empty($longpoll_period))
			$longpoll_period=0;
		return $longpoll_period;
	}

    /* checks if remote end has closed its TCP connection or not */
    private function getConnectionStatus() {
            $remote_ip = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
            $remote_port=$_SERVER['REMOTE_PORT'];

			$connection_status = $this->netstat->getStatus($remote_ip,$remote_port);

			$this->logger->debug("Agent::getConnectionStatus:[".getmypid()."]($remote_ip:$remote_port->$connection_status");
			return $connection_status;

			/*
            $cmd="netstat -tn | fgrep ' $remote_ip:$remote_port '";

            $pfp=popen($cmd,"r");
			if(!$pfp) {
				$this->logger->error("getConnectionStatus: $cmd");
			}

            $buf = fgets($pfp, 1024);
            pclose($pfp);

            $buf=preg_replace('!\s+!', ' ', $buf); //remove multiple spaces
            $buf=trim($buf);

            $buf_r=explode(" ",$buf);

            if (count($buf_r)) {
                    $state=$buf_r[count($buf_r)-1];
                    return trim($state);
            }

            return "NOTFOUND";
			*/
    }


	//sets tunnel the port bound by ssh returned from ssh -R 0:...
	public function setTunnelPort($agent_id,$command_id,$params) {
		$data=$params['data'];
		$exit_status=$params['exitstatus'];

		$this->logger->info("Agent::setTunnelPort:agent_id:$agent_id, command_id:$command_id, data:$data");

		$sql="UPDATE command ".
		     "SET tunnel_port=:tunnel_port, tunnel_active=:tunnel_active ".
		     "WHERE id=:command_id AND agent_id=:agent_id";

		$this->db->execute($sql,array (
				'command_id' => $command_id,
				'agent_id' => $agent_id,
				'tunnel_port' => trim($data),
				'tunnel_active' => true,
				)
		);

	}


	public function setCommandResult($agent_id,$command_id,$params) {
		$data=$params['data'];
		$exit_status=$params['exitstatus'];

		$this->logger->info("Agent::setCommandResult:agent_id:$agent_id, command_id:$command_id, params:".print_r($params,true)); 

		if ($this->verifyAgentPassword($agent_id,$this->getRequestPassword()) <0) 
			return -1;

		$sql="UPDATE command SET time_returned=:time_returned, ".
			 "output=:output, exit_status=:exit_status, tunnel_active=:tunnel_active ".
		     " WHERE id=:command_id AND agent_id=:agent_id";

		$this->db->execute($sql,array (
				'command_id' => $command_id,
				'agent_id' => $agent_id,
				'time_returned' => time(),
				'output' => $data,
				'exit_status' => $exit_status,
				'tunnel_active' => False, //tunnel has now finished
				)
		);

	}

	public function markCommunicated($command_id,$agent_id) {
		$sql="UPDATE command SET time_communicated=:time_communicated WHERE id=:command_id AND agent_id=:agent_id";
		$this->db->execute($sql,array (
				'command_id' => $command_id,
				'agent_id' => $agent_id,
				'time_communicated' => time(),
				)
		);
	}

	//converts a command table row to a command that coolagent understands
	public function command2txt($command) {
		$exectype=strtoupper($command['exec_type'][0]);

		$txt="$exectype {$command['timeout']} {$command['id']}\n".
			 $command['cmd']."\n";

		return $txt;

	}

	/*
	 * Functions to be called by the server (return  json)
	 */


    public function getAgents() {
        $this->logger->debug("Agent::getAgents");
        // make sure it exists before updating
        $sql="SELECT agent_id, info, time_registered, last_ip, longpoll_period, poll_period, (strftime('%s','now') - last_poll_ts - longpoll_period) as age_sec FROM agent";
        $stmt=$this->db->execute($sql);
        $rows=$stmt->fetchAll(\PDO::FETCH_ASSOC);
		/* convert info json string to array */
		foreach ($rows  as $k=>&$v) {
			if (strlen($v['info']));
				$v['info'] = json_decode($v['info'],true);

		}

        return $rows;
    }


	//get communicated but not returned commands
	//ignore commands that have not timed-out yet (command may be still running).
	public function getPendingSentCommands($agent_id) {
		$now=time(0);

		$sql="SELECT exec_type,id,timeout,cmd FROM command ".
			 "WHERE agent_id=:agent_id AND time_returned is null ".
             " AND time_communicated is not null ".
             " AND ($now - time_communicated) > (timeout + 45) ".
			 " ORDER by time_requested ASC";

		$stmt=$this->db->execute($sql,array('agent_id'=>$agent_id));
		$rows=$stmt->fetchAll(\PDO::FETCH_ASSOC);
		$this->logger->debug("Agent::getPendingSentCommands:agent_id:$agent_id, ncommands:".count($rows));
		return $rows;
	}


	//get not communicated commands (queried by agent on start)
	public function getNewCommands($agent_id) {

		$sql="SELECT exec_type,id,timeout,cmd FROM command WHERE agent_id=:agent_id ".
             " AND time_communicated is null ORDER by time_requested ASC";
		$stmt=$this->db->execute($sql,array('agent_id'=>$agent_id));
		$rows=$stmt->fetchAll(\PDO::FETCH_ASSOC);
		$this->logger->debug("Agent::getNewCommands:agent_id:$agent_id, ncommands:".count($rows));
		return $rows;
	}


	//delete agent
	public function deleteAgent($agent_id) {
			$this->logger->debug("Agent::deleteAgent:id:".$agent_id);

			if (strlen($agent_id)) {
					$this->logger->error("Agent::deleteAgent:agent_id:(".$agent_id.") is too short");
			}
			$sql="DELETE from agent where agent_id=:agent_id";
			$stmt=$this->db->execute($sql,array('agent_id'=>$agent_id));
	}

	//unregister agent
	public function unregister($agent_id,$ip) {
		$this->agent_id=$agent_id;

		if ($this->validateAgentID($agent_id) <0) 
			return array('status'=>'error','message'=>"Could not validate agent_id $agent_id, check logs");

		//check if already unregistered
		$password=$this->getPasswordByAgentID($agent_id);

		if (!strlen($password)) {
			$msg="Agent::unregister: agent asked for unregistration by seems not registered (password empty) agent_id: $agent_id";
			$this->logger->error($msg);
			return array('status'=>'error','message'=>$msg);
		}

		$sql="UPDATE agent SET password=:password, time_registered=:time_registered WHERE agent_id=:agent_id";
		$this->db->execute($sql,array (
				'agent_id' => $agent_id,
				'password' => '',
				'time_registered' => time(),
				)
		);

		$msg="Agent::unregister: unregistration success for agent_id: $agent_id";
		$this->logger->error($msg);

		return array('status'=>'success','message'=>$msg);
	}

	public function getStats($agent_id,$queryParams) {
		
		$date_start="end-10d";
		$date_end="now";
		$resolution_sec="1800";

		if (!empty($queryParams['date_start']))
			$date_start=$queryParams['date_start'];
		if (!empty($queryParams['date_end']))
			$date_end=$queryParams['date_end'];
		if (!empty($queryParams['resolution_sec']))
			$resolution_sec=$queryParams['resolution_sec'];

		$this->logger->debug("Agent::getStats:id:".$agent_id);
		$rrd_stats_dir = $this->settings['rrd']['stats_dir'];
		$rrd_fn = $rrd_stats_dir."/".$agent_id.".rrd";

		if (!file_exists($rrd_fn)) {
			$msg="Agent::getStats: rrd file not found: : $rrd_fn";
			$this->logger->error($msg);
			return array('status'=>'error','message'=>$msg);
		}

		//http://oss.oetiker.ch/rrdtool/doc/rrdfetch.en.html
		//$result = rrd_fetch($rrd_fn, array( "AVERAGE", "--start", "end-7d","--resolution",1800*1));
		$v=rrd_version();
		if (version_compare($v,"1.5.0") <0) 
			$result = rrd_fetch($rrd_fn, array( 
				"AVERAGE", 
				"--start", $date_start,
				"--end",$date_end,
				"--resolution",$resolution_sec));
		else
			$result = rrd_fetch($rrd_fn, array( 
				"AVERAGE", 
				"--align-start", 
				"--start", $date_start,
				"--end",$date_end,
				"--resolution",$resolution_sec));

		//create the seqvals for sparklines which just want a 0 or 1, no timestamp
		foreach ($result['data']['isup'] as $k=>&$v) {
			if (0 ==  $v) $v=-1;
			if (is_nan ($v)) $v=0;
		}
		$seqvals=array_values($result['data']['isup']);
		$result['seqvalues']=$seqvals;

		return $result;
	}


    //return status (age) for all agents +geoip information
    public function getAgentsGeoStatus() {
        $this->logger->debug("Server::getAgentsGeoStatus");

        $sql="SELECT ".
        "agent_id, (strftime('%s','now') - last_poll_ts - longpoll_period)/(60 * 60 * 24 * 1) as age_days, last_ip ".
        " FROM agent ORDER BY age_days DESC";

        $stmt=$this->db->execute($sql);
        $rows=array();
        while (($row = $stmt->fetch(\PDO::FETCH_ASSOC)) !== false) {
            $t=$this->ip2geo($row['last_ip']);
            //$t=$this->ip2geo('8.8.8.8'); //for testing

            $row['latitude'] = isset($t->location->latitude)?$t->location->latitude:'';
            $row['longitude'] = isset($t->location->longitude)?$t->location->longitude:'';
            $row['city'] = isset($t->city->name)?$t->city->name:'';
            $row['country'] = isset($t->country->name)?$t->country->name:'';
            $row['subdivision'] =  isset($t->mostSpecificSubdivision->name)?$t->mostSpecificSubdivision->name:'';
            $rows[]=$row;
        }

        return $rows;
    }


    public function getAgentsStatus() {
        $this->logger->debug("Server::getAgentsGeoStatus");

        $sql="SELECT ".
        "agent_id, (strftime('%s','now') - last_poll_ts - longpoll_period)/(60 * 60 * 1) as age_hours, last_ip ".
        " FROM agent ORDER BY age_hours DESC";

        $stmt=$this->db->execute($sql);
        $rows=$stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $rows;
    }


    /* Docs: http://maxmind.github.io/GeoIP2-php/ */
    /* Class Docs: http://maxmind.github.io/GeoIP2-php/doc/v2.3.1/class-GeoIp2.Model.City.html */
    /* PHAR: https://github.com/maxmind/GeoIP2-php/releases */
    /* GeoIP Lite DB: http://dev.maxmind.com/geoip/geoip2/geolite2/ */

    public function ip2Geo($ip) {
        include_once $this->settings['geoip']['phar'] ;


        $reader = new Reader($this->settings['geoip']['db']);
        try {
            $t = $reader->city($ip);
            return $t;
        } catch (AddressNotFoundException $e) {
            $this->logger->error("Server::ip2geo:$ip:".print_r($e,true));
        }

    }



}
?>
