<?php

/* Commands and tunnels */

namespace App\Model;

//use Slim\Views\Twig;
use Psr\Log\LoggerInterface;

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;

class Command {

	private $db;
	private $logger;
	private $settings;

    public function __construct(LoggerInterface $logger,$db,$settings) {
		$this->logger = $logger;
		$this->db = $db;
		$this->settings = $settings;
    }
 
	public function getPendingCommands() {
		$this->logger->debug("Command::getPendingCommands");

		$sql="SELECT id, agent_id, cmd,time_requested, time_communicated, type ".
		"FROM command WHERE time_returned IS NULL";

		$stmt=$this->db->execute($sql);
		$rows=$stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $rows;

	}

	public function deleteCommand($command_id) {
		$this->logger->debug("Command::deleteCommand:id:".$command_id);
		$cmd = $this->getCommandByID($command_id) ;
		if ($cmd['type'] == 'tunnel') {
			return $this->cancelTunnel($command_id);
		}
		else
			return $this->cancelPendingCommand($command_id);
	}

	public function cancelPendingCommand($command_id) {
		$this->logger->debug("Command::cancelPendingCommand:id:".$command_id);

		if (!is_numeric($command_id)) {
			$this->logger->error("Command::cancelPendingCommand:id:(".$command_id.") is not numeric");
		}
		$sql="DELETE from  command where id=:id AND time_communicated is null";
		$stmt=$this->db->execute($sql,array('id'=>$command_id));

		return array('status'=>'ok','msg'=>'deleted');
	}


	/* - check if tunnel is active, 
	   - call external command to kill sshd associated with port 
	   - if it returns "ok", update DB and set tunnel_active to 0
     */
	public function cancelTunnel($command_id) {
		$this->logger->debug("Command::cancelTunnnel:cmd_id:".$command_id);

		if (!is_numeric($command_id)) {
			$this->logger->error("Command::cancelTunnel:cmd_id:(".$command_id.") is not numeric");
		}

		$sql="SELECT tunnel_port FROM command WHERE type='tunnel' AND tunnel_active=1 AND time_returned IS NULL and id=:command_id";
		$stmt=$this->db->execute($sql,array('command_id'=>$command_id));
		$row=$stmt->fetch(\PDO::FETCH_ASSOC);

		if (!is_numeric($row['tunnel_port'])) {
			$this->logger->error("Command::cancelTunnel:cmd_id:(".$command_id."), tunnel_port is not numeric");
		}

		$tunnel_port=$row['tunnel_port'];
		

		$cancel_tunnel_cmd=$this->settings['network']['cancel_tunnel_command'];

		$cancel_tunnel_result = `$cancel_tunnel_cmd -p $tunnel_port`;
		$this->logger->info("Command::cancelTunnel:cmd_id:(".$command_id."), cancel returned:".$cancel_tunnel_result);

		if ($cancel_tunnel_result == 'ok') {
			$this->logger->info("Command::cancelTunnel:cmd_id:(".$command_id."), marking as canceled");
			$sql="UPDATE from  command set tunnel_active=0 WHERE id=:id";
			$stmt=$this->db->execute($sql,array('id'=>$command_id));
			return array('status'=>'ok','msg'=>'');
		}

		return array('status'=>'error','msg'=>$cancel_tunnel_result);
	}



	public function getActiveTunnels($agent_ids=array()) {
		$this->logger->debug("Command::getActiveTunnels");

		if (!count($agent_ids)) {
			$sql="SELECT id, agent_id, time_requested, time_returned, tunnel_port FROM ".
				 "  command WHERE type='tunnel' AND tunnel_active=1 AND time_returned IS NULL";
		}
		else {
			$agent_str="'" . implode("','", $agent_ids) . "'";
			$sql="SELECT id, agent_id, time_requested, time_returned, tunnel_port FROM ".
				 "  command WHERE type='tunnel' AND tunnel_active=1 AND time_returned IS NULL ".
				 "AND agent_id IN ($agent_str)";
		}

		$stmt=$this->db->execute($sql);
		$rows=$stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $rows;
	}


	//executed and finished commands
	public function getCommandHistory($limit='') {
		$this->logger->debug("Command::getCommandHistory");

		if (!empty($limit)) {
			$limit=" LIMIT $limit ";
		}

		$sql="SELECT id, group_tag, agent_id, time_requested, time_returned, cmd, exit_status, type, exec_type, SUBSTR(output,1,20) as output ".
			" FROM command ".
			" WHERE time_returned is not null ". // finished commands
			" ORDER BY group_tag desc, time_requested DESC $limit";
		$stmt=$this->db->execute($sql);
		$rows=$stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $rows;
	}

	//executed commands
	public function getCommandByID($command_id) {
		$this->logger->debug("Command::getCommandByID");

		$sql="SELECT * FROM command WHERE id=:command_id";
		$stmt=$this->db->execute($sql,array('command_id'=>$command_id));
		$row=$stmt->fetch(\PDO::FETCH_ASSOC);
		return $row;
	}



	public function getScopeSets() {
		$this->logger->debug("Command::getScopeSets");

		$sql="SELECT id, description, agent_ids FROM agent_scope command ORDER BY description ASC";
		$stmt=$this->db->execute($sql);
		$rows=$stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $rows;
	}

	public function addCommand($pparams) {
		$type=$pparams['type'];
		$agent_ids=$pparams['agent_ids'];
		$exec_type=$pparams['exec_type'];
		$command=$pparams['command'];
		$timeout=$pparams['timeout'];

		if (empty($exec_type)) {
			$this->logger->error("Command::addCommand:agent_id:$agent_id, error: exec_type is undefined");
			return null;
		}

		$this->logger->info("Command::addCommand:$type,$exec_type,$command, agent_ids:".print_r($agent_ids,true));

		$time_requested=time();
		$group_tag = uniqid();
		if ($type == 'tunnel') {
			foreach ($agent_ids as $agent_id) {
				$sql="INSERT into command(agent_id, exec_type, cmd, time_requested, type, timeout, tunnel_port, tunnel_active, group_tag) VALUES ".
					"(:agent_id, :exec_type, :command, :time_requested, :type, :timeout, :tunnel_port, :tunnel_active, :group_tag) ";

				$port=0; //port=0 means ssh will bind to a random free port and coolagent will notify the server of the bound port with an http post

				$command=$this->makeTunnelCommand($agent_id,$port);
				$this->logger->debug("Command::addCommand(tunnel): agent:$agent_id, command: ($command)");

				$stmt=$this->db->execute($sql,array(
					'agent_id'=>$agent_id,
					'exec_type'=>$exec_type,
					'command'=>$command,
					'time_requested'=>$time_requested,
					'type'=>'tunnel',
					'timeout'=>$timeout,
					'tunnel_port'=>$port,
					'tunnel_active'=>False,
					'group_tag'=>$group_tag,
					)
				);
			}
		}
		elseif ($type == 'cmd' || $type == 'internal') {
			$sql="INSERT into command(agent_id,exec_type,cmd,time_requested,type,timeout,group_tag) VALUES ".
				"(:agent_id,:exec_type,:command,:time_requested,:type,:timeout,:group_tag) ";

			foreach ($agent_ids as $agent_id) {
				$this->logger->debug("Command::addCommand($type):$type,$exec_type,$command, agent_ids:".print_r($agent_ids,true));

				$stmt=$this->db->execute($sql,array(
					'agent_id'=>$agent_id,
					'exec_type'=>$exec_type,
					'command'=>$command,
					'time_requested'=>$time_requested,
					'type'=>$type,
					'timeout'=>$timeout,
					'group_tag'=>$group_tag,
					)
				);
			}
		} //cmd
		else {
			$this->logger->error("Command::addCommand($type):$type,$exec_type,$command, invalid type");
		}



	}

	private function makeTunnelCommand($agent_id,$port) {
		// #ssh_client# ssh  %s -R %d:%s:%d %s
		$userhost_dest=$this->settings['network']['tunnel_dest_username']."@".$this->settings['network']['tunnel_dest_hostname'];
		$command="#ssh_client# $userhost_dest -R $port:localhost:22 -N";
		//dropbear: -K 50 -t (or -T?) 
		//openssh: -C -o ServerAliveInterval=50
		return $command;
	}


	/* NOT USED
	 * Bind to a port and quit, hoping the OS will leave this port unallocated for a while.
	 * Used to to "allocate" a port in order to pass it to ssh -R to the agent  if the agent does 
	 * not support detecting the auto-bound port number on the remote host (when passing 0 as port number to ssh)
	 */
	private function bindAPortAndQuit($port=0,$address="0.0.0.0") {
		// Create a new socket
		if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
			$this->logger->error("Command::bindAPortAndQuit:socket_create: ".socket_strerror(socket_last_error() ));
			return null;
		}

		if (!socket_set_option($sock, SOL_SOCKET, SO_REUSEADDR, 1)) { 
			$this->logger->error("Command::bindAPortAndQuit:socket_set_option: ".socket_strerror(socket_last_error() ));
			return null;
		} 

		if (socket_bind($sock, $address, $port) === false) {
			$this->logger->error("Command::bindAPortAndQuit:socket_bind: ".socket_strerror(socket_last_error() ));
			return null;
		}

		if (socket_getsockname ( $sock , $addr , $assigned_port ) === false) {
			$this->logger->error("Command::bindAPortAndQuit:socket_getsockname: ".socket_strerror(socket_last_error() ));
			return null;
		}

		if (socket_listen($sock, 5) === false) {
			$this->logger->error("Command::bindAPortAndQuit:socket_listen: ".socket_strerror(socket_last_error() ));
			return null;
		}

		socket_shutdown($sock, 2);
		socket_close($sock);

		return $assigned_port;

	}

	
	//public function ping($agent_id,$queryParams,$ip) {

}
?>
