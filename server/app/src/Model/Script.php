<?php

namespace App\Model;

use Psr\Log\LoggerInterface;

class Script {

	private $db;
	private $logger;
	private $settings;

    public function __construct(LoggerInterface $logger,$db,$settings) {
		$this->logger = $logger;
		$this->db = $db;
		$this->settings = $settings;
    }
 

	public function getScriptTitles() {
		$this->logger->debug("Script::getScripts");

		$sql="SELECT id,title,time_modified FROM script";
		$stmt=$this->db->execute($sql);
		$rows=$stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $rows;
	}


	public function getScriptByID($script_id) {
		$this->logger->debug("Script::getScriptByID");

		if (!is_numeric($script_id)) {
			$this->logger->error("Script::getScriptByID:id:(".$script_id.") is not numeric");
			return array('status'=>'error','message'=>"script_id ($script_id) is not numeric");
		}

		$sql="SELECT * FROM script WHERE id=:script_id";
		$stmt=$this->db->execute($sql,array('script_id'=>$script_id));
		$row=$stmt->fetch(\PDO::FETCH_ASSOC);
		$row['status']='ok';
		return $row;
	}

	public function addScript($post_params) {
		$this->logger->info("Script::addScript");
		$this->logger->debug("Script::addScript:post_params:".print_r($post_params,true));

		if (empty($post_params['title']) ||  strlen($post_params['title'])<2) {
			return array('status'=>'error','message'=>'title too short');
		}
		if (strlen($post_params['content'])<3) {
			return array('status'=>'error','message'=>'content too short');
		}

		$sql="INSERT INTO  script (title,content,time_modified) VALUES (:title,:content,:mtime)";
		$stmt=$this->db->execute($sql,array(
			'title'=>$post_params['title'],
			'content'=>$post_params['content'],
			'mtime'=>time(0)
			)
		);

		return array('status'=>'ok','message'=>'added');
	}

	public function deleteScriptID($script_id) {
		$this->logger->debug("Script::deleteScriptID:id:".$script_id);

		if (!is_numeric($script_id)) {
			$this->logger->error("Script::deleteScriptID:id:(".$script_id.") is not numeric");
			return array('status'=>'error','message'=>"script_id ($script_id) is not numeric");
		}

		$sql="DELETE from script where id=:id";
		$stmt=$this->db->execute($sql,array('id'=>$script_id));

	}

	public function updateScriptID($script_id,$put_params) {

		$this->logger->debug("Script::updateScriptID:id:".$script_id);

		if (empty($put_params['title']) ||  strlen($put_params['title'])<2) {
			return array('status'=>'error','message'=>'title too short');
		}

		if (!is_numeric($script_id)) {
			$this->logger->error("Script::updateScriptID:id:(".$script_id.") is not numeric");
			return array('status'=>'error','message'=>"script_id ($script_id) is not numeric");
		}

		$sql="UPDATE script set title=:title, content=:content, time_modified=:mtime where id=:id";
		$stmt=$this->db->execute($sql,array(
			'id'=>$script_id,
			'title'=>$put_params['title'],
			'content'=>$put_params['content'],
			'mtime'=>time(0)
		));
		return array('status'=>'ok','message'=>"");

	}


}
?>
