<?php
 
namespace App\Model ;

use \PDO as PDO;
use Psr\Log\LoggerInterface;

class DB {
    private $dbh;
	private $logger;
	private $settings;
 
    /** Connects to database **/
    public function __construct(LoggerInterface $logger,$settings=array()) {
		$this->logger = $logger;
		$this->settings = $settings;

		$logger->debug("DB CONSTRUCTOR".  rand() );
		return $this->connect();
    }
 
	//w/ prepare
    function execute($sql,$params=NULL) {
		$dbh=$this->dbh;

		$sth = $dbh->prepare($sql);
		$error = $dbh->errorInfo();

		if(((int)$error[0]||(int)$error[1]) && isset($error[2])) {
			//$errorstr= "DB Error: ($sql):".$error[2].". Parameters:".implode(",",$params);
			$errorstr= "DB Error: ($sql):".$error[2].". Parameters:".print_r($params,true);
			$errorbt= debug_backtrace();
			$errorno=$error[1]+$error[0];
			$this->logger->error("dbh->prepare: $errorstr");// BACKTRACE:".print_r($errorbt,true));
			return 0;
		}

		try {
			if (is_array($params))
				  $sth->execute($params);
			else
				  $sth->execute();
		}
		catch (PDOException $e) {
			$this->logger->error("execute: ($sql) error: " . $e->getMessage() );
		    die();
		}

		$error = $sth->errorInfo();
		if(((int)$error[0]||(int)$error[1]) && isset($error[2])) {
			//$errorstr= "DB Error: ($sql): ".$error[2].". Parameters:".implode(",",$params);
			$errorstr= "DB Error: ($sql):".$error[2].". Parameters:".print_r($params,true);
			$errorbt= debug_backtrace();
			$errorno=$error[1]+$error[0];
			$this->logger->error("dbh->execute:$errorstr");
			 //BACKTRACE:".print_r($errorbt,true));
		}

		return $sth;
	}

    function connect() {
		$dbname=$this->settings['path'];

		try {
		    $dbh = new PDO("sqlite:$dbname");
		}
		catch (PDOException $e) {
			$this->logger->error("Open database ($dbname) error: " . $e->getMessage() );
		    die();
		}
		
		//some db initialization
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
		$ret = $dbh->exec("PRAGMA case_sensitive_like = 0;");
		$ret = $dbh->exec("PRAGMA encoding = \"UTF-8\";");
		$this->dbh = $dbh;
		$this->logger->debug("DB $dbname connected");

		return $dbh;
    }

	function getDBH() {
		return $this->dbh;
	}
 
     
}
