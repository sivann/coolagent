<?php

namespace App\Model;

//use Slim\Views\Twig;
use Psr\Log\LoggerInterface;

class Server {

	private $db;
	private $logger;
	private $settings;

    public function __construct(LoggerInterface $logger,$db,$settings) {
		$this->logger = $logger;
		$this->db = $db;
		$this->settings = $settings;
    }
 

	public function getScopeSets() {
		$this->logger->debug("Server::getScopeSets");

		$sql="SELECT id, description, agent_ids FROM agent_scope command ORDER BY description ASC";
		$stmt=$this->db->execute($sql);
		$rows=$stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $rows;
	}


    private function getTablesStatus() {
		$sql="SELECT * from table_ts";
		$stmt=$this->db->execute($sql);
		$row=$stmt->fetch(\PDO::FETCH_ASSOC);
		return $row;
	}

    //for ajax long-poll
    public function getInitData() {
		return array('network' => $this->settings['network']);
	}


    //for UI ajax long-poll
    public function getChangedTables($qparams) {
		$b_cl_ts=$qparams['browser_agent_ts'];
		$b_co_ts=$qparams['browser_command_ts'];

		if (empty($b_cl_ts) || empty($b_co_ts)) {
			$this->logger->warning("Server::getChangedTables:($b_cl_ts) or ($b_co_ts) is empty");
			sleep (3);
			return array('status'=>'error:no params sent');
		}

		$sql="SELECT * from table_ts";

		$changed_tables=array('status'=>'unknown');

		//longpoll loop would start here

		$stmt=$this->db->execute($sql);
		$rows=$stmt->fetch(\PDO::FETCH_ASSOC);

		if ($b_cl_ts!=$rows['agent_ts']) {
			$changed_tables['status']='changed';
			$changed_tables["agent_ts"]=$rows['agent_ts'];
		}

		if ($b_co_ts!=$rows['command_ts']) {
			$changed_tables['status']='changed';
			$changed_tables["command_ts"]=$rows['command_ts'];
		}

		if (count($changed_tables)>1) {
			$this->logger->debug("Server::getChangedTables:changed_tables:".print_r($changed_tables,true));
			return $changed_tables;
		}

		/* longpoll blocks the browser ajax :-(, just return new status. Could define 2nd domain for longpoll reqs and use cors */
		/*
		session_write_close();
		if ($loopcount>25) { //max half-minute long-poll
			$this->logger->debug("getChangedTables:loopcount==15, no changes, return empty list");
			return array('status'=>'nochanges');
		}
		*/
		$this->logger->debug("Server::getChangedTables:, no changes, returning empty list");
		return array('status'=>'nochanges');

    }

    /* generate .ini
	 * reads coolagent.ini.sample
	 * replaces agent_id with a random one if requested (software/ini/random)
	 * sets correct server_url
	 * uses same longpoll_period as the one saved in the db info column on the last fullupdate
	 * leaves the rest as is on the sample
	 */
    public function generateINI($agent_id,$sample_ini_fn,$request) {
		$ini_array = parse_ini_file ($sample_ini_fn, true, INI_SCANNER_NORMAL);
		if ($ini_array === false) {
			$err=error_get_last();
			$this->logger->error("Server::generateINI:{$sample_ini_fn}:({$err['message']})");
			return false;
		}

		$uri=$request->getUri();
		$url=$uri->getScheme().'://'.$uri->getHost().':'.$uri->getPort().$uri->getBasePath();

		if ($agent_id == 'random') {
			$rnd_id = uniqid('agent_');
			$ini_array['coolagent']['agent_id'] = $rnd_id;
		}
		else {
			$agent_info = $this->getAgentInfo($agent_id);
			if (is_numeric($agent_info['longpoll_period'])) {
				$ini_array['coolagent']['longpoll_period'] = $agent_info['longpoll_period'];
			}
			$ini_array['coolagent']['agent_id'] = $agent_id;
		}

		$ini_array['coolagent']['server_url'] = $url;

		return $this->make_ini($ini_array);
	}

	private function make_ini($array) {
		$res = array();
		foreach($array as $key => $val)
		{
			if(is_array($val))
			{
				$res[] = "[$key]";
				foreach($val as $skey => $sval) $res[] = "$skey = ".(is_numeric($sval) ? $sval : '"'.$sval.'"');
			}
			else $res[] = "$key = ".(is_numeric($val) ? $val : '"'.$val.'"');
		}
		return implode("\r\n", $res);

	}

	//return json encoded info column
	private function getAgentInfo($agent_id) {
			$sql="SELECT info FROM agent WHERE agent_id=:agent_id ";
			$stmt=$this->db->execute($sql,array('agent_id'=>$agent_id));
			$row=$stmt->fetch(\PDO::FETCH_ASSOC);
			$info = $row['info'];
			$info_r = json_decode($info,true);
			return $info_r;
	}



}
?>
