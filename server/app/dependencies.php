<?php
// DIC configuration

$container = $app->getContainer();

// -----------------------------------------------------------------------------
// Service providers
// -----------------------------------------------------------------------------

/* parse coolagent.version files and make version data available to twig */
function findAgentDownloadableVersions() {
	$path=getcwd()."/../../agent/bindist/";
	$version_data=[];

	foreach (glob("$path/*",GLOB_ONLYDIR) as $dirname) {
		$version_fn = $dirname.'/coolagent.version';
		if (file_exists($version_fn)) {
			$version_data[basename($dirname)] = explode("|",(file_get_contents($version_fn)));
		}
		else
			$version_data[basename($dirname)] = null;
	}
	return $version_data;
}

// Twig
$container['view'] = function ($c) {
    $settings = $c->get('settings');
    $view = new \Slim\Views\Twig($settings['view']['template_path'], $settings['view']['twig']);

	//make some variables accessible to twig
	$view->getEnvironment()->addGlobal("uri", $c->get('request')->getUri()); //path and basePath
	$view->getEnvironment()->addGlobal("session", $_SESSION); //session info

	$agentversion=findAgentDownloadableVersions();

	$view->getEnvironment()->addGlobal("agentversion", $agentversion); //downloadable agent binary versions

    // Add extensions
    $view->addExtension(new Slim\Views\TwigExtension($c->get('router'), $c->get('request')->getUri()));
    $view->addExtension(new Twig_Extension_Debug());
    return $view;
};


// Flash messages
$container['flash'] = function ($c) {
    return new \Slim\Flash\Messages;
};



// -----------------------------------------------------------------------------
// Service factories
// -----------------------------------------------------------------------------

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings');
    $logger = new \Monolog\Logger($settings['logger']['name']);
    $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
	if ($settings['logger']['syslog'])
		$logger->pushHandler(new \Monolog\Handler\SyslogHandler($settings['logger']['name'],$settings['logger']['facility'], \Monolog\Logger::toMonologLevel($settings['logger']['level']) ));
	if (strlen($settings['logger']['syslog']))
		$logger->pushHandler(new \Monolog\Handler\RotatingFileHandler($settings['logger']['file'],10, \Monolog\Logger::toMonologLevel($settings['logger']['level']) ));
    return $logger;
};

// -----------------------------------------------------------------------------
// Action factories
// -----------------------------------------------------------------------------

$container['App\Action\HomeAction'] = function ($c) {
    return new App\Action\HomeAction($c['view'], $c['logger']);
};

/*
$container['App\Action\ViewClientsAction'] = function ($c) {
    return new App\Action\ViewClientsAction($c['view'], $c['logger'],$c['db']);
};
*/


////////////////////

/* PDO Database */
$container['db']=function ($c) {
    $DB = new App\Model\DB($c['logger'],$c['settings']['pdo']);
	return $DB;
};

/*
$container['netstat']=function ($c) {
    $netstat = new App\Netstat($c['settings']['external_netstat']);
	return $netstat;
};
*/



// Agent
//$client=$container['App\Model\Agent'];
$container['App\Model\Agent'] = function ($c) {
    return new App\Model\Agent($c['logger'],$c['db'],$c['settings']);
};


// Server
$container['App\Model\Server'] = function ($c) {
    return new App\Model\Server($c['logger'],$c['db'],$c['settings']);
};

// Command
$container['App\Model\Command'] = function ($c) {
    return new App\Model\Command($c['logger'],$c['db'],$c['settings']);
};


// Script
$container['App\Model\Script'] = function ($c) {
    return new App\Model\Script($c['logger'],$c['db'],$c['settings']);
};


