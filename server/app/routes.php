<?php
// Routes

/*
$app->get('/', 'App\Action\HomeAction:dispatch')
    ->setName('homepage');
*/

$app->get('/',  function ($request, $response, $args) {
	$this->view->render($response, 'home.twig');
});

$app->get('/ui/dashboard/',  function ($request, $response, $args) {
	$this->view->render($response, 'dashboard.twig');
});

$app->get('/ui/history/',  function ($request, $response, $args) {
	$this->view->render($response, 'history.twig');
});

$app->get('/ui/maintenance/',  function ($request, $response, $args) {
	$this->view->render($response, 'maintenance.twig');
});

$app->get('/ui/scripts/',  function ($request, $response, $args) {
	$this->view->render($response, 'scripts.twig');
});

$app->get('/ui/agents/',  function ($request, $response, $args) {
	//$this->view->render($response,'agents.twig',array('uri'=>$request->getUri(),'session'=>$_SESSION) ); 
	$this->view->render($response,'agents.twig');
})->setName('agents');


$app->get('/ui/logout/',  function ($request, $response, $args) {
	$_SESSION["loggedin"] = 0;
	$response=$response->withStatus(401)->write('logout'); //that causes the actual logout for HTTP
	$this->view->render($response,'gotohome.twig',array('uri'=>$request->getUri(),'session'=>$_SESSION,'message'=>'You have been logged out.') ); 
	return $response;
})->setName('logout');


// Download the client software
$app->get('/software/agent/{arch}[/{fn}]',  function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Agent Download");

	$arch=trim($args['arch']);
    $clientfn="../../agent/bindist/$arch/coolagent";

    if (!file_exists($clientfn)) {
        $response=$response->withStatus(404)->write("$clientfn not found");
		$this->logger->error("$clientfn: not found ".getcwd());
        return $response;
    }

    $fsize=filesize($clientfn);
    $fcontents=file_get_contents($clientfn);

    $response=$response
    ->withStatus(200)
    ->withHeader('Content-Length',$fsize)
    ->withHeader('Expires',0)
    ->withHeader('Content-Disposition','attachment; filename="coolagent"')
    ->write($fcontents);

    return $response;
});

// Download the agent.ini 
$app->get('/software/ini/{agent_id}[/{fn}]',  function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("INI download");
    $sample_ini_fn="../../agent/coolagent.ini.sample";

    if (!file_exists($sample_ini_fn)) {
        $response=$response->withStatus(404)->write("$sample_ini_fn not found");
		$this->logger->error("$sample_ini_fn: not found ".getcwd());
        return $response;
    }

	$server=$this->get('App\Model\Server');
	$ini=$server->generateINI($args['agent_id'],$sample_ini_fn,$request);

    $fsize=strlen($ini);

    $response=$response
    ->withStatus(200)
    ->withHeader('Content-Length',$fsize)
    ->withHeader('Expires',0)
    //->withHeader('Content-Disposition','attachment; filename="coolagent.ini.new"')
    ->write($ini);

    return $response;
});




// === AGENTS

$app->get('/agents',  function ($request, $response, $args) {
	$agents=$this->get('App\Model\Agent')->getAgents();
	$response = $response->withJson($agents);
	return $response;
});

//Get Statistics of all currently online/offline agents
$app->get('/agents/status',  function ($request, $response, $args) {
	$agent=$this->get('App\Model\Agent');
	$agent_Status=$agent->getAgentsStatus();
	$response = $response->withJson($agent_Status);
	return $response;
});


//Get Statistics of all currently online/offline agents + geoIP query results
$app->get('/agents/geostatus',  function ($request, $response, $args) {
	$agent=$this->get('App\Model\Agent');
	$agent_geostatus=$agent->getAgentsGeoStatus();
	$response = $response->withJson($agent_geostatus);
	return $response;
});


//agent rrd stats
$app->get('/agents/{agent_id}/stats',  function ($request, $response, $args) {
	$agent=$this->get('App\Model\Agent');
	$queryParams = $request->getQueryParams();
	$stats=$agent->getStats(trim($args['agent_id']),$queryParams);
	$response = $response->withJson($stats);
	return $response;
});


//agent requests registration, cookie-based auth
$app->get('/ca/agents/{agent_id}/registration',  function ($request, $response, $args) {
	$queryParams = $request->getQueryParams();
	$ipAddress = $request->getAttribute('ip_address'); //loaded in middleware.php
	$agent=$this->get('App\Model\Agent');

	$txt=$agent->register(trim($args['agent_id']),$queryParams,$ipAddress);
	$response->write($txt);

	return $response;
});

//unregister agent (delete its password) so it can register
$app->delete('/agents/{agent_id}/registration',  function ($request, $response, $args) {
	$ipAddress = $request->getAttribute('ip_address'); //loaded in middleware.php
	$agent=$this->get('App\Model\Agent');
	$result=$agent->unregister(trim($args['agent_id']),$ipAddress);
	$response=$response->withJson($result);
	return $response;
});

//agent requests new command, cookie-based auth
$app->get('/ca/agents/{agent_id}/nextcommand',  function ($request, $response, $args) {
	$queryParams = $request->getQueryParams();
	$ipAddress = $request->getAttribute('ip_address');
	$agent=$this->get('App\Model\Agent');

	$txt=$agent->getNextCommand(trim($args['agent_id']),$queryParams,$ipAddress);
	$response->write($txt);

	return $response;
})->setName('agent_getcmd');


//agent posts ssh port bound on the target server
$app->post('/ca/agents/{agent_id}/commands/{command_id}/tunnelport',  function ($request, $response, $args) {
	$agent=$this->get('App\Model\Agent');

	$post_params = $request->getParsedBody(); 
	$agent->setTunnelPort($args['agent_id'],$args['command_id'],$post_params);

	return $response;
});


//agent posts result, cookie-based auth
$app->post('/ca/agents/{agent_id}/commands/{command_id}/result',  function ($request, $response, $args) {
	$agent=$this->get('App\Model\Agent');

	$post_params = $request->getParsedBody(); 
	$agent->setCommandResult($args['agent_id'],$args['command_id'],$post_params);
	
	return $response;
});


//delete agent
$app->delete('/agents/{agent_id:\d+}',  function ($request, $response, $args) {
	$agent=$this->get('App\Model\Agent');
	$agent->deleteAgent($args['agent_id']);
	$response=$response->withHeader('Content-Type','application/json');
	return $response;
});


// === COMMANDS
$app->get('/commands/pending',  function ($request, $response, $args) {
	$command=$this->get('App\Model\Command');

	$pending_commands=$command->getPendingCommands();
	$response = $response->withJson($pending_commands);
	return $response;
});


$app->get('/commands/export',  function ($request, $response, $args) {
	$command=$this->get('App\Model\Command');
	$queryParams = $request->getQueryParams();
	$cmd_history=$command->getCommandHistory($queryParams['limit']);

	$d=date('Y-m-d_H-i-s');

	$response=$response
	->withHeader('Content-Type','application/json')
	->withHeader('Content-Disposition',"attachment; filename=\"history-$d.json\"")
	->write(json_encode($cmd_history));
	return $response;
});

$app->get('/commands',  function ($request, $response, $args) {
	$command=$this->get('App\Model\Command');
	$queryParams = $request->getQueryParams();
	$limit = isset($queryParams['limit'])?$queryParams['limit']:'';

	$cmd_history=$command->getCommandHistory($limit);
	$response=$response->withJson($cmd_history);
	return $response;
});

//add
$app->post('/commands',  function ($request, $response, $args) {
	$command=$this->get('App\Model\Command');
	$post_params = $request->getParsedBody(); 
	$command->addCommand($post_params);

	return $response;
});

//get command output
$app->get('/commands/{command_id}',  function ($request, $response, $args) {
	$command=$this->get('App\Model\Command');
	$output=$command->getCommandByID($args['command_id']);
	$response=$response->withJson($output);
	return $response;
});


//cancel pending command or cancel tunnel
$app->delete('/commands/{command_id}',  function ($request, $response, $args) {
	$command=$this->get('App\Model\Command');
	$result = $command->deleteCommand($args['command_id']);
	$response=$response->withJson($result);

	return $response;
});

$app->get('/commands/tunnels/active',  function ($request, $response, $args) {
	$command=$this->get('App\Model\Command');
	$queryParams = $request->getQueryParams();
	if (isset($queryParams['agent_ids']))
		$active_tunnels=$command->getActiveTunnels($queryParams['agent_ids']);
	else
		$active_tunnels=$command->getActiveTunnels();
	$response = $response->withJson($active_tunnels);
	return $response;
});




// === SERVER MISC

//For various settings
$app->get('/server/misc/initdata',  function ($request, $response, $args) {
	$server=$this->get('App\Model\Server');
	$queryParams = $request->getQueryParams();
	$initData=$server->getInitData($queryParams);

	$response=$response
	->withHeader('Content-Type','application/json')
	->write(json_encode($initData));
	return $response;
});


//For browser ajax long-poll
$app->get('/server/misc/changedtables',  function ($request, $response, $args) {
	$server=$this->get('App\Model\Server');
	$queryParams = $request->getQueryParams();
	$changed_tables=$server->getChangedTables($queryParams);

	$response=$response
	->withHeader('Content-Type','application/json')
	->write(json_encode($changed_tables));
	return $response;
});


// === SCRIPTS

//returns script id and name
$app->get('/scripts',  function ($request, $response, $args) {
	$script_list = $this->get('App\Model\Script')->getScriptTitles();

	$response=$response
	->withHeader('Content-Type','application/json')
	->write(json_encode($script_list));
	return $response;
});

$app->get('/scripts/{script_id}',  function ($request, $response, $args) {
	$queryParams = $request->getQueryParams();
	$script = $this->get('App\Model\Script')->getScriptByID($args['script_id']);

	$response=$response
	->withHeader('Content-Type','application/json')->write(json_encode($script));
	return $response;
});

//add script
$app->post('/scripts',  function ($request, $response, $args) {
	$script_list = $this->get('App\Model\Script')->addScript();
	$result = $this->get('App\Model\Script')->addScript($request->getParsedBody());

	$response=$response
	->withHeader('Content-Type','application/json')->write(json_encode($result));
	return $response;
});

//update script
$app->put('/scripts/{script_id}',  function ($request, $response, $args) {
	$queryParams = $request->getQueryParams();
	$result = $this->get('App\Model\Script')->updateScriptID($args['script_id'],$request->getParsedBody());

	$response=$response
	->withHeader('Content-Type','application/json')->write(json_encode($result));
	return $response;

});

//delete script

$app->delete('/scripts/{script_id}',  function ($request, $response, $args) {
	$queryParams = $request->getQueryParams();
	$script = $this->get('App\Model\Script')->deleteScriptID($args['script_id']);

	$response=$response
	->withHeader('Content-Type','application/json')->write(json_encode($result));
	return $response;

})


?>
