<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);



/* REMOTE IP */

//https://github.com/akrabat/rka-ip-address-middleware
$checkProxyHeaders = true; // Note: Never trust the IP address for security processes!
//$trustedProxies = ['10.0.0.1', '10.0.0.2']; // Note: Never trust the IP address for security processes!
$trustedProxies = array();
$app->add(new RKA\Middleware\IpAddress($checkProxyHeaders, $trustedProxies));




/* AUTH */
// https://github.com/tuupola/slim-basic-auth
// use hashed passwords with: htpasswd -nbBC 10 somebody passw0rd
$users = array(
	"sivann" => "lala",
	"root" => "t00r",
);

use \Slim\Middleware\HttpBasicAuthentication\AuthenticatorInterface;

/* Return true/false to determine if auth succeeds */
class CustomAuthenticator implements AuthenticatorInterface {
	public $users;
	public $app;
	public $request;

    public function __construct(array $arguments) {
		$this->app = $arguments['app'];
	}


    public function __invoke(array $arguments) {
		$user=$arguments['user'];
		$password=$arguments['password'];
		//$request = $this->app->getContainer()->get('request');

		$settings = $this->app->getContainer()->get('settings');
		$allowed_ips=$settings['auth']['allowed_ips'];
		$remote_ip=$_SERVER['REMOTE_ADDR'];

		//allow allowed_ips
		if (in_array($remote_ip,$allowed_ips)) {
			return true;
		}

		if ($user == 'sivann')
			return true;
		else
			return false;
    }
}

$app->add(new \Slim\Middleware\HttpBasicAuthentication([
    "path" => ["/ui", "/commands","/agents"],
    "realm" => "CoolAgent Protected",
	"secure" => false, // allow HTTP (else only HTTPS)
	/*
    "users" => [
        "sivann" => "lala",
        "root" => "t00r",
        "somebody" => "passw0rd"
    ],
	*/
	"authenticator" => new CustomAuthenticator(array('app'=>$app)),
    "callback" => function ($request, $response, $arguments) { //on success
		$_SESSION["loggedin"] = 1;
		$_SESSION["user"] = $arguments['user']; //also in $_SERVER["PHP_AUTH_USER"]
		/* 
		$arguments:
			Array
			(
				[user] => sivann
				[password] => lala
			) 
		*/
		return $response;

    },
    "error" => function ($request, $response, $arguments) use ($app) { //on 401
		$_SESSION["loggedin"] = 0;

		$container = $app->getContainer();
		$renderer=$container->get('view');

		$renderer->render($response,'gotohome.twig',array('uri'=>$request->getUri(),'session'=>$_SESSION,'message'=>$arguments['message']) );
        return $response;
		//$msg = $arguments["message"].". Press refresh.";
        //return $response->write($msg);
    }
]));

