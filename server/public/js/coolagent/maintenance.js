
/* Agents Panel */

$(function() {
	loadAgents();
	addHandlers();

	initEditor();

});

function loadAgents() {
	getAgents().done(populateAgentList);
}


function populateAgentList(agents) {
	var $select=$('#agent_list');
	$select.empty();
	$select.append('<option value="">-</option>');
	$.each(agents, function(key, val){
		$select.append('<option value="' + val.agent_id + '">' + val.agent_id + '</option>');
	})

}


/* AGENTS */

function getAgents() {

    var jqxhr = $.ajax({
        url: basepath + "/agents",
        context: document.body,
        dataType: 'json'
    }).
    done(function(data) {
		//agents=data;
    }).
    fail(function(error) {
        console.log('loadAgents error:');
        console.log(error.responseJSON);
    });

    return jqxhr; //for promise use
}



function deleteAgent(agent_id) {

    if (!agent_id) {
        console.log('Requested delete without specifying agent_id');
        return;
    }

    var jqxhr = $.ajax({
        url: basepath + "/agents/"+agent_id,
		method: "DELETE",
        context: document.body,
    }).
    done(function(data) {
        //showAgentsTable(data);
		loadAgents();
    }).
    fail(function(error) {
        console.log('deleteAgent error: agent_id:'+agent_id);
        console.log(error.responseJSON);
        console.log(error);
    });

    return jqxhr; //for promise use
}


function unregisterAgent(agent_id) {

    if (!agent_id) {
        console.log('Requested unregister without specifying agent_id');
        return;
    }

    var jqxhr = $.ajax({
        url: basepath + "/agents/"+agent_id+"/registration",
		method: "DELETE",
        context: document.body,
    }).
    done(function(data) {
        //showAgentsTable(data);
		loadAgents();
    }).
    fail(function(error) {
        console.log('unregisterAgent error: agent_id:'+agent_id);
        console.log(error.responseJSON);
        console.log(error);
    });

    return jqxhr; //for promise use
}


function updateAgent(agent_id) {

    if (!agent_id) {
        console.log('Requested update without specifying agent_id');
        return;
    }

	return sendCommand({type:'internal', exec_type:'u', command:'', timeout:0, agent_ids:[agent_id]});
}


function sendCommand(post_data) {

    var jqxhr = $.ajax({
        url: basepath + "/commands",
        method: "POST",
        context: document.body,
        data: post_data,
    }).
    done(function(data) {
        //console.log(data);
        //loadPendingCommands();
    }).
    fail(function(error) {
        console.log('sendCommand error:');
        console.log(error.responseJSON);
        console.log(error);
    });

    return jqxhr; //for promise use
};

function addHandlers() {

	//confirm manual here:://myclabs.github.io/jquery.confirm/ 
	$("#agent_delete").confirm({
		text: "Are you sure you want to delete this agent?",
		title: "Confirmation required",
		confirm: function(button) {
			var agent_id;
			agent_id=$('#agent_list').val();
			deleteAgent(agent_id);
		},
		cancel: function(button) {
			// nothing to do
		},
		confirmButton: "Yes",
		cancelButton: "No",
		post: false,
		confirmButtonClass: "btn-danger",
		cancelButtonClass: "btn-default",
		dialogClass: "modal-dialog modal-sm" 
	});

	//confirm manual here:://myclabs.github.io/jquery.confirm/ 
	$("#agent_unregister").confirm({
		text: "Are you sure you want to unregister this agent?",
		title: "Confirmation required",
		confirm: function(button) {
			var agent_id;
			agent_id=$('#agent_list').val();
			unregisterAgent(agent_id);
		},
		cancel: function(button) {
			// nothing to do
		},
		confirmButton: "Yes",
		cancelButton: "No",
		post: false,
		confirmButtonClass: "btn-warning",
		cancelButtonClass: "btn-default",
		dialogClass: "modal-dialog modal-sm" 
	});

	$("#agent_update").confirm({
		text: "Are you sure you want to self-update the software on this agent?",
		title: "Confirmation required",
		confirm: function(button) {
			var agent_id;
			agent_id=$('#agent_list').val();
			updateAgent(agent_id);
		},
		cancel: function(button) {
			// nothing to do
		}
	});

}



