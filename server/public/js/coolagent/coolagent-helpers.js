
// gets string or number as argument
function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

// returns string representation of a duration in seconds
function secondsToTime(secs)
{
    var datediff;
    var m,d,h,y

	if (secs && secs <0)
		return secs +'sec';

    if (secs && secs >0) {
        datediff=Number(secs/60); //hours
        datestr = datediff<60  &&  Math.round(datediff*10)/10 + ' min' ||
                    datediff<(24*60)  &&  Math.round(datediff/60*10)/10 + ' hr' ||
                    datediff<(8760*60) &&  Math.round(datediff/24/60*10)/10 + ' days' ||
                    datediff>=(8760*60)  &&  Math.round(datediff/8760/60*10)/10 + ' yr';
    }
    else {
        datestr='?';
        datediff='-';
    }
    return datestr;
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

//unix epoch (seconds) 2 date
function epoch2date(epoch) {
	if (!epoch || isNaN(epoch))
		return '';


	var date = new Date(parseFloat(epoch*1000))
	var datestr=
		date.getDate() + "/" +
		pad((date.getMonth() + 1),2) + "/" +
		date.getFullYear() + " " +
		pad(date.getHours(),2) + ":" +
		pad(date.getMinutes(),2) + ":" +
		pad(date.getSeconds(),2)

	return datestr;
}


String.prototype.trunc = String.prototype.trunc ||
      function(n){
          return this.length>n ? this.substr(0,n-1)+'…' : this;
      };

//encode double quote, leaving the rest intact
if (!String.prototype.encodeDQ) {
  String.prototype.encodeDQ = function () {
    return this.replace(/</g, '&lt;')
               .replace(/>/g, '&gt;')
               .replace(/"/g, '&quot;');
  };
}


if (!String.prototype.encodeHTML) {
  String.prototype.encodeHTML = function () {
    return this.replace(/&/g, '&amp;')
               .replace(/</g, '&lt;')
               .replace(/>/g, '&gt;')
               .replace(/"/g, '&quot;')
               .replace(/'/g, '&apos;');
  };
}

if (!String.prototype.decodeHTML) {
  String.prototype.decodeHTML = function () {
    return this.replace(/&apos;/g, "'")
               .replace(/&quot;/g, '"')
               .replace(/&gt;/g, '>')
               .replace(/&lt;/g, '<')
               .replace(/&amp;/g, '&');
  };
}


