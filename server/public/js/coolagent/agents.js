//view agent list
//

//tables:
var agentstbl; //main table
var pendingcmdtbl=null;
var activetunnelstbl;
var cmdhistorytbl;

var settings={};

var max_opentunnel_agents=3 ; //max tunnels to request at once

//agent and command table timestamp (in the form of "time()-rand()")
var browser_agent_ts='x';
var browser_command_ts='x';

var currentScope={}; // list of {agentid1:true, agentid2:true...} 

$(function() {
	$.when( getInitData(), loadTablesStatus() )
	.then(function( data, textStatus, jqXHR ) {
		  //data: json data returned by loadTblesStatus
		loadAgents();
		loadPendingCommands();
		loadActiveTunnels();
		loadCmdHistory();
		getScripts().done(populateRunCmdScriptList);
		addHandlers();
		updateScopeList();
		poll();
	});
});


getInitData = function() {

	var poll_url=basepath + "/server/misc/initdata";

	var jqxhr = $.ajax({
		url: poll_url
	}).
	done(function(data) {
		settings=data;
	}).
	fail(function(error) {
		console.log('getInitData error:');
		console.log(error);
	});

	return jqxhr; //for promise use


}

//get current table timestamps in order to 
//initialize browser_x_ts variables and detect subsequent changes
loadTablesStatus = function() {

	var poll_url=basepath + "/server/misc/changedtables?browser_agent_ts="+browser_agent_ts+"&browser_command_ts="+browser_command_ts+"";

	var jqxhr = $.ajax({
		url: poll_url
	}).
	done(function(data) {
		processTableChanges(data);
	}).
	fail(function(error) {
		console.log('loadTablesStatus error:');
		console.log(error);
	});

	return jqxhr; //for promise use
};


function poll(){
	var poll_url
    setTimeout( function(){
		  poll_url=basepath + "/server/misc/changedtables?browser_agent_ts="+browser_agent_ts+"&browser_command_ts="+browser_command_ts+"";
		  $.ajax({ url: poll_url, success: function(data){
			processTableChanges(data);
			poll();
		  }, dataType: "json"});
	  }, 10000);
}



function processTableChanges(data) {
	if (data.agent_ts && (browser_agent_ts != data.agent_ts)) {
		$('#agents_list_needs_refresh').show();
		//don't auto-refresh or user selections will be lost
		browser_agent_ts=data.agent_ts;
	}

	if (data.command_ts && (browser_command_ts != data.command_ts)) {
		loadPendingCommands();
		loadActiveTunnels();
		loadCmdHistory();
		browser_command_ts=data.command_ts;
	}

}

/******* SCRIPT ***************/
function getScripts() {
    var jqxhr = $.ajax({
        url: basepath + "/scripts",
        context: document.body,
        dataType: 'json'
    }).
    done(function(data) {
    }).
    fail(function(error) {
        console.log('loadScripts error:');
        console.log(error.responseJSON);
    });
    return jqxhr; //for promise use
}

function populateRunCmdScriptList(scripts) {
    var $select=$('#run_command_scriptlist');
    $select.empty();
	$select.append('<option value="">-</option>');
    $.each(scripts, function(key, val){
        $select.append('<option value="' + val.id + '">' + val.title + '</option>');
    })
}

function getScript(script_id) {

    if (!script_id || !isNumber(script_id)) {
        console.log('getScript:invalid script_id' + script_id);
        return;
    }

    var jqxhr = $.ajax({
        url: basepath + "/scripts/"+script_id,
        method: "GET",
        context: document.body,
    }).
    fail(function(error) {
        console.log('getScript error: script_id:'+script_id);
        console.log(error.responseJSON);
        console.log(error);
    });

    return jqxhr; //for promise use
}


function copyRunCmdScriptContent(data) {
	if (!data.content) {
		console.log('ERROR copyRunCmdScriptContent: no data.content');
		return;
	}
	$("#run_command_command").text(data.content);
}


/** CMD HIST **/

function loadCmdHistory() {
	$('#cmd_history_refresh').show();
	var jqxhr = $.ajax({
		url: basepath + "/commands?limit=1000", //up to 1000 recent history in this page
		context: document.body,
		dataType: 'json'
	}).
	done(function(data) {
		//console.log(data);
		showCmdHistoryTable(data);
		$('#cmd_history_refresh').hide();
	}).
	fail(function(error) {
		console.log('loadCmdHistory error:');
		console.log(error.responseJSON);
	});

	return jqxhr; //for promise use
};

//draw?
function showCmdHistoryTable(data) {
	var d=[],rowno,rowdata,c,colkeys,value;
	var estatusj;

	if (cmdhistorytbl != null) {
		cmdhistorytbl.destroy();
		$('#cmdhistory_tbl tbody >tr').remove();
	}

	for (rowno in data) {
		rowdata=data[rowno];
		$row=$('<tr/>');

        for (c in rowdata) {
			if (c == 'exec_type' || c == 'group_tag' || c == 'type') 
				continue;

			value=rowdata[c];
			if (c == 'output') { //truncate command output (which is already truncated by sqlite on this call)
				$row.append( $('<td  title="'+value.encodeDQ()+' &hellip;">').text(value.trunc(6)).addClass(c));
			}
			else if (c == 'cmd') { 
				$row.append( $('<td  title="'+value.encodeDQ()+' &hellip;">').text(value.trunc(8)).addClass(c).attr('data-search',value.encodeDQ()) );
			}
			else if (c == 'time_requested')  {
				$row.append( $('<td>').html(epoch2date(value)).addClass(c).attr('data-sort',encodeURI(value)));
			}
			else if (c == 'time_returned')  {
				$row.append( $('<td>').html(epoch2date(value)).addClass(c).attr('data-sort',encodeURI(value)));
			}
			else if (c == 'exit_status')  {
				try {
					estatusj = JSON.parse(rowdata['exit_status']);
				}
				catch (e) {
					estatusj={'exitstatus':'','exitreason':'-'};
					console.log('invalid exit_status for command id:'+rowdata['id']);
					console.log('status json exception: '+e);
				}
				$row.append( $('<td>').html(estatusj.exitstatus).addClass(c));
			}
			else {
				$row.append( $('<td>').html(value).addClass(c));
			}
		}
		//"show output" button:
		$row.append( $('<td>').addClass('action').html(
		'<button data-target="#show_command_output_modal" data-toggle="modal"  ' +
		' data-id="' + rowdata['id'] + '" ' +
		' data-command="' + rowdata['cmd'].encodeDQ() + '" ' +
		' data-output="' + rowdata['output'].encodeDQ() + '" ' +
		' class="btn btn-xs btn-success hist-show-output">Output</button>'));

		//if (rowdata['exit_status'] == -9999) {
		if (estatusj.exitstatus == -9999) {
			$row.addClass('warning');
		}
		else if (estatusj.exitstatus != 0) {
			$row.addClass('danger');
		}

		$('#cmdhistory_tbl tbody:last-child').append($row);
	}

	cmdhistorytbl = $('#cmdhistory_tbl').DataTable({
		"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
		"order": [] //don't order by default
		//"ordering": false //disable ordering
	});

}


function loadActiveTunnels() {

	$('#activetunnels_refresh').show();

	var jqxhr = $.ajax({
		url: basepath + "/commands/tunnels/active",
		context: document.body,
		dataType: 'json'
	}).
	done(function(data) {
		//console.log(data);
		showActiveTunnelsTable(data);
	}).
	fail(function(error) {
		console.log('loadActiveTunnels error:');
		console.log(error.responseJSON);
	});

	return jqxhr; //for promise use
};

function showActiveTunnelsTable(data) {
	var d=[],rowno,rowdata,c,colkeys,value;
	var $x,s;


	if (activetunnelstbl != null) {
		activetunnelstbl.destroy();
		$('#activetunnels_tbl tbody >tr').remove();
	}

	for (rowno in data) {
		rowdata=data[rowno];
		$row=$('<tr/>');

        for (c in rowdata) {
			value=rowdata[c];
			if (c == 'time_requested')  
				$row.append( $('<td>').html(epoch2date(value)).addClass(c).attr('data-sort',value));
			else if (c == 'time_returned')
				$row.append( $('<td>').html(epoch2date(value)).addClass(c).attr('data-sort',value));
			else
				$row.append( $('<td>').html(rowdata[c]).addClass(c));
		}

		//HERE
		$x=$('<button type="button" class="btn btn-xs btn-info" data-container="body" data-toggle="popover" data-placement="bottom" data-content="x">');
		s='ssh -p '+ rowdata.tunnel_port + ' ' + settings.network.tunnel_dest_username + '@' + settings.network.tunnel_dest_hostname + '';
		$x.attr('data-content',s).html('SSH cmd');
		$row.append( $('<td>').addClass('action').html($x.prop('outerHTML')) );
		$row.append( $('<td>').addClass('action').html('<button data-agentid="' + rowdata['agent_id'] + '" data-id="' + rowdata['id'] + '" class="btn btn-xs btn-danger cmd-close-tunnel">Cancel</button>'));
		$('#activetunnels_tbl tbody:last-child').append($row);
	}

	activetunnelstbl = $('#activetunnels_tbl').DataTable({
	//	"paging":   false,
    //   "ordering": false,
    //  "info":     false,
		"lengthMenu": [ [10, 25, 50], [10, 25, 50] ],
		"order": [] //don't order by default
	});
	$('#activetunnels_refresh').fadeOut( "slow" );

	$('#activetunnels_tbl [data-toggle="popover"]').popover()


	//console.log(data);
}



function loadPendingCommands() {

	$('#pending_commands_refresh').show();

	var jqxhr = $.ajax({
		url: basepath + "/commands/pending",
		context: document.body,
		dataType: 'json'
	}).
	done(function(data) {
		//console.log(data);
		showPendingCommandsTable(data);
	}).
	fail(function(error) {
		console.log('updatePending error:');
		console.log(error.responseJSON);
	});

	return jqxhr; //for promise use
};

function showPendingCommandsTable(data) {
	var d=[],rowno,rowdata,c,colkeys,value;


	if (pendingcmdtbl != null) {
		pendingcmdtbl.destroy();
		$('#pendingcmd_tbl tbody >tr').remove();
	}

	for (rowno in data) {
		rowdata=data[rowno];
		$row=$('<tr/>');

        for (c in rowdata) {
			//if (c == 'id') continue;
			value=rowdata[c];
			if (c == 'time_requested')  
				$row.append( $('<td>').html(epoch2date(value)).addClass(c).attr('data-sort',value));
			else if (c == 'time_communicated')
				$row.append( $('<td>').html(epoch2date(value)).addClass(c).attr('data-sort',value));
			else if (c == 'time_returned')
				$row.append( $('<td>').html(epoch2date(value)).addClass(c).attr('data-sort',value));
			else if (c == 'cmd')
				$row.append( $('<td  title="'+value.encodeDQ()+' &hellip;">').text(value.trunc(8)).addClass(c).attr('data-search',value.encodeDQ()) );
			else
				$row.append( $('<td>').text(value).addClass(c));
		}

		//do not offer to cancel already-communicated commands
		if (rowdata['time_communicated']==null)
			$row.append( $('<td>').addClass('action').html('<button data-id="' + rowdata['id'] + '" data-toggle="tooltip" class="btn btn-xs btn-danger cancel-pendingcmd" title="Cancel not-yet communicated cmd">Cancel</button>'));
		else
			$row.append( $('<td>').addClass('action').html('running'));
		$('#pendingcmd_tbl tbody:last-child').append($row);
	}

	pendingcmdtbl = $('#pendingcmd_tbl').DataTable({
		"lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
		"order": [] //don't order by default
	});

	$('#pending_commands_refresh').fadeOut( "slow" );
	$('#pendingcmd_tbl [data-toggle="tooltip"]').tooltip();

	//console.log(data);
}



// Fetch agent list from server in json and show them
function loadAgents() {

	$('#agents_list_refresh').show();

	var jqxhr = $.ajax({
		url: basepath + "/agents",
		context: document.body,
		dataType: 'json'
	}).
	done(function(data) {
		//console.log(data);
		showAgentsTable(data);
	}).
	fail(function(error) {
		console.log('loadAgents error:');
		console.log(error.responseJSON);
	});

	return jqxhr; //for promise use
}



function showAgentsTable(data) {
	var isdelayed;
	var col;
	var grace_sec = 30 ; //grace period after poll_period to declare a host delayed
	var val=0;
	var nodename='',machine='',release='',additional_info='',os_distribution='',os_release='',model='',manufacturer='',agent_version=''; //from parsing info json

	if (agentstbl != null) {
		agentstbl.destroy();
		$('#agentlist_tbl tbody >tr').remove();
	}
	var polcolskel="<div class=polldata> <div data-toggle='tooltip' title='(delay - poll_period - longpoll_period) Can go as low as -longpoll_period' class='age_sec poll_delay label label-default'></div> "+
		"<div class='poll_info'><div data-toggle='tooltip'  title='poll period' class='poll_period label label-default'></div> "+
		" <div data-toggle='tooltip'  title='longpoll_period' class='longpoll_period label label-default'></div> </div>"+
		"</div>";

	for (rowno in data) {
		rowdata=data[rowno];
		$row=$('<tr/>');

        colkeys=['agent_id','info','time_registered','last_ip','age_sec'];
        d=Date(rowdata['last_poll_ts']);

        for (i in colkeys) {
            colkey=colkeys[i]

			if (colkey == 'age_sec') {
				val = secondsToTime(rowdata['age_sec']);
				var $col=$("<td>").html(polcolskel);
				$('div.poll_delay',$col).html(val);
				$('div.poll_period',$col).html(rowdata.poll_period);
				$('div.longpoll_period',$col).html(rowdata.longpoll_period);
				$col.attr('data-order',rowdata['age_sec']);
				$row.append($col);
			}
			else if (colkey == 'info') {
				try {
					//infoj = JSON.parse(rowdata['info']);
					infoj = rowdata['info'];
				}
				catch (e) {
					console.log(e);
					console.log('invalid info json for agent_id:'+rowdata['agent_id'] + ' Rowdata:');
					console.log(rowdata);
					infoj = {};
				}

				if (infoj && infoj.additional_info) additional_info=infoj.additional_info; else additional_info='';
				if (infoj && infoj.nodename) nodename=infoj.nodename; else nodename='';
				if (infoj && infoj.machine) machine=infoj.machine; else machine='';
				if (infoj && infoj.release) release=infoj.release; else release='';
				if (infoj && infoj.os_release) os_release=infoj.os_release; else os_release='';
				if (infoj && infoj.os_distribution) os_distribution=infoj.os_distribution; else os_distribution='';
				if (infoj && infoj.agent_version) agent_version=infoj.agent_version; else agent_version='';
				if (infoj && infoj.manufacturer) manufacturer=infoj.manufacturer; else manufacturer='';
				if (infoj && infoj.model) model=infoj.model; else model='';


				$row.append( $('<td>').text(additional_info).addClass('additional_info'));
				$row.append( $('<td>').text(agent_version).addClass('agent_version'));
				$row.append( $('<td>').text(nodename).addClass('nodename'));
				$row.append( $('<td>').text(machine).addClass('machine'));
				var infotxt=""
				infotxt=os_distribution+" "+os_release+" "+release+" "+manufacturer+" "+model;
				$row.append( $('<td>').text(infotxt).addClass('agent_info')); //info is already used
			}
			else if (colkey == 'time_registered') {
				var secs = rowdata[colkey];
				var dt=epoch2date(secs);
				$row.append( $('<td>').html(dt).addClass(colkey));
			}
			else {
				val = rowdata[colkey];
				$row.append( $('<td>').html(val).addClass(colkey));
			}
        }

		//CMD Status (based on number of pending commands)

		//different colors for very delayed
		var delaysecs = rowdata['age_sec'] - rowdata['poll_period'];
		if ((delaysecs) > (86400*2 ) ) {
			$row.addClass('delayed2');
			$('.poll_delay',$row).removeClass('label-default').addClass('label-danger');
		}
		else if (delaysecs > grace_sec ) {
			$row.addClass('delayed');
			$('.poll_delay',$row).removeClass('label-default').addClass('label-warning');
		}
		else if (delaysecs < 0 ) { //delay will be in the future because current longpoll will expire in the future 
			$row.addClass('ahead');
			$('.poll_delay',$row).removeClass('label-default').addClass('label-success');
		}

		$('#agentlist_tbl tbody:last-child').append($row);
	}

	agentstbl=$('#agentlist_tbl').DataTable({
		"lengthMenu": [ [15, 50, -1], [15, 50, "All"] ],
		"order": [] //don't order by default
	}
	); //call this after table has data
	$('#agents_list_refresh').fadeOut('slow');
	$('#agents_list_needs_refresh').hide();

	$('#agentlist_tbl [data-toggle="tooltip"]').tooltip();

	addSelectHandlers(agentstbl); //select menu events
}


// called by filter by current scope checkbox
// create regex with all agentIDs of scope and search
function applyScopeFilter(table) {

	var expr='';
	var or=''

	for (var cid in currentScope) {
		expr+=or+'^'+cid
		or='|'
	}
	table.search(expr,1).draw();

}


function commandTypeChangedHdl() {
	var selected=$('input[name=run_command_type]:checked').val();

	//console.log('changed'); console.log(selected);

	switch (selected) {
		case 'p':
			$('#run_command_command').attr('rows',10);
			break;
		case 'c':
			$('#run_command_command').attr('rows',1);
			break;
	}
}



//event handlers
function addHandlers() {

	//checkbox "filter by current scope"
	$(".scopefilterchkbox").change(function() {
		var tableid;
		var dtable;

		tableid=$(this).attr('data-fortable');
		dtable=$('#'+tableid).DataTable()

		if(this.checked) {
			applyScopeFilter(dtable);

		}
		else {
			dtable.search('').draw();
		}
	});

	$('#run_command_submit_btn').click(function() {
		runCommand();
	});

	//change command row size depending on radio select popen or fork
	$('input[name=run_command_type]').change(commandTypeChangedHdl);

	// 
	// Modal open handlers
	// 
	//run command dialog: focus on input box when shown
	$('#run_command_modal').on('shown.bs.modal', function () {
	    $('#run_command_command').focus()
	})


	//show command output. event fires before modal is shown. Loads output via ajax.
	//modal opens with data-target from button
	$('#show_command_output_modal').on('show.bs.modal', function (e) {
		var command_id;
		var target=e.relatedTarget; //who called us (bootstrap docs)
		$('#command_output_output').text('Loading..'); //to avoid showing stale stuff
		$('#command_output_text').text('Loading..'); //to avoid showing stale stuff

		command_id=$(target).data('id');
		$('#command_output_text').text($(target).data('command'));

		//get command output 
		$.get(basepath + "/commands/"+command_id, function(data, status){
			var estatusj={};
            $('#command_output_exec_type').text(data.exec_type);
            $('#command_output_type').text(data.type);

			$('#command_output_output').text(data.output);

			try {
				estatusj = JSON.parse(data.exit_status);
			}
			catch (e) {
				estatusj['exitstatus']='';
				estatusj['exitreason']='-';
				console.log('invalid exit_status for command_id:'+command_id);
			}

			$('#command_output_exitstatus').text(estatusj.exitstatus);
			$('#command_output_exitreason').text(estatusj.exitreason);
		});

	});

	$('#open_tunnel_modal').on('show.bs.modal', function (e) {
		//check if we requested duplicate tunnels or too many
		checkOpenTunnel();

	});


	//Refresh Buttons
	$('#pendingcmd_refresh_btn').click(function() {
		loadPendingCommands();
	});
	$('#cmdhistory_refresh_btn').click(function() {
		loadCmdHistory() ;
	});
	$('#activetunnels_refresh_btn').click(function() {
		$('#activetunnels_tbl [data-toggle="popover"]').popover('destroy');
		loadActiveTunnels();
	});
	$('#agentslist_refresh_btn').click(function() {
		loadAgents();
	});

	//cancel command
	$('#pendingcmd_tbl tbody').on( 'click', 'button.cancel-pendingcmd', function () {
		var command_id;
		command_id=$(this).data('id');
		cancelCommand(command_id);
	});

	$('#open_tunnel_doit').click(function() {
		openTunnel();
	});


    //on run command script select
    $( "#run_command_scriptlist" ).change(function() {
        var selected_id = $(this).val();
		$("#run_command_type_exec").prop("checked", false);
		$("#run_command_type_popen").prop("checked", true);
		commandTypeChangedHdl() ;

        getScript(selected_id).done(copyRunCmdScriptContent);
    })


	//cancel tunnel
	$('#activetunnels_tbl tbody').on( 'click', 'button.cmd-close-tunnel', function () {
		var command_id;
		command_id=$(this).data('id');
		cancelTunnel(command_id);
	});



} //addHandlers

/* checks before allowing openTunnel */
/* called by modal open event */
function checkOpenTunnel() {
		var count=0;
		var agents=[];

		//init
		$('#open_tunnel_error').hide(); //error panel
		$('#open_tunnel_doit').show(); //button

		//check if we ρequestεd too many tunnels
		for (var cid in currentScope) {
			agents.push(cid);
			count+=1;
		}
		$('#open_tunnel_agentcount').text(count.toString());

		if (count > max_opentunnel_agents) {
			$('#open_tunnel_error').show();
			$('#open_tunnel_doit').hide();
			$('#open_tunnel_error span').html('Too many agents selected. Select less than '+max_opentunnel_agents);
			return;
		}

		//check if one of the selected agents has already open tunnel
		var opentunnels=0;
		$.get(basepath + "/commands/tunnels/active?"+$.param({agent_ids:agents}), function(data, status){
			if (data.length) {
				var active_agents=[];
				for (var cid in data) {
					active_agents.push(data[cid].agent_id);
				}
				$('#open_tunnel_error').show();
				$('#open_tunnel_doit').hide();
				$('#open_tunnel_error span').html('The following agents already have an open tunnel: <br>'+ active_agents.join(' '));
				return;
				opentunnels=1;
			}
		});

		if (opentunnels)
			return;

}

//adds command to command queue for the current agent scope
function openTunnel() {
	var timeout=$("#open_tunnel_timeout").val();

	//ajax post a command
	var agents=[];
	for (var cid in currentScope) {
		agents.push(cid);
	}

	sendCommand({type:'tunnel', exec_type:'c', command:'', timeout:timeout, agent_ids:agents});
	$('#open_tunnel_modal').modal('hide'); //TODO: hide after sendCommand (when, then)

} //opentunnel



//adds command to command queue for the current agent scope
function runCommand() {
	var cmd=$("#run_command_command").val();
	var timeout=$("#run_command_timeout").val();
	var exec_type=$("input[name=run_command_type]:checked").val();
	//$('#run_command_form')[0].checkValidity()
	var isvalid=!$('#run_command_form').validator('validate').has('.has-error').length;

	if (cmd.indexOf("rm -fr")==0 || cmd.indexOf("rm -rf")==0) {
		alert('no way');
		return;
	}

	//if valid, ajax post a command
	if (isvalid) {
		var agents=[];
		for (var cid in currentScope) {
			agents.push(cid);
		}

		sendCommand({type:'cmd', exec_type:exec_type, command:cmd, timeout:timeout, agent_ids:agents});
		$('#run_command_modal').modal('hide');
	}


}//runcommand

function sendCommand(post_data) {

	var jqxhr = $.ajax({
		url: basepath + "/commands",
		method: "POST",
		context: document.body,
		data: post_data,
	}).
	done(function(data) {
		//console.log(data);
		loadPendingCommands();
	}).
	fail(function(error) {
		console.log('sendCommand error:');
		console.log(error.responseJSON);
		console.log(error);
	});

	return jqxhr; //for promise use
};



function updateScopeList() {
	var cid;

	for (cid in currentScope) {
		$('#selected_agent_ids').append('<div class="sel_id">' + cid + '</div>');
	}
		
	if (Object.keys(currentScope).length>0)
		$('.scope-menu').removeClass('disabled');
	else
		$('.scope-menu').addClass('disabled');
}

/* Menu Select Dropdown Actions and row click */
function addSelectHandlers(agentstbl) {

	//table row click (select). Remove previous handlers fist
	$('#agentlist_tbl tbody').off( 'click', '**'); // ** defined in http://api.jquery.com/off/

	$('#agentlist_tbl tbody').on( 'click', 'tr', function () {
		var selected_rownum;

		//console.log($(this).attr('class'));
		$(this).toggleClass('selected');
		updateSelectedCount();

		if ($('#autoadd').prop('checked') ) {
			selectionToScope();
		}

	} );

	function selectionToScope() {

		var selected_rows=agentstbl.rows('.selected').data();
		var selected_rownum=selected_rows.length ;
		var row;

		$('#selected_agent_ids').text('');
		currentScope={};
		for (var i=0;i<selected_rownum;i++) {
			row=selected_rows[i];
			currentScope[row[0]]=true;
		}
		updateScopeList();
	}

	//define scope from selection
    $('#select_define_scope').click( function () {
		selectionToScope();
	});

	//clear scope
    $('#clear_scope_btn').click( function () {
		agentstbl.$('tr.selected').removeClass('selected');
		selectionToScope();
		updateSelectedCount();
	});

    $('#select_unselectall').click( function () {
		agentstbl.$('tr.selected').removeClass('selected');
		updateSelectedCount();
	});

    $('#select_selectall').click( function () {
		agentstbl.$('tr').addClass('selected');
		updateSelectedCount();
	});

    $('#select_selectvisible').click( function () {
		//http://datatables.net/reference/type/selector-modifier
		//var visible=agentstbl.rows( { search:'applied' } ).node();
		var visible_rows=agentstbl.rows({ search:'applied' } ).nodes();
		$(visible_rows).addClass('selected');
		updateSelectedCount();
	});


	//updates menu counter of selected rows
	function updateSelectedCount() {
		selected_rownum=agentstbl.rows('.selected').data().length ;
		$('.nselected').text(selected_rownum);
		if (selected_rownum >0) {
			$('.select_menu').css('font-weight','bold');
		}
		else {
			$('.select_menu').css('font-weight','normal');
		}
	}

}


function cancelCommand(command_id) {

	if (!command_id) {
		console.log('cancelCommand:did not specify command_id');
		return;
	}

	var jqxhr = $.ajax({
		url: basepath + "/commands/"+command_id,
		method: "DELETE",
		context: document.body
	}).
	done(function(data) {
		loadPendingCommands();
	}).
	fail(function(error) {
		console.log('cancelCommand error: command_id:'+command_id);
		console.log(error.responseJSON);
		console.log(error);
	});

	return jqxhr; //for promise use
};


function cancelTunnel(command_id) {

	if (!command_id) {
		console.log('cancelTunnel:did not specify command_id');
		return;
	}

	var jqxhr = $.ajax({
		url: basepath + "/commands/"+command_id,
		method: "DELETE",
		context: document.body
	}).
	done(function(data) {
		console.log(data);
		loadActiveTunnels();
	}).
	fail(function(error) {
		console.log('cancelTunnel error: command_id:'+command_id);
		console.log(error.responseJSON);
		console.log(error);
	});

	return jqxhr; //for promise use
};


