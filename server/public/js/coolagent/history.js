
var cmdhistorytbl;


$(function() {
	loadCmdHistory();
	addHandlers();
});



//event handlers
function addHandlers() {

    //show command output. event fires before modal is shown. Loads output via ajax.
    //modal opens with data-target from button
    $('#show_command_output_modal').on('show.bs.modal', function (e) {
        var command_id;
        var target=e.relatedTarget; //who called us (bootstrap docs)
        $('#command_output_output').text('Loading..'); //to avoid showing stale stuff
        $('#command_output_text').text('Loading..'); //to avoid showing stale stuff

        command_id=$(target).data('id');
        $('#command_output_text').text($(target).data('command'));

        //get command output
        $.get(basepath + "/commands/"+command_id, function(data, status){
            var estatusj={};
            $('#command_output_output').text(data.output);
            $('#command_output_exec_type').text(data.exec_type);
            $('#command_output_type').text(data.type);

            try {
                estatusj = JSON.parse(data.exit_status);
            }
            catch (e) {
                estatusj['exitstatus']='';
                estatusj['exitreason']='-';
                console.log('invalid exit_status for command_id:'+command_id);
            }

            $('#command_output_exitstatus').text(estatusj.exitstatus);
            $('#command_output_exitreason').text(estatusj.exitreason);
        });

    });

	$('#history_export_btn').click(function () {
		window.location.href =  basepath + "/commands/export";
	});

}




function loadCmdHistory() {
    $('#cmd_history_refresh').show();
    var jqxhr = $.ajax({
        url: basepath + "/commands", //up to 1000 recent history in this page
        context: document.body,
        dataType: 'json'
    }).
    done(function(data) {
        //console.log(data);
        showCmdHistoryTable(data);
        $('#cmd_history_refresh').hide();
    }).
    fail(function(error) {
        console.log('loadCmdHistory error:');
        console.log(error.responseJSON);
    });

    return jqxhr; //for promise use
};


function showCmdHistoryTable(data) {
    var d=[],rowno,rowdata,c,colkeys,value;
    var estatusj;

    if (cmdhistorytbl != null) {
        cmdhistorytbl.destroy();
        $('#cmdhistory_tbl tbody >tr').remove();
    }

    for (rowno in data) {
        rowdata=data[rowno];
        $row=$('<tr/>');

        for (c in rowdata) {
            value=rowdata[c];
            if (c == 'output') { //truncate command output (which is already truncated by sqlite on this call)
                $row.append( $('<td  title="'+value.encodeDQ()+' …">').text(value.trunc(19)).addClass(c));
            }
            else if (c == 'cmd') {
                $row.append( $('<td  title="'+value.encodeDQ()+' …">').text(value.trunc(15)).addClass(c).attr('data-search',value.encodeDQ()) );
            }
            else if (c == 'time_requested')  {
                $row.append( $('<td>').html(epoch2date(value)).addClass(c).attr('data-sort',encodeURI(value)));
            }
            else if (c == 'time_returned')  {
                $row.append( $('<td>').html(epoch2date(value)).addClass(c).attr('data-sort',encodeURI(value)));
            }
            else if (c == 'exit_status')  {
                try {
                    estatusj = JSON.parse(rowdata['exit_status']);
                }
                catch (e) {
                    estatusj={'exitstatus':'','exitreason':'-'};
                    console.log('invalid exit_status for command id:'+rowdata['id']);
                    console.log(e);
                }
                $row.append( $('<td>').html(estatusj.exitstatus).addClass(c));
            }
            else {
                $row.append( $('<td>').html(value).addClass(c));
            }
        }
        //"show output" button:
        $row.append( $('<td>').addClass('action').html(
        '<button data-target="#show_command_output_modal" data-toggle="modal"  ' +
        ' data-id="' + rowdata['id'] + '" ' +
        ' data-command="' + rowdata['cmd'].encodeDQ() + '" ' +
        ' data-output="' + rowdata['output'].encodeDQ() + '" ' +
        ' class="btn btn-xs btn-success hist-show-output">Output</button>'));


        //if (rowdata['exit_status'] == -9999) {
        if (estatusj.exitstatus == -9999) {
            $row.addClass('warning');
        }
        else if (estatusj.exitstatus != 0) {
            $row.addClass('danger');
        }

        $('#cmdhistory_tbl tbody:last-child').append($row);
    }

    cmdhistorytbl = $('#cmdhistory_tbl').DataTable({
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "order": [] //don't order by default
        //"ordering": false //disable ordering
    });

}


