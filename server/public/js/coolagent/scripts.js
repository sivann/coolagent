
//var agents; //populated by agents table

var script_editor;

/* Agents Panel */

$(function() {
	loadScripts();
	addHandlers();
	initEditor();
});


function loadScripts() {
	getScripts().done(populateScriptList);
}


//ACE Editor https://ace.c9.io
function initEditor() {
    script_editor = ace.edit("script_editor");
    script_editor.setTheme("ace/theme/cobalt");
    script_editor.getSession().setMode("ace/mode/sh");
	//script_editor.setOptions({ maxLines: 40, minLines: 30 });
	script_editor.setValue('');
}

/* SCRIPTS */

function populateScriptList(scripts) {
	var $select=$('#script_list');
	$select.empty();
	$.each(scripts, function(key, val){
		var tm = epoch2date(val.time_modified);
		$select.append('<option value="' + val.id + '">[' + tm + '] ' + val.title + '</option>');
	})
}

function saveScript(script_id,put_data) {
	if (!script_id) {
		console.log('saveScript:did not specify script_id');
		return;
	}

    var jqxhr = $.ajax({
        url: basepath + "/scripts/"+script_id,
        method: "PUT",
        context: document.body,
		data: put_data,
        dataType: 'json'
    }).
    done(function(data) {
		if (data && data.status && data.status=='error') {
			$('#script_msg').show();
			$('#script_msg').text(data.message);
		}
		else
			$('#script_msg').hide();
    }).
    fail(function(error) {
        console.log('saveScript error:');
        console.log(error.responseJSON);
    });

    return jqxhr; //for promise use
}

function enableScriptDelBtn() {
	$('#delete_script_btn').prop('disabled',false);
}

function addScript(post_data) {
	if (!post_data) {
		console.log('addScript:did not specify script_id');
		return;
	}

    var jqxhr = $.ajax({
        url: basepath + "/scripts",
        method: "POST",
        context: document.body,
		data: post_data,
        dataType: 'json'
    }).
    done(function(data) {
		if (data && data.status && data.status=='error') {
			$('#script_msg').show();
			$('#script_msg').text(data.message);
		}
		else
			$('#script_msg').hide();

    }).
    fail(function(error) {
        console.log('addScript error:');
        console.log(error.responseJSON);
    });

    return jqxhr; //for promise use
}

function getScript(script_id) {

    if (!script_id || !isNumber(script_id)) {
        console.log('getScript:invalid script_id' + script_id);
        return;
    }

    var jqxhr = $.ajax({
        url: basepath + "/scripts/"+script_id,
		method: "GET",
        context: document.body,
    }).
    fail(function(error) {
        console.log('getScript error: script_id:'+script_id);
        console.log(error.responseJSON);
        console.log(error);
    });

    return jqxhr; //for promise use
}

function showScriptContent(data) {
	if (data.status && data.status=='error') {
		console.log('showScriptContent: ERROR');
		console.log(data);
		$('#script_msg').show();
		$('#script_msg').text('');
		if (data.message)
			$('#script_msg').text(data.message);

		return;
	}
	else
		$('#script_msg').hide();

	$('#script_title').val(data.title);
	$('#script_id').val(data.id);
	$('#script_content').val(data.content);

	script_editor.setValue(data.content);

	var contenthead=data.content.substring(0,30);
	if (contenthead.indexOf('python')>-1) {
		script_editor.getSession().setMode("ace/mode/python");
	}
	else if (contenthead.indexOf('sh')>-1) {
		script_editor.getSession().setMode("ace/mode/sh");
	}
	else if (contenthead.indexOf('php')>-1) {
		script_editor.getSession().setMode("ace/mode/php");
	}
	else if (contenthead.indexOf('html')>-1) {
		script_editor.getSession().setMode("ace/mode/html");
	}
	else
		script_editor.getSession().setMode("ace/mode/sh");
	script_editor.gotoLine(1);
	script_editor.scrollToLine(0);
	script_editor.focus();
}


function deleteScript(script_id) {

    if (!script_id) {
        console.log('deleteScript:requested delete without specifying script_id');
        return;
    }

    var jqxhr = $.ajax({
        url: basepath + "/scripts/"+script_id,
		method: "DELETE",
        context: document.body,
    }).
    done(function(data) {
        //showAgentsTable(data);
		loadAgents();
    }).
    fail(function(error) {
        console.log('deleteScrpt error: script_id:'+script_id);
        console.log(error.responseJSON);
        console.log(error);
    });

    return jqxhr; //for promise use
}

function getScripts() {

    var jqxhr = $.ajax({
        url: basepath + "/scripts",
        context: document.body,
        dataType: 'json'
    }).
    done(function(data) {
    }).
    fail(function(error) {
        console.log('loadScripts error:');
        console.log(error.responseJSON);
    });

    return jqxhr; //for promise use
}


function populateAgentList(agents) {
	var $select=$('#agent_list');
	$select.empty();
	$select.append('<option value="">-</option>');
	$.each(agents, function(key, val){
		$select.append('<option value="' + val.agent_id + '">' + val.agent_id + '</option>');
	})

}


function addHandlers() {
	$('#script_title').keypress(function(event){
		if (event.keyCode === 10 || event.keyCode === 13) 
			event.preventDefault();
	});

	$("#new_script_btn").click(function() {
		$('#script_title').val('');
		$('#script_id').val('');
		$('#script_content').val('');
		script_editor.setValue('');
		$("#script_list option:selected").prop("selected", false);
	});


	$("#delete_script_btn").confirm({
		text: "Are you sure you want to delete this script ?",
		title: "Confirmation required",
		confirm: function(button) {
			var script_id=$('#script_id').val();

			deleteScript(script_id).done(loadScripts) ;
			$("#script_list option:selected").prop("selected", false);
			$('#script_title').val('');
			$('#script_id').val('');
			$('#script_content').val('');
			script_editor.setValue('');

		},
		cancel: function(button) {
			// nothing to do
		}
	});


	$("#save_script_btn").click(function(e) {
		//e.preventDefault();
		var script_id = $('#script_id').val();
		var script_data={};

		script_data.title=$('#script_title').val();
		script_data.id=$('#script_id').val();
		script_data.content=$('#script_content').val();
		script_data.content=script_editor.getValue();

		//add
		if (isNumber(script_id)) {
			saveScript(script_id,script_data).done(loadScripts);
			//loadScripts();
		}
		else { //save existing
			addScript(script_data).done(loadScripts);
		}
	});



	//on script select
	$( "#script_list" ).change(function() {
		var selected_id = $(this).val();
		getScript(selected_id).done(showScriptContent).done(enableScriptDelBtn);
	})
}



