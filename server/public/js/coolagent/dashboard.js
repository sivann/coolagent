/* dashboard.js */

var agents; //populated by agents table

var localCache = {
	data: {},
    exist: function (o) {
        return localCache.data.hasOwnProperty(o) && localCache.data[o] !== null;
    },
    remove: function (o) {
        delete localCache.data[o];
    },
    set: function (o, cachedData, callback) {
        localCache.remove(o);
        localCache.data[o] = cachedData;
        if ($.isFunction(callback)) callback(cachedData);
    },
    get: function (o) {
        console.log('Getting in cache for ' + o);
        return localCache.data[o];
    }
}


/* Google Charts */
function chartsInit() {
	// https://developers.google.com/chart/interactive/docs/gallery/histogram
	// Load the Visualization API and the corechart package.
	google.charts.load("current", {packages:["corechart",'calendar']});
	google.charts.setOnLoadCallback(statusHistDraw);  //histogram
	//google.charts.setOnLoadCallback(statusMapDraw); //geoMap

	statusMapDraw(); //This is google maps, not geoMaps

	google.charts.setOnLoadCallback(averageCalendarChartDraw); //geoMap
}

//called when google API is loaded
function statusHistDraw() {
	var options = {
	  title: 'Current agent state histogram, in days',
	  legend: { position: 'none' },
	  animation: {startup: true },
	  fontSize: 9,
	  hAxis: {
		  title: 'Days Offline'
	  },
	  vAxis: {
		  title: '# Agents'
	  },
	  histogram: { 
		  bucketSize: 7, // days
		  maxNumBuckets: 20,
		  lastBucketPercentile: 10
		  }
	};

	var histogram_visualization = new google.visualization.Histogram(document.getElementById('agentstatus_histogram'));

	getAgentsStatus().done(function(d) {
		var d1=[]
		var j,row;
		var r;


		//
		//convert object to array of arrays
		d1.push(['Agent ID', 'Days Offline']);

		for (j in d) {
			row=d[j];
			d1.push([row['agent_id'], row['age_hours']/24 ]);
		}
		var data = google.visualization.arrayToDataTable(d1);
		histogram_visualization.draw(data, options);
	});

}

function showCurrentAgentStats(d) {
	var total=0;
	var available=0;

	for (j in d) {
		total++;
		row=d[j];
		if (row['age_hours']==0) {
			available++;
		}
	}
	$('#stat_agents_total').text(total);
	$('#stat_agents_available').text(available);

}

//called when google API is loaded
function statusMapDraw() {
	var options = {
	  title: 'Current agent state geomap',
	  showTip: true,
	  useMapTypeControl: true,
	  zoomLevel: 1,
	  mapType: 'normal'
	};

	var mapOptions = {
	  zoom: 2,
	  center: {lat: 30.0, lng: 0.0},
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var gm = google.maps;
	var map = new gm.Map(document.getElementById('agentstatus_map'), mapOptions);

	var overlapOptions= {
		keepSpiderfied: true
	}
	var oms = new OverlappingMarkerSpiderfier(map,overlapOptions); //allows multiple markers on the same spot to spread out on click

	var iw = new gm.InfoWindow();
	oms.addListener('click', function(marker, event) {
	  iw.setContent(marker.desc);
	  iw.open(map, marker);
	});

	oms.addListener('spiderfy', function(markers) {
	  iw.close();
	});

	getAgentsGeoStatus().done(function(d) {
		var d1=[]
		var j,row;
		var lat,lon;

		for (j in d) {
			row=d[j];

			lat = row['latitude'];
			lon = row['longitude'];

			//var datum = window.mapData[i];
			var loc = new gm.LatLng(lat, lon);
			var marker = new gm.Marker({
				position: loc,
				title: row.agent_id,
				map: map
			});
			marker.desc = row.agent_id + ', contacted: '+row.age_days +' days ago';
			if (row.age_days>1)
				marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red.png')
			else
				marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green.png')
			oms.addMarker(marker);  // 

		}

		/*
		var bounds = new gm.LatLngBounds();
		map.fitBounds(bounds);
		*/
	});


}


//called when google API is loaded
function statusChartDraw() {
	var options = {
	  title: 'Current agent state geochart',
	  animation: {startup: true },
	  displayMode: 'markers',
	  enableRegionInteractivity: true,
	  region: 'world',
	  colorAxis: {
		  minValue: 0,   //lower than this will be 1st color
		  colors: ['#00FF00', '#FF0000']
		  }
	};

	var geochart_visualization = new google.visualization.GeoChart(document.getElementById('agentstatus_geochart'));

	getAgentsGeoStatus().done(function(d) {
		var d1=[]
		var j,row;
		var lon;
		
		//convert object to array of arrays
		d1.push(['Country', 'Days Offline']);

		for (j in d) {
			row=d[j];
			d1.push([row['country'] + ', ' + row['city'] , row['age_days'] ]);
		}
		//console.log(d1);
		var data = google.visualization.arrayToDataTable(d1);
		geochart_visualization.draw(data, options);
	});

}



//show a specific agent calendar status
function statusCalendarDraw(d,id,description) {
	var options = {
	   title: "Availability, "+description,
	   height: 600,
	   calendar: { cellSize: 15 },
	   colorAxis: {
		   minValue: -1,  
		   maxValue: 1,  
		   colors: ['#D9534F','#bbb', '#5CB85C'],
		   },
		noDataPattern: {
			backgroundColor: '#aaa',
			color: '#fff'
		}
	};

	var calendar_visualization = new google.visualization.Calendar(document.getElementById(id)); 

	var d1=[]
	var j,row;
	var lon;
	var d_isup;

	//convert object to array of arrays
	d1.push(['Date', 'Days Offline']);

	if ((d.data && d.data.isup)) {
		d_isup=d['data']['isup'];
	}

	for (epoch in d_isup) {
		var isup;
		isup=d_isup[epoch];
		var ndt=new Date(epoch*1000)
		d1.push([ndt, isup]);
	}

	var data = google.visualization.arrayToDataTable(d1);
	calendar_visualization.draw(data, options);
}




/* Agents Panel */

$(function() {
    loadAgents().done(fillStatusTable);
    addHandlers();

	fillStatusTable(agents); //sparklines table

	chartsInit(); //google charts 

});



function loadAgents() {
    return getAgents().done(populateAgentList);
}

function populateAgentList(agents) {
    var $select=$('#agent_list');
    $select.empty();
    $select.append('<option value="statsAVERAGE">AVERAGE</option>');
    $.each(agents, function(key, val){
        $select.append('<option value="' + val.agent_id + '">' + val.agent_id + '</option>');
    })

}

function getAgentsGeoStatus() {

    var jqxhr = $.ajax({
        url: basepath + "/agents/geostatus",
        context: document.body,
        dataType: 'json'
    }).
    done(function(data) {
    }).
    fail(function(error) {
        console.log('getAgentsGeoStatus error:');
        console.log(error.responseJSON);
    });

    return jqxhr; //for promise use
}


function getAgentsStatus() {
/*
	if (localCache.exist('agentsstatus')) {
		var x = function() {
			function done(d1) = {

			}
		}
		var d = localCache.get('agentsstatus',d);
		return x;
	}
*/

    var jqxhr = $.ajax({
        url: basepath + "/agents/status",
        context: document.body,
        dataType: 'json'
    }).
    done(function(data) {
		localCache.set('agentsstatus',data);
		showCurrentAgentStats(data)
    }).
    fail(function(error) {
        console.log('getAgentsStatus error:');
        console.log(error.responseJSON);
    });

    return jqxhr; //for promise use
}


function getAgents() {

    var jqxhr = $.ajax({
        url: basepath + "/agents",
        context: document.body,
        dataType: 'json'
    }).
    done(function(data) {
        agents=data;
    }).
    fail(function(error) {
        console.log('loadAgents error:');
        console.log(error.responseJSON);
    });

    return jqxhr; //for promise use
}

function getAgentRRD(agent_id,date_start,date_end,resolution_sec){
	var getparams='';

    if (!agent_id) {
        console.log('Requested agent RRD without specifying agent_id');
        return;
    }

    if (!date_start) 
		date_start='';

    if (!date_end) 
		date_end='';

    if (!resolution_sec) 
		resolution_sec='';

    var jqxhr = $.ajax({
        url: basepath + "/agents/"+agent_id+'/stats?'+$.param({date_start:date_start, date_end:date_end, resolution_sec:resolution_sec}),
        context: document.body,
        dataType: 'json'
    }).
    done(function(data) {
		if (data.status == 'error') {
			console.log(data.message);
		}
    }).
    fail(function(error) {
        console.log('getAgentRRD error:'+agent_id);
        console.log(error.responseJSON);
    });

    return jqxhr; //for promise use

}

//Function closure to pass extra arguments (the div ID) to the done promise
var showSparkDoneHandler = function(param) {
    return function(data, textStatus, jqXHR) {
		$('#ss_'+param).sparkline(data.seqvalues, {type: 'tristate', barWidth: '1'} );
    };
};

//sparklines table
function fillStatusTable(agents) {
	var agent_id,i;
	$('#statuses_tbl > tbody').empty();
	//console.log(agents);

	for (i in agents) {
		//console.log('i='+i);
		agent_id = agents[i]['agent_id'];


		$('#statuses_tbl > tbody:last-child').append('<tr><td>'+agent_id+'</td><td> <span id="ss_'+i+'">-</span></td></tr>\n');
		getAgentRRD(agent_id).done(showSparkDoneHandler(i));

		/*		
		//This won't work because by the time done gets called, all i would be the last i (have the same value) for all invocations
		getAgentRRD(agent_id,i).done(function(d) {
			console.log('i='+i)
			//$('#ss_'+i).sparkline(d.seqvalues, {type: 'tristate', barWidth: '3'} );
		});
		*/
	}
}

function addHandlers() {

	$('#agent_hist_calendar_show').click(function() {
		var agent_id, rrd_values;
		var agent_id=$('#agent_list').val();
		if (agent_id)  {
			//here
			var date_start=$('#agent_hist_calendar_datestart').val();
			var date_end=$('#agent_hist_calendar_dateend').val()
			var resolution_sec=$('#agent_hist_calendar_resolutionsec').val()
			getAgentRRD(agent_id,date_start,date_end,resolution_sec).done(function(d) {
				statusCalendarDraw(d,'agent_statuscalendar',agent_id);
			});
		}
	});


	$('#agent_hist_spark_show').click(function() {
		var agent_id, rrd_values;
		var agent_id=$('#agent_list').val();
		if (agent_id)  {
			var date_start=$('#agent_hist_calendar_datestart').val();
			var date_end=$('#agent_hist_calendar_dateend').val()
			var resolution_sec=$('#agent_hist_calendar_resolutionsec').val()

			getAgentRRD(agent_id,date_start,date_end,resolution_sec).done(function(d) {
				var drange='';
				$('#agent_sparkstate').sparkline(d.seqvalues, {type: 'tristate', barWidth: '1'} );
				drange=epoch2date(d.start) + ' - ' + epoch2date(d.end);
				$('#agent_sparkstate_daterange').text(agent_id+' '+drange);
			});
		}
	});

}

//draw the calendar availability chart for the "average" agent 
function averageCalendarChartDraw() {
	var agent_id="statsAVERAGE";
	var date_start='end-1year';
	var date_end='now';
	var resolution_sec=86400
	var description="average of agents contacted last month";

	getAgentRRD(agent_id,date_start,date_end,resolution_sec).done(function(d) {
		statusCalendarDraw(d,'agent_average_statuscalendar',description);
	});
}

