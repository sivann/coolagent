CREATE TABLE agent (
    agent_id TEXT PRIMARY KEY,
    last_poll_ts INTEGER, -- last poll time
    poll_period INTEGER,
    last_ip TEXT,
    salt_id TEXT, -- with index
    nodename TEXT, -- from uname(2)
    machine TEXT, -- arch
    release TEXT, -- o/s release
    info TEXT,  -- json with various system grains (os, version, etc)
    longpoll_period INTEGER, 
    password TEXT
, time_registered INTEGER);
CREATE TABLE command (
    id INTEGER PRIMARY KEY,
    agent_id TEXT, -- agent.id
    exec_type INTEGER, -- C,P
    cmd TEXT,
    timeout INTEGER,
    exit_status INTEGER, --default null
    output TEXT,
    time_requested INTEGER, -- requested from interface
    time_communicated INTEGER, -- set when the agent got it via poll
    time_returned INTEGER, -- set when agent returned results
    type TEXT, --cmd/tunnel/scenario
    tunnel_port INTEGER,
    tunnel_active INTEGER, group_tag TEXT, -- t/f
    FOREIGN KEY(agent_id) REFERENCES agent(id)
);
CREATE TABLE agent_scope (
    id INTEGER PRIMARY KEY,
    agent_ids TEXT, -- json array
    description TEXT
);
CREATE TABLE table_ts (
    command_ts TEXT,
    agent_ts TEXT
);
CREATE TRIGGER update_table_ts_command_trigger 
after update on command 
BEGIN 
    update table_ts set 'command_ts' = strftime('%s', 'now') || '-' || hex(randomblob(5));
END;
CREATE TRIGGER insert_table_ts_command_trigger 
after insert on command 
BEGIN 
    update table_ts set 'command_ts' = strftime('%s', 'now') || '-' || hex(randomblob(5));
END;
CREATE TRIGGER delete_table_ts_command_trigger 
after delete on command 
BEGIN 
    update table_ts set 'command_ts' = strftime('%s', 'now') || '-' || hex(randomblob(5));
END;
CREATE TRIGGER update_table_ts_agent_trigger 
after update on agent 
BEGIN 
    update table_ts set 'agent_ts' = strftime('%s', 'now') || '-' || hex(randomblob(5));
END;
CREATE TRIGGER insert_table_ts_agent_trigger 
after insert on agent 
BEGIN 
    update table_ts set 'agent_ts' = strftime('%s', 'now') || '-' || hex(randomblob(5));
END;
CREATE TRIGGER delete_table_ts_agent_trigger 
after delete on agent 
BEGIN 
    update table_ts set 'agent_ts' = strftime('%s', 'now') || '-' || hex(randomblob(5));
END;
CREATE INDEX command_time_returned_idx on command(time_returned);
CREATE INDEX command_type_idx on command(type);
CREATE INDEX command_tunnel_active_idx on command(tunnel_active);
CREATE INDEX command_time_communicated_idx on command(time_communicated);
CREATE INDEX command_group_tag_idx on command(group_tag);
CREATE TABLE script(
    id INTEGER PRIMARY KEY,
    title TEXT,
    content TEXT
, time_modified INTEGER);
