#!/bin/bash

echo "Updating geoIP DB (about 60MB)"

set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd ${DIR}/../data/geoIP/

curl http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz|gunzip > GeoLite2-City.mmdb
