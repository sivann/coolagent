#!/usr/bin/python

# run periodically (every 25 minutes), and update rrd-stats .rrd with current agent status
#
# The following is performed:
# 1. fetches list of agent_ids and their status (HTTP REST call)
# 2. compare agent_ids from db and files and return a list of agent_ids without RRD files
# 3. creates missing RRDs 
# 4. updates all RRD files (directly)

#TODO: 
# * global rrd file containing average status of all agents
# * why is this in python and not php as the rest?

import json
import os
import sys
import glob
import numpy
import argparse

from pprint import pprint
import pycurl
from StringIO import StringIO

import rrdtool

options = {
	'API_URL_BASE':'http://127.0.0.1/~sivann/coolagent/server/public',
	'rrd_basepath':'../data/rrd-stats',
	'grace_sec':60,
        'avg_ignoreolderthan':2592000, # when calculating average status of all agents, ignore agents older than  (sec)
        'avgagent_id':'statsAVERAGE' #name of "agent" containing average RRD values
}

#Globals
agent_data= []

parser = argparse.ArgumentParser(description='Update rrd files. Run periodically.')
parser.add_argument('--urlbase', dest='urlbase', default=options['API_URL_BASE'], help='REST URL basepath (e.g. https://coolagent.example.com)')
args = parser.parse_args()


def getAgentData():
    global agent_data, options, args
    #url=options['API_URL_BASE']+'/agents'
    url=args.urlbase+'/agents'
    
    if not len(agent_data):
	buffer = StringIO()
	c = pycurl.Curl()
	c.setopt(c.URL, url)
	#c.setopt(c.WRITEDATA, buffer) #needs curl > 7.19.3
        c.setopt(c.WRITEFUNCTION, buffer.write)
	c.perform()
        status_code=c.getinfo(pycurl.HTTP_CODE)
        if status_code > 400:
            print url
            print 'getAgentData:returned ',status_code
            print buffer.getvalue()
            sys.exit(1)
	c.close()

	body_string = buffer.getvalue()
	agent_data = json.loads(body_string)

    return agent_data

def getAgentIDsFromData(agent_data):
    agent_data = getAgentData();
    agent_ids = {}
    #convert to dict for fast lookups
    for item in agent_data:
	agent_ids[item['agent_id']]=1
    return agent_ids

def getRRDFiles(rrd_basepath):
    #rrd_files = [f for f in os.listdir(rrd_basepath) if os.path.isfile(os.path.join(rrd_basepath, f))]
    #glob.iglob('*.rrd')
    listpath = rrd_basepath+'/*'
    rrd_files = glob.glob(listpath)
    rrd_files = [os.path.basename(f) for f in rrd_files]

    #convert to dict for fast lookups
    fdict = {}
    for f in rrd_files:
	    fdict[f]=1
    return fdict

#compare agent_ids from db and files and return a list of agent_ids without RRD files
def findRRDMismatch(rrd_basepath):
    global options

    agent_data = getAgentData()
    agent_ids = getAgentIDsFromData(agent_data)
    rrd_files = getRRDFiles(rrd_basepath)

    agent_ids[options['avgagent_id']]=1 #append the statistics agent, so it gets created if it's missing

    missing_rrd =  [obj for obj in agent_ids if obj+'.rrd' not in rrd_files]
    unused_rrd =  [obj for obj in rrd_files if obj not in agent_ids]

    return {'missing_rrd':missing_rrd, 'unused_rrd':unused_rrd}


#update rrd files
def updateRRD(agentData):
    global options

    for agent in agent_data:
	age_sec = int(agent['age_sec'])
	if age_sec > options['grace_sec']: #give some 30sec grace for the post to complete
	    status = '0'
	else:
	    status = '1'
	rrdfile=options['rrd_basepath']+'/'+agent['agent_id']+'.rrd'
	value = 'N:'+status
	print 'Updating '+rrdfile + ' with '+value
	rrdtool.update(str(rrdfile), value)

#create rrd files of supplied agents for the 1st time
def createRRD(agents):
    global options
    for agent_id in agents:
	rrdfile=options['rrd_basepath']+'/'+agent_id+'.rrd'
	print 'Creating NEW RRD ',rrdfile
	rrdtool.create(str(rrdfile), '--start', 'now', '--step', '1800', 
	    'DS:isup:GAUGE:1800:0:1',
	    #'DS:perc95:COMPUTE:data,95,PERCENT', #fails
	    'RRA:AVERAGE:0.8:1:1200', #25 days, 1 sample/30min
	    'RRA:AVERAGE:0.8:6:1440', #180 days, 1 sample/3h (6*1800)
	    'RRA:AVERAGE:0.8:48:1095' #3yr, 1 sample/24h
	    )

#updates rrd values of statistics agent 
def updateStatAgentsRRD(agentData):
    global options

    status_mean=0;
    count=0
    statuses=[]
    for agent in agent_data:
	count+=1
	age_sec = int(agent['age_sec'])
	if age_sec > options['grace_sec']: 
	    status = 0 #int
	else:
	    status = 1

	if age_sec < options['avg_ignoreolderthan']: #1 month
	    statuses.append(status)

    avgstatus = numpy.average(numpy.array(statuses))
    agentAVG={}
    agentAVG['agent_id'] = options['avgagent_id']
    agentAVG['age_sec'] = avgstatus
    agentData.append(agentAVG);

    rrdfile=options['rrd_basepath']+'/'+agentAVG['agent_id']+'.rrd'
    value = 'N:'+str(avgstatus)
    print 'Updating '+rrdfile + ' with '+value
    rrdtool.update(str(rrdfile), value)


mismatched = findRRDMismatch(options['rrd_basepath'])
createRRD(mismatched['missing_rrd'])
#pprint (agent_data)
updateRRD(agent_data);

updateStatAgentsRRD(agent_data) 
