# ys-server

This is a project written in  Slim 3 that includes Twig, Flash messages and Monolog.

## Install:
mkdir log cache
chown apache log cache data data/ys.db


## Update components and dependencies:
./composer.phar update (this will create the vendor/ directory)

## Key directories

* `app`: Application code
* `app/src`: All class files within the `App` namespace
* `app/templates`: Twig template files
* `cache/twig`: Twig's Autocreated cache files
* `log`: Log files
* `public`: Webserver root
* `vendor`: Composer dependencies

## Key files

* `public/index.php`: Entry point to application
* `app/templates/`: Twig html templates 
* `app/settings.php`: Configuration
* `app/dependencies.php`: Services for Pimple DI
* `app/middleware.php`: Application middleware
* `app/routes.php`: All application routes are here

