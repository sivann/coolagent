# Cool Agent #

![walrus-150.jpg](https://bitbucket.org/repo/7yxyGA/images/386598832-walrus-150.jpg)


* Poll a server periodically for commands to run via HTTP(S) GET
* Return results asynchronously via HTTP(S) POST

## Agent ##

Configured via coolagent.ini

```
#!none

Usage: coolagent [-d] [-p] [-c ini_file]
	-d		debug
	-p		http protocol debug
	-c [ini_file]	path of coolagent.ini
	-r	replace: terminate current lockfile-holding process and run

```


### coolagent.ini ###

```
#!ini


[coolagent]
;server URL to poll for command and post results to
cmd_server_url=https://coolagent.servername.com/

;Enable HTTP TLS CA Verification 1: true, 0: false
;Determines whether libcurl verifies the authenticity of the peer's certificate
ssl_certificate_verify_peer=1

;verify the certificate's name against host, i.e. the name in the certificate must match the URL you operate against.
ssl_certificate_verify_host=0

;If https_certificate_verify==1, provide an alternative path for CA. Leave blank for default.
ssl_certificate_capath=

;ssh binary to replace for the #ssh_client# placeholder in commands
ssh_client=/usr/bin/ssh -o StrictHostKeyChecking=no -o ServerAliveInterval=50
;dropbear: -K 50 -t (or -T?)


;maximum number of commands that can run simultaneously (Default:1)
max_children=2

;send this ID string with each poll; must be unique
agent_id=agent-id.my.server

;poll for commands period (seconds) (default: 900)
poll_period=10

;fast poll period (default: 60)
fast_poll_period=5

;how many fast poll loops to perform after each C or P command (default:10)
fastpolls_implicit=10

;how many fast poll loops to perform after each A (attention) command (default:30)
fastpolls_explicit=30

;longpoll value is communicated to the server on 1st HTTP GET. if longpoll_seconds == 0 
;server will not block "get next command" requests, and will return 
;NOOP if no command is pending.
;
;if longpoll_seconds > 0 server will block new command requests for longpoll_seconds 
;(and return NOOP) and will return as soon as a new command is available for this agent. 
;
;Longpoll mode minimizes HTTP requests, and makes command response immediate but may be less
;reliable when TCP connections are unreliable, and uses more server resources since all clients 
;keep an established connection.
;If you choose longpoll, poll_period and fast_poll_period should be set to something short like 1-2 seconds
;This will also set the HTTP GET timeout (via libcurl). if longpoll_timeout==0 HTTP GET timeout is set to 
;a default of 60 seconds.
;Default: 0
longpoll_period=600

;additional_info: additional info you want to display/search for on the server
; if string starts with | then the rest is executed with popen and its result is assigned to the additional_info
; e.g.:
; additional_info=9884c2dc-9fd7-4c18-b570-5843cde3c5e2
; additional_info=|cat /etc/salt/minion.d/id.conf|awk '{print $NF}'
additional_info=|cat /etc/salt/minion.d/id.conf|awk '{print $NF}'

```

## How it Works ##

### Protocol ###
Protocol Workflow
agent communication:

* agent starts and requests next command from server
    *  on first ever request, agent issues a "register" request. The server adds the agent data if the agent is unregistered (has no password) and returns a random password. This password is saved by the agent to password_file (coolagent.pass) and is used on the agent’s subsequent requests (via a cookie).
    *  on first request after running, agent issues a "full update", i.e also publishes its settings/arch/periods/etc on the GET parameters (as base64 encoded json). Subsequent requests contain only the agent_id, for bandwidth saving.
* server accepts immediately this request, but does not return: polls in the background periodically (5 seconds) the DB for new commands for a total of “longpoll_period” seconds (e.g. 1800). Each agent can set its own longpoll period. When new commands appear for this agent, server returns next command to the agent. If longpoll_period expires, servers return NOOP command
    *  if the agent disconnects (due to network, restart,etc) during longpolling, while waiting for next command, the servers detects that (through netstat checking of remote agent’s TCP port) and stops the DB query loop, so when the agent reconnects again it gets the correct next command.
*  If a command is returned to the agent from the server, it is executed through either fork/popen or a double fork (first one to grab command’s output and exit status and second to exec() the actual command). The 1st forked child POSTs the command output (and exit status) to the server. 
    * If the command is a tunnel, the 1st forked child uppon executing the ssh port forwarding, notifies the server (POST) of the port bound by the SSH daemon (by grabbing ssh output). The server adds this info on the commands table, port column (POST to /agents/{agent_id}/commands/{command_id}/tunnelport') and marks tunnel as active.
* agent waits for poll_period, and reissues a GET nextcommand request, even if a command is currently running. Up to max_children commands can be spawned in parallel.


https://docs.google.com/document/d/1gf6XEXvoe5VxJQvE__sgAtkKtuwTYoFB7fWpLFFbrqU

### REST API ###


```
#!none
REST interface:

GET /
GET /ui/dashboard/
GET /ui/history/
GET /ui/maintenance/
GET /ui/agents/

GET /software/agent/{arch}
GET /software/ini/random #returns an .ini with a random agent_id
GET /software/ini/{your registered agent_id here} #returns an .ini containing the correct agent_id, the last defined longpoll_period

GET /agents
GET /agents/status
GET /agents/geostatus
GET /agents/{agent_id}/stats
GET /agents/{agent_id}/registration
DELETE /agents/{agent_id}/registration
GET /agents/{agent_id}/nextcommand
POST /agents/{agent_id}/commands/{command_id}/tunnelport
POST /agents/{agent_id}/commands/{command_id}/result
DELETE /agents/{agent_id:\d+}

GET /commands/pending
GET /commands/export
GET /commands
POST /commands
GET /commands/{command_id}
DELETE /commands/{command_id}
GET /commands/tunnels/active

GET /server/misc/initdata
GET /server/misc/changedtables

GET /scripts
GET /scripts/{script_id}
POST /scripts
PUT /scripts/{script_id}
DELETE /scripts/{script_id}

```




### Command results ###
Command results are returned from the agent by POST. The following (POST) parameters are set:

```
#!none
    [data] => command output
    [exitstatus] => command exit status
    [exitreason] => command exit reason
```

The POST is submitted to a URL which includes the following GET parameters:

```
#!none
    [action] => the string 'result'
    [cmd_id] => the random command id set by server this command was requested
```


### Commands ###
The server issues a command as a reply to the GET request using the format below. 
The 1st line is the header, subsequent lines are the command to run.

```
#!

<coolagent_cmd> <timeout> <cmd_id>
<command to run>

```

~~~~ .txt
coolagent_cmd:
C: fork->fork/exec command (this is preferred)
P: fork->popen command
N: No command follows (NOOP)
A: attention (perform faster poll loop)
E: Error, next line contains error string
R: Register, next line contains passsword
I: INI set, next line contains:inikey inivalue
U: Update, agent overwrites its executable with a web-supplied arch-dependent version, and restarts

timeout: seconds. Kill spawned process after that timeout.
cmd_id: a random long, used to match commands with their output
~~~~

#### Example ####

```
#!php

<?php
  $cmd_id= mt_rand ( 1 , 65535 );

  echo "C 5 $cmd_id\n";
  echo "/bin/ls ."
?>
```


# Installation #

## Server ##


### OS environment ###
The server is PHP/javascript, requires:

* PHP 5.5 or newer.
* package php-pecl-rrd (not rrdtool-php)

for centos7 scl:
	copy from fedora24 rrd.so to /opt/rh/rh-php56/root/usr/lib64/php/modules/rrd.so
	enable .so in /etc/opt/rh/rh-php56/php.d/rrd.conf


### Install geoIP database ###

Run:


```
#!
server/bin/geoip-update.sh
```




This will update data/geoip/GeoLite2-City.mmdb


### vendor libraries ###

Install via composer as follows:

    php composer.phar self-update
    php composer.phar install

Creates the vendor/ directory, downloads dependencies in there, as defined in composer.json. If the composer.lock file exists it uses the version from .lock.

    php composer.phar update
Updates all vendor/ versions according to composer.json (ignores .lock)

### directory permissions ###
* Point your virtual host document root to your new application's `public/` directory.
* Ensure `cache/`, `logs/`, `data/` `data/rrd-stats` and `data/coolagent.db` are writeable from apache.


### Settings ###
copy app/settings.php.sample  to app/settings.php and edit

### Upgrading ###
The agent can self-update its executable and can request a new updated .ini from the server.
```

