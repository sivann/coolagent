/*
   coolagent
   remote command execution agent
   sivann at gmail.com 2015

*/

/* Get strsignal() declaration from <string.h> */
#define _POSIX_C_SOURCE
#define _GNU_SOURCE


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <limits.h>
#include <syslog.h>
#include <sys/klog.h>

#include <sys/prctl.h>


#include <arpa/inet.h>
#include <netdb.h>
#include <curl/curl.h>
#include <sys/utsname.h>

#include <minIni.h>
#include <base64.h>
#include <which.h>
#include <coolagent.h>


#define MAX_CMD_OUTPUT_LENGTH 1048576 /* maximum command output length (bytes) */
#define MAX_CMD_LENGTH 1048576 /* length of command & popen scripts (bytes) */
#define TIMEOUT_KILL_SEC 3 /* If timed out command has been unsuccessfully killed with SIGTERM, wait that many seconds before SIGKILL */

/* Globals */
option_s options;
struct sysinfo_s si;
char password[128];
char password_cookie[256];
struct hostent *chost;

pid_t cmd_pid;             /* pid of execed command */
int cmd_exitstatus;        /* exit status of cmd_pid */
char cmd_exitreason[128];

int nchildren=0;           /* current number of forked children processes */
int fastpolls=0;           /* remaining number of fast poll cycles */
char inifile[256];
int popen_alarm_active=0;  /* interrupted by alarm, for expiring popen commands */
char logmsg[MAX_CMD_LENGTH];
int lock_fd;               /* file lock used to prevent agent to run twice */

char register_url[2048]; // where to self-register
char fullinfo_url[2048]; // full poll (includes info json)
char poll_url[1024];     // short poll (only agent_id)

int main (int argc, char *argv[]) {
    int opt;
    struct sigaction sa;

    char lockfn[512];
    char lock_agentname[128];

    /* Defaults */
    strcpy (inifile,"coolagent.ini");
    options.debug=0;
    options.debughttp=0;
    options.replace_daemon=0;
    options.poll_period=900;
    options.fast_poll_period=60;
    options.longpoll_period=0;
    options.fastpolls_implicit=10; /* after each command */
    options.fastpolls_explicit=30; /* when requested by command 'A' (attention) */
    options.max_children=2;
    options.ssl_certificate_verify_peer=0; /* enable: 1 */
    options.ssl_certificate_verify_host=0; /* enable: 2 */

    /* initialize in case options are missing from .ini */
    options.additional_info=NULL;
    options.agent_id=NULL;
    options.server_url=NULL;
    options.ssl_certificate_capath=NULL;

    /* Parse Options */
    while ( (opt = getopt (argc, argv, "vhdprc:") ) != -1) {
        switch (opt) {
        case 'd':
            options.debug++;
            break;
        case 'p':
            options.debughttp++;
            break;
        case 'c':
            strlcpy (inifile,optarg,255);
            break;
        case 'r':
            options.replace_daemon = 1;
            break;
        case 'v':
            showVersion();
            exit(0);
            break;
        case 'h':
            showUsage();
            exit(0);
            break;
        default: /* '?' */
            showUsage();
            exit (EXIT_FAILURE);
        }
    }

    // Initialize syslog
    openlog (argv[0], LOG_PID|LOG_PERROR , LOG_LOCAL3);
    syslog (LOG_INFO, "Started by uid %d", getuid () );

    //parse coolagent.ini
    parseINI();

    //process additional_info ini external command or string
    processAdditionalInfo();

    /* Acquire lock to prevent agents to run simultaneously. Replace previous process if requested by commandline option. */
    strlcpy(lock_agentname,options.agent_id,127);
    str_replace_char_inline(lock_agentname,'/','-');
    snprintf(lockfn,512,"/tmp/coolagent-%s.lock",lock_agentname);
    lock_or_act(lockfn, options.replace_daemon);


    // Gather some system facts like distro, kernel, for use in agent_info etc
    getHostInfo (&si) ;
    if (options.debug) {
        printf ("os_distribution:%s\n",si.os_distribution);
        printf ("os_release:%s\n",si.os_release);
    }

    // Encode option and url strings, create URLs
    makeURLs();

    // add signal handler to count child processes and limit parallel commands (max_children)
    sigemptyset (&sa.sa_mask);
    sa.sa_flags = 0;
    sa.sa_handler = sigchildhdl_Count;
    if (sigaction (SIGCHLD, &sa, NULL) == -1) {
        snprintf (logmsg,sizeof(logmsg),"main:sigaction:%s",strerror (errno) );
        syslog (LOG_ERR,"%s",logmsg);
    }



    // Add environment variables to be inherited by executed scripts/commands
    setenv("COOL_AGENT_ID",options.agent_id,0);
    setenv("COOL_AGENT_ARCH",si.utsn.machine,0);
    setenv("COOL_AGENT_OS_DISTRIBUTION",si.os_distribution,0);
    setenv("COOL_AGENT_OS_RELEASE",si.os_release,0);

    //main HTTP poll loop, does not return 
    mainHTTPLoop(argv); 

    syslog (LOG_INFO, "After main loop, exiting");

    return 0;
}


//
// Parse coolagent.ini
//
void parseINI() {

    if (ini_browse (ini_cb_handler, &options, inifile) < 0) {
        sprintf (logmsg,"%s: %s\n",inifile,strerror (errno) );
        syslog (LOG_ERR,"%s",logmsg);
        exit(EXIT_FAILURE);
    }


    if (!options.password_file) {
        sprintf (logmsg,"ERROR: password_file not found in .ini");
        syslog (LOG_ERR,"%s",logmsg);
        exit (EXIT_FAILURE);
    }
    else if (!strlen_no_ws(options.password_file)) {
        sprintf (logmsg,"ERROR: password_file empty in .ini");
        syslog (LOG_ERR,"%s",logmsg);
        exit (EXIT_FAILURE);
    }

    if (!options.server_url) {
        sprintf (logmsg,"ERROR: server_url not found in .ini");
        syslog (LOG_ERR,"%s",logmsg);
        exit (EXIT_FAILURE);
    }
    else if (!strlen_no_ws(options.server_url)) {
        sprintf (logmsg,"ERROR: server_url empty in .ini");
        syslog (LOG_ERR,"%s",logmsg);
        exit (EXIT_FAILURE);
    }

    if (!options.agent_id) {
        sprintf (logmsg,"ERROR: agent_id not found in .ini");
        syslog (LOG_ERR,"%s",logmsg);
        exit (EXIT_FAILURE);
    }
    else if (!strlen_no_ws(options.agent_id)) {
        sprintf (logmsg,"ERROR: agent_id empty in .ini");
        syslog (LOG_ERR,"%s",logmsg);
        exit (EXIT_FAILURE);
    }

    if (contains_chars(options.agent_id," \\")) {// Ensure valid agent_id
        sprintf (logmsg,"ERROR: agent_id contains invalid characters");
        syslog (LOG_ERR,"%s",logmsg);
        exit (EXIT_FAILURE);
    }
}


//
// Get additional_info ini option value from external command if required.
// Do not exit on failure as an external file alteration should not break this agent.
//
void processAdditionalInfo() {
    if (options.additional_info &&  options.additional_info[0] == '|') {
        getAdditionalInfo();
    }
    else { //no popen, just copy the string
        options.additional_info_value = calloc (strlen (options.additional_info) +1,1);
        strcpy (options.additional_info_value,options.additional_info);
    }

    if (options.debug) {
        printf ("options.additional_info:%s\n",options.additional_info);
        printf ("Additional_info_value:(%s) [%ld]\n",options.additional_info_value,strlen(options.additional_info_value));
    }

}




/* URL and base64 encoded option and URL strings, create URLs */
void makeURLs() {
    int len;
    char agent_info_base64[1500];
    char agent_info_jsonstr[1500];
    char html5_map[256] = {0}; /* for url encoding */

    // Create the urlencoded versions of agent_id and additional_info_value
    url_encode_init (&html5_map[0]);

    options.agent_id_enc = calloc (strlen (options.agent_id) *3+1,1);
    url_encode (options.agent_id, options.agent_id_enc, html5_map);

    options.additional_info_value_enc = calloc (strlen (options.additional_info_value) *3+1,1);
    url_encode (options.additional_info_value, options.additional_info_value_enc, html5_map);


    // base64 encode the agent_info json
    snprintf (agent_info_jsonstr, 1500,
              "{\"poll_period\":\"%d\",\"nodename\":\"%s\",\"release\":\"%s\",\"machine\":\"%s\", "
              "\"os_distribution\":\"%s\",\"os_release\":\"%s\","
              "\"longpoll_period\":\"%d\", \"additional_info\":\"%s\" , \"agent_version\":\"%s %s\" }",
              options.poll_period, si.utsn.nodename, si.utsn.release, si.utsn.machine,
              si.os_distribution,si.os_release,
              options.longpoll_period, options.additional_info_value_enc, gitversion,gitdate
              );
    len=Base64encode (agent_info_base64, agent_info_jsonstr, strlen (agent_info_jsonstr) );
    agent_info_base64[len]=0;

    if (options.debug) {
        printf ("\nConfig loaded from 'coolagent.ini':\n-------------\n");
        printf ("server_url=%s\nagent_id=%s\nagent_id_urlencoded=%s\nssh_client=%s\nadditional_info_value(urlencoded)=%s\n",
                options.server_url, options.agent_id, options.agent_id_enc, options.ssh_client, 
                options.additional_info_value_enc);
        printf ("ssl_certificate_verify_peer=%d\nssl_certificate_verify_host=%d\nssl_certificate_capath=(%s)\npassword_file:%s\n\n",
                options.ssl_certificate_verify_peer, options.ssl_certificate_verify_host, options.ssl_certificate_capath,
                options.password_file);
    }


    //
    // Create poll urls
    //

    // Create the fullinfo_url, which is accessed at the start
    snprintf (fullinfo_url,2048,"%s/ca/agents/%s/nextcommand?agent_info=%s",
              options.server_url, options.agent_id_enc, agent_info_base64);

    // Create the poll_url, for loop polling
    sprintf (poll_url,"%s/ca/agents/%s/nextcommand", options.server_url, options.agent_id_enc);


    // Register URL used for 1st time registration
    snprintf (register_url,2048,"%s/ca/agents/%s/registration?agent_info=%s",
              options.server_url, options.agent_id_enc, agent_info_base64);
}



/* Register if needed, and poll HTTP in a loop forever. argv is needed to self-restart if commanded */
void mainHTTPLoop(char **argv) {
    struct MemoryStruct chunk;
    int is_first_loop=1;
    int want_fullupdate=0;
    int want_registration=0;

    // Register if no password file with cookie password was found.
    if (readPassword() == -1)
        want_registration = 1;
    else
        want_registration = 0;


    //
    // Main HTTP loop 
    //
    while (1) {
        chunk.buf = malloc (1);
        *chunk.buf = 0;
        chunk.size = 0;

        if (options.debug) {
            printf ("\n[%d]:Loop\n",getpid());
            printf ("[%d]:want_registration:%d\n", getpid(), want_registration);
            printf ("[%d]:want_fullupdate:%d\n", getpid(), want_fullupdate);
        }

        if (is_first_loop && want_registration) {
            if (options.debug)
                fprintf (stderr,"[%d]:register_url: (%s)\n", getpid(), register_url);
            httpsGet (register_url, &chunk);
        }
        else if (is_first_loop  || want_fullupdate) {
            want_fullupdate = 0;
            if (options.debug)
                fprintf (stderr,"[%d]:is_first_loop:fullinfo_url:will get next command from (%s)\n", getpid(), fullinfo_url);
            httpsGet (fullinfo_url, &chunk);
        }
        else { //typicall poll
            if (options.debug)
                fprintf (stderr,"[%d]:poll_url:will get next command from (%s)\n", getpid(), poll_url);
            httpsGet (poll_url, &chunk);
        }

        parseCmdAndAct (&chunk, argv, &want_registration, &want_fullupdate);

        if (chunk.buf)
            free (chunk.buf);

        if (fastpolls>0) {
            deepSleep (1000*options.fast_poll_period);
            fastpolls--;
        }
        else
            deepSleep (1000*options.poll_period);

        is_first_loop=0;
    }


}









void deepSleep(unsigned long milisec)
{
    struct timespec req={0};
    time_t sec=(int)(milisec/1000);

    milisec=milisec-(sec*1000);
    req.tv_sec=sec;
    req.tv_nsec=milisec*1000000L;
    if (options.debug)
        fprintf (stderr,"[%d]:deepSleep:sleeping for %ld ms\n",getpid(),milisec);
    while(nanosleep(&req,&req)==-1) {
        if (errno==EINTR)
            continue;
        else {
            perror("deepSleep:nanosleeep");
        }

    }
}


/* sleep, uninterrupted by signals */
/*
//Buggy, can possibly sleep only  0 since remaining is truncated to 0
void deepSleep (unsigned int seconds) {
    unsigned int rem;

    if (options.debug)
        fprintf (stderr,"[%d]:deepSleep:sleeping for %d seconds\n",getpid(),seconds);

    for (rem=seconds; rem;)
        rem=sleep (rem);
}
*/

/* kill cmd_pid (for timeout) */
void sigalarm_CommandKiller (int signum) {       /* parent SIGALRM handler     */
    int r;

    r = kill (cmd_pid, SIGTERM);
    if (r != 0) {
        sprintf (logmsg,"[%d]:SIGALRM,signal:%d:kill error:%s\n",getpid(),signum,strerror (errno) );
        syslog (LOG_ERR,"%s",logmsg);
        exit (errno);
    }

    deepSleep (1000*TIMEOUT_KILL_SEC);

    /* if pid still exists, kill -9 */
    if (kill (cmd_pid,0) )
        kill (cmd_pid,SIGKILL);

    if (options.debug) {
        fprintf (stderr,"[%d]:SIGALRM:sigalarm_CommandKiller:killed:%d\n",getpid(),cmd_pid);
    }

    /*
      We have set SIG_IGN on SIGCHLD, so no wait necessary
    */

    return;
}

void alarm_popen_handler () {       /* parent SIGALRM handler     */
    popen_alarm_active = 1;
}


/* runCommand: call a command via exec (cmd_type:C) or popen (cmd_type:P)
 * command_type: C,P
 * - fork:
 * - parent: returns , continues loop
 *   - child: forks again:
 *     - parent waits for output, posts output via http, and kills child on timeout
 *     - child execs <cmd> (C), or execs /bin/sh -c <cmd> (P)
 *
 * command_type: 0  (not used any more)
 * - fork:
 * - parent: returns , continues loop
 *   - child: calls cmd with popen, reads output, posts, kills process group on timeout
 */

void runCommand (char * cmd, char cmd_type, unsigned long cmd_id, int timeout) {
    unsigned long int mypid, child_pid;
    char buf[MAX_CMD_OUTPUT_LENGTH] ; //used to also hold command output
    char xbuf[1024];

    char post_result_url[512], post_tunnelport_url[512];
    FILE * fp;

    char *args[64];
    int r,pipefd[2];

    char port_ssh_prefix[]="Allocated port "; //when ssh binds its own port with ssh -R 0:..
    char *p;
    int ssh_port=0;

    sigset_t signal_set;
    struct sigaction sa;

    sprintf (post_result_url,"%s/ca/agents/%s/commands/%lu/result",options.server_url, options.agent_id, cmd_id);
    sprintf (post_tunnelport_url,"%s/ca/agents/%s/commands/%lu/tunnelport",options.server_url, options.agent_id, cmd_id);

    if (options.debug)
        printf ("runCommand(%s,%d)\n",cmd,timeout);

    //block sigchld signals so it stays blocked on child (signal handler set in main())
    sigemptyset (&signal_set);
    sigaddset (&signal_set, SIGCHLD);
    if (sigprocmask (SIG_BLOCK, &signal_set, NULL) == -1) {
        sprintf (logmsg,"sigprocmask(block):%s",strerror (errno) );
        syslog (LOG_ERR,"%s",logmsg);
    }

    if (! (child_pid=fork () ) ) { //child
        char pname[64];
        struct rlimit limit;

        //signal(SIGCHLD, SIG_IGN);
        //no special handler for executed command
        if (cmd_type != 'P') {
            sa.sa_handler = SIG_IGN;
        }
        else if (cmd_type == 'P') { //allow popen to handle its child and return correct exit status on pclose()
            sa.sa_handler = SIG_DFL;
        }

        sigemptyset (&sa.sa_mask);
        sa.sa_flags = 0;
        if (sigaction (SIGCHLD, &sa, 0) == -1) {
            sprintf (logmsg,"sigaction (ign):%s",strerror (errno) );
            syslog (LOG_ERR,"%s",logmsg);
        }

        /* Do not leave core files */
        getrlimit (RLIMIT_CORE, &limit);
        limit.rlim_cur = 0;
        setrlimit (RLIMIT_CORE, &limit);

        mypid = getpid ();

        if (options.debug)
            printf ("[%lu]:Child %lu born, cmd:%s, timeout:%d, cmd_type:%c\n", 
                    mypid, child_pid, cmd, timeout, cmd_type);

        snprintf(pname, sizeof(pname),"coolagent %c cmd %ld", cmd_type, cmd_id);
        //prctl(PR_SET_NAME, "Test");

        /* Plain popen no longer used since it lacks good status reporting, timeout, stderr */
        if (cmd_type == '0') { //popen
            unsigned int len=0;
            int ret;
            struct sigaction sa_alrm;

            popen_alarm_active = 0;

            sigemptyset (&sa_alrm.sa_mask);
            sa_alrm.sa_handler=alarm_popen_handler;
            sa_alrm.sa_flags = 0; //interrupt fread when ALARM goes off (sigaction SA_RESTART=0)

            sigaction(SIGALRM, &sa_alrm, NULL); //when it goes off, popen_alarm_actie will become 1
            alarm (timeout);

            if (options.debug)
                printf ("[%d]:Before popen command.\n", getpid() );


            //change process group before popen (so we don't kill parent)
            setpgid(0,0);

            //prevent popened stuff to become zombies
            //signal(SIGCHLD,SIG_IGN);

            fp = popen (cmd,"r");

            //ignore kill ourselves
            signal(SIGTERM,SIG_IGN);

            if (options.debug)
                printf ("[%d]:After popen command.\n", getpid() );

            if (fp == NULL) {
                sprintf (logmsg,"popen cmd:%s\n",strerror (errno) );
                syslog (LOG_ERR,"%s",logmsg);
                exit (errno);
            }

            buf[0]=0;
            len=0;
            /* fread will be interrupted by alarm */
            while ( (r=fread (xbuf,1,sizeof (xbuf)-1,fp) ) != 0) {
                if (options.debug)
                    printf ("[%d]:popen readloop\n", getpid() );
                xbuf[r]=0;
                len+=r;
                if (len>=sizeof (buf) ) {
                    sprintf (logmsg,
                             "[%d]: WARNING: popen readloop: command %ld output > sizeofbuf (%ld):truncated\n",getpid(),cmd_id,sizeof (buf) );
                    syslog (LOG_WARNING,"%s",logmsg);
                    break;
                }
                strcat (buf,xbuf);

                if (options.debug) {
                    fprintf (stderr,"[%d]: popen readloop: xbuf(%s)",getpid(),xbuf);
                }
            }

            if (options.debug) {
                fprintf (stderr,"[%d]:before pclose\n", getpid() );
                fprintf (stderr,"[%d]:popen_alarm_active(timeout): %i\n", getpid(), popen_alarm_active);
            }


            if (popen_alarm_active) { //timeout
                sprintf (logmsg,"timeout on cmd:%ld",cmd_id);
                syslog (LOG_INFO,"%s",logmsg);

                kill(0,SIGTERM); // kill process group

                if (options.debug) {
                    fprintf (stderr,"[%d]:killed -15 process group\n", getpid());
                }


                httpsPost (post_result_url, buf, -50, "interrupted by timeout") ;
                kill(0,SIGKILL); // kill process group for good
                exit(0);  //not reached
            }
            else {
                ret = pclose (fp);
                if (options.debug) {
                    if (ret) {
                        fprintf (stderr,"[%d]: pclose:%d error:%s\n",getpid(),ret,strerror(errno));
                    }
                    else
                        fprintf (stderr,"[%d]: pclose:%d\n",getpid(),ret);
                }
                httpsPost (post_result_url, buf, ret, "") ;
            }

            if (options.debug)
                printf ("[%d]:After popen command.\n", getpid() );
        }
        else if (cmd_type=='C' || cmd_type=='P') { //exec
            int i=0;
            unsigned int len=0;
            char b[512];

            if (pipe (pipefd) == -1) {
                sprintf (logmsg,"pipe:%s",strerror (errno) );
                syslog (LOG_ERR,"%s",logmsg);
                return;
            }

            cmd_exitstatus=-9999;
            cmd_exitreason[0]=0;

            //signal handler, to get exit code of execed command

            sigemptyset (&sa.sa_mask);
            sa.sa_flags = 0;
            sa.sa_handler = sigchildhdl_GetExitStatus;
            if (sigaction (SIGCHLD, &sa, NULL) == -1) {
                sprintf (logmsg,"main:sigaction:%s",strerror (errno) );
                syslog (LOG_ERR,"%s",logmsg);
            }
            //block sigchld signals so it stays blocked on child
            sigemptyset (&signal_set);
            sigaddset (&signal_set, SIGCHLD);
            if (sigprocmask (SIG_BLOCK, &signal_set, NULL) == -1)  {
                sprintf (logmsg,"sigprocmask(block):%s",strerror (errno) );
                syslog (LOG_ERR,"%s",logmsg);
            }


            if (! (cmd_pid=fork () ) ) {
                //inside child, will be replaced by cmd via exec
                char *newcmd;

                sa.sa_handler = SIG_IGN;
                sigemptyset (&sa.sa_mask);
                sa.sa_flags = 0;
                if (sigaction (SIGCHLD, &sa, 0) == -1) {
                    sprintf (logmsg,"sigaction (ign):%s",strerror (errno) );
                    syslog (LOG_ERR,"%s",logmsg);
                }
                //signal(SIGCHLD, SIG_IGN);
                if (options.debug)
                    printf ("[%d]:Before exec command id:%ld\n", getpid(),cmd_id);

                /* Replace command variables */
                newcmd=str_replace (cmd,"#ssh_client#",options.ssh_client);


                i=0;
                while ( (dup2 (pipefd[1], 1) == -1) && (errno == EINTR) && i < 100) {
                    i++;
                }
                close (pipefd[0]); //close read-end of pipe
                dup2 (1, 2); // stderr->stdout

                if (cmd_type == 'C') {
                    makeargv (newcmd,args); //make argument vector from newcmd (newcmd gets nulls on delimiters)
                    if (execvp (args[0], args) == -1) {
                        snprintf (logmsg,sizeof(logmsg),"execvp:%s,command:%s",strerror (errno),cmd );
                        syslog (LOG_ERR,"%s",logmsg);
                        exit (errno);
                    }
                }
                else { // cmd_type == 'P'
                    for (i = strlen (newcmd) - 1; (!isalnum (newcmd[i]) ); i--) newcmd[i]=0; //rtrim
                    if (execl("/bin/sh", "sh", "-c", newcmd, (char *)0) == -1) {
                        snprintf (logmsg,sizeof(logmsg),"SHELL execvp:%s command:%s",strerror (errno),cmd );
                        /* exec in this case will always succeed unless /bin/sh is missing, 
                         * it's the shell not exec that returns the error */
                        syslog (LOG_ERR,"%s",logmsg);
                        exit (errno);
                    }
                }
                fprintf (stderr,"ERROR: how did we reach this\n");
                exit (-5555);
            }

            //unblock signal handler so we can get exit status
            sigemptyset (&signal_set);
            sigaddset (&signal_set, SIGCHLD);
            if (sigprocmask (SIG_UNBLOCK, &signal_set, NULL) == -1) {
                sprintf (logmsg,"sigprocmask(unblock):%s",strerror (errno) );
                syslog (LOG_ERR,"%s",logmsg);
            }

            //parent, waits for child's output and posts it
            signal (SIGALRM, sigalarm_CommandKiller);
            alarm (timeout);

            if (options.debug)
                printf ("[%d]:Added SIGALRM for %d seconds (should kill pid %d)\n", getpid(), timeout, cmd_pid);

            close (pipefd[1]); //close write-end of pipe

            if (options.debug)
                printf ("[%d]:Reading child %d output\n", getpid(), cmd_pid);

            buf[0]=b[0]=0;
            //read command output in chunks until buf is full
            while ( (r=read (pipefd[0], b, sizeof (b)-1) ) != 0) {
                if (r<0) {
                    if (errno==EINTR)
                        continue;
                    else {
                        fprintf (stderr,"WARNING: read(pipefd[0]):%s",strerror (errno) );
                    }
                    break;
                }
                b[r]=0;
                len+=r;
                if (len>=sizeof (buf) ) {
                    sprintf (logmsg,
                             "[%d]: WARNING: readloop: command %ld output > sizeofbuf (%ld):truncated\n", getpid(), cmd_id, sizeof (buf) );
                    syslog (LOG_ERR,"%s",logmsg);
                    break;
                }
                strcat (buf,b);

                if (options.debug) {
                    fprintf (stderr,"[%d]: readloop: (%s)",getpid(),b);
                }

                //Now check if we must comunicate ssh auto-bound port back to the server
                if (strstr (cmd,"#ssh_client#") ) {
                    p=strstr (b,port_ssh_prefix);
                    ssh_port=0;
                    if (p) {
                        sscanf (p+strlen (port_ssh_prefix),"%d",&ssh_port);
                    }
                    if (ssh_port) {
                        if (options.debug) {
                            fprintf (stderr,"[%d]: Should notify server that bound ssh_port is: (%d)\n", getpid(), ssh_port);
                        }
                        sprintf (xbuf,"%d",ssh_port);
                        httpsPost (post_tunnelport_url, xbuf, 0, "") ;
                    }
                }
            }
            close (pipefd[0]);

            if (options.debug) {
                fprintf (stderr,"[%d]: cmd_exitstatus:%d, cmd_exitreason:%s\n",getpid(), cmd_exitstatus, cmd_exitreason);
                fprintf (stderr,"[%d]: waiting for child handler to update exitstatus\n",getpid() );
            }

            // Wait a bit for SIGCHLD signal handler to update exitstatus variable for us to report to server
            while (cmd_exitstatus == -9999) {
                int sleepcount=0;
                sleep (1);
                if (sleepcount++ > 10) {
                    sprintf (logmsg,"[%d]: waited too much for exitstatus update, returning\n",getpid() );
                    syslog (LOG_WARNING,"%s",logmsg);
                }
            }

            if (options.debug) {
                fprintf (stderr,"[%d]: Done reading:[%s], cmd_exitstatus:%d, cmd_exitreason:%s\n",
                        getpid(), buf, cmd_exitstatus, cmd_exitreason);
                fprintf (stderr,"[%d]:Will now post on (%s)\n", getpid(), post_result_url);
            }

            httpsPost (post_result_url, buf, cmd_exitstatus, cmd_exitreason) ;
            //sleep(5); //for TCP LINGER, now set inside a curl callback

            if (options.debug)
                fprintf (stderr,"[%d]:Done Posting\n", getpid() );

            exit (0);

        } //cmd==C || P
        exit (0);
    }
    else { // we are the  parent
        // unblock sigchld
        sigemptyset (&signal_set);
        sigaddset (&signal_set, SIGCHLD);
        if (sigprocmask (SIG_UNBLOCK, &signal_set, NULL) == -1)  {
            sprintf (logmsg,"sigprocmask(unblock):%s",strerror (errno) );
            syslog (LOG_WARNING,logmsg);
        }

        nchildren++;
        if (options.debug)
            printf ("[%d]:I have %d children\n", getpid(), nchildren);
    }
} // runCommand

/* argv is an array of pointers to buf parts
 * buf will be overwritten (spaces replaced with 0)
 */
void  makeargv (char *buf, char **argv) {
    while (*buf != '\0') {       /* if not the end of buf */
        while (*buf == ' ' || *buf == '\t' || *buf == '\n' || *buf == '\r' )
            *buf++ = '\0';     /* replace white spaces with 0    */
        *argv++ = buf;

        while (*buf != '\0' && *buf != ' ' &&
                *buf != '\t' && *buf != '\n' && *buf != '\r' )
            buf++;
    }
    *argv = '\0';
}

/* Parse server-supplied command string */
void parseCmdAndAct (struct MemoryStruct * ms, char *argv[], int *want_registration, int *want_fullupdate) {
    unsigned int timeout;
    unsigned long cmd_id;
    int r;
    char cmd_type;
    char cmd[MAX_CMD_LENGTH];
    char buf[MAX_CMD_LENGTH];
    char *p;

    cmd[0]=0;

    //read 1st line
    r=sscanf (ms->buf,"%c %u %lu",&cmd_type,&timeout,&cmd_id);
    if (r != 3) {
        snprintf (logmsg,sizeof(logmsg),"Got invalid response to nextcommand request, response:(%s)\n",ms->buf);
        syslog (LOG_ERR,"%s",logmsg);
        return ;
    }

    switch (cmd_type) {
        case 'P': // run command or script via /bin/sh -c  
        case 'C': // run command
            fastpolls=options.fastpolls_implicit;
           
            //Read command
            p=strchr (ms->buf,'\n'); //position of 1nd line end
            strlcpy (cmd,&ms->buf[p-ms->buf+1],MAX_CMD_LENGTH-1); //copy the rest to cmd
            if ( (p=strrchr (cmd,'\n') ) ) //remove trailing newline from cmd
                *p=0;

            //check for max running
            if (nchildren >= options.max_children) {
                sprintf (logmsg,"nchildren(%d) > max(%d)\n",nchildren,options.max_children);
                syslog (LOG_ERR,"%s",logmsg);
                return ;
            }
            if (!strlen (cmd) ) {
                if (options.debug)
                    printf ("EMPTY cmd\n");
                return;
            }

            //run command
            runCommand (cmd,cmd_type,cmd_id,timeout);

            break;
        case 'A': //attention: tight query loop for a while
            fastpolls=options.fastpolls_explicit;
            break;
        case 'I': //INI set command
            {
                char inikey[64];
                char inival[512];
                char t[512];
                int n;

                p=strchr (ms->buf,'\n'); //position of 1nd line end
                strlcpy (buf,&ms->buf[p - ms->buf + 1],511); //copy the rest to buf
                sscanf(inikey,"%63s",buf);
                sscanf(inival,"%511s",&buf[strlen(inikey)]);
                //edit ini here if inikey already exists
                //check if key exists
                n = ini_gets("coolagent", inikey, NULL, t, sizeof(t)/sizeof(t[0]), inifile);
                if (!n) {
                    //error, non existent inikey
                }
                else {
                    n = ini_puts("coolagent", inikey, inival, inifile);
                    if (!n) {
                        //error writing ini?
                    }
                }
                //POST result?
            }
        
            break;

        case 'U': //Update agent
                p=strchr (ms->buf,'\n'); //position of 1nd line end
                strlcpy (buf,&ms->buf[p - ms->buf + 1],511); //copy the rest to buf
                update_agent(argv, si.utsn.machine,cmd_id);
            break;


        case 'R': //register, next line contains passsword
            p=strchr (ms->buf,'\n'); //position of 1nd line end
            strlcpy (buf,&ms->buf[p-ms->buf+1],511); //copy the rest to buf
            strlcpy (password,buf,sizeof (password) );
            writePassword();

            //perform a full update, in case server requested re-registration while 
            //we were running (e.g. in case somebody deleted the agent server-side)
            *want_fullupdate = 1;

            if (readPassword() == -1)
                *want_registration = 1;
            else
                *want_registration = 0;

            if (options.debug)
                fprintf (stderr,"PASSWORD from server:[%s]\n",password);
            buf[0]=0;
            break;

        case 'E': //error, probably as a response to an incorrect request of ours
            p=strchr (ms->buf,'\n'); //position of 1nd line end
            strlcpy (buf,&ms->buf[p-ms->buf+1],511); //copy the rest to buf
            snprintf (logmsg,sizeof(logmsg),"ERROR from server:[%s]\n",buf);
            syslog (LOG_ERR,"%s",logmsg);
            buf[0]=0;
            break;

        case 'N': //No command
            break;
        default:
            sprintf (logmsg,"Unknown command type:[%c]\n",cmd_type);
            syslog (LOG_ERR,"%s",logmsg);
    }

    snprintf(logmsg,sizeof(logmsg),"got command: cmd_type:%c, timeout:%u, cmd_id:%lu, command:%s",cmd_type,timeout,cmd_id,cmd);
    syslog (LOG_INFO,"%s",logmsg);

    if (options.debug) {
        printf("***Got:%s\n",ms->buf);
        printf ("cmd_type:%c, timeout:%u, cmd_id:%lu\n",cmd_type,timeout,cmd_id);
        fflush(stdout);
    }
} /* parseCmdAndAct */

/* Read the cookie password */
int readPassword() {
    FILE * fp;

    if ( access ( options.password_file, F_OK ) == -1 ) {
        if (options.debug) {
            fprintf (stderr,"readPassword:password file %s:%s, will request registration\n",options.password_file,strerror (errno) );
        }
        return -1; //does not exist
    }


    fp=fopen (options.password_file,"r");
    fscanf (fp,"%127s",password);
    strcpy (password_cookie,"password=");
    strlcat (password_cookie,password,128);
    fclose (fp);

    if (options.debug) {
        fprintf (stderr,"readPassword:password=%s\n",password);
    }

    return 0;
}

/* Write the cookie password */
void writePassword() {
    FILE * fp;

    if (options.debug) {
        fprintf(stderr,"Writting password file, password=[%s]\n",password);
    }

    umask (066);
    fp=fopen (options.password_file,"w");

    if (!fp) {
        snprintf(logmsg,sizeof(logmsg),"[%d]:ERROR:%s:%s", getpid(), options.password_file, strerror(errno));
        fprintf(stderr,"%s",logmsg);
        syslog (LOG_ERR,"%s",logmsg);
        exit (errno);
    }
    fprintf (fp,"%s\n",password);
    fclose (fp);
}

void showVersion() {
    printf ("%s|%s|%s\n",gitversion, gitdate, compdate);
}

void showUsage() {
    printf ("\nCool Agent, remote execution client. (https://bitbucket.org/sivann/coolagent)\n\ntag/commit:%s\ncommited on:%s\ncompiled on:%s\n\n",gitversion, gitdate, compdate);
    printf ("Usage: coolagent [-d] [-p] [-c ini_file]\n");
    printf ("\t-d\t\tdebug\n");
    printf ("\t-p\t\thttp protocol debug\n");
    printf ("\t-c [ini_file]\tpath of coolagent.ini\n");
    //printf ("\t-I\t\trequest coolagent.ini from server. Will contain random agent_id if unregistered.\n");
    printf ("\t-r\t\treplace: terminate current lockfile-holding process and run\n");
    printf("\n");
}


// Gather some local system info
void getHostInfo (struct sysinfo_s * si) {
    FILE *fp;

    if (uname (& (si->utsn) ) != 0) {
        perror ("uname:");
        exit (EXIT_FAILURE);
    }

    //detect package manager
    if (which_exists ("yum") )
        strcpy (si->pkgmgr,"yum");
    else if (which_exists ("apt-get") )
        strcpy (si->pkgmgr,"apt");
    else if (which_exists ("opkg") )
        strcpy (si->pkgmgr,"opkg");
    else if (which_exists ("dnf") )
        strcpy (si->pkgmgr,"dnf");
    else if (which_exists ("pacman") )
        strcpy (si->pkgmgr,"pacman");
    else if (which_exists ("emerge") )
        strcpy (si->pkgmgr,"emerge");
    else if (which_exists ("zypp") )
        strcpy (si->pkgmgr,"zypp");

    //detect distribution
    if (which_exists ("lsb_release") ) {
        FILE * pfp;
        char buf[1024];
        char *p,*p1;
        int i,r;

        pfp = popen ("lsb_release -a","r");
        if (!pfp) {
            perror ("lsb_release -a:");
            exit (EXIT_FAILURE);
        }
        while (fgets (buf, sizeof (buf), pfp) != NULL) {
            if ( (p=strcasestr (buf,"Distributor ID:") ) ) {
                p1=strrchr (buf,':')+1;
                while (isspace (*p1) ) p1++;
                for (i = strlen (p1) - 1; (!isalnum (p1[i]) ); i--) p1[i]=0; //rtrim
                strlcpy (si->os_distribution,p1,64);
            }
            else if ( (p=strcasestr (buf,"Release:") ) ) {
                p1=strrchr (buf,':')+1;
                while (isspace (*p1) ) p1++;
                for (i = strlen (p1) - 1; (!isalnum (p1[i]) ); i--) p1[i]=0; //rtrim
                strlcpy (si->os_release,p1,16);
            }
        }

        r = pclose (pfp);
        if (r == -1) {
            perror ("lsb_release -a:pclose:");
            exit (EXIT_FAILURE);
        }
    }
    else if ( (!strcmp (si->pkgmgr,"opkg") ) && (fp = fopen ("/etc/issue", "r") ) ) { //embian
        char buf[1024];
        char *p,*p1;
        int i;

        while (fgets (buf, sizeof (buf), fp) != NULL) {
            if ( (p=strcasestr (buf,"embian") ) ) {
                strcpy (si->os_distribution,"embian");
                p1=strrchr (buf,' ')+1;
                while (isspace (*p1) ) p1++;
                for (i = strlen (p1) - 1; (!isalnum (p1[i]) ); i--) p1[i]=0; //rtrim
                strlcpy (si->os_release,p1,16);
                strcpy (si->manufacturer,"inacces");
            }

            else if ( (p=strcasestr (buf,"Kernel ") ) ) {
                p1=strrchr (buf,' ')+1;
                while (isspace (*p1) ) p1++;
                for (i = strlen (p1) - 1; (!isalnum (p1[i]) ); i--) p1[i]=0; //rtrim
                strlcpy (si->model,p1,16);
            }
        }

        fclose (fp);
    }
}

/* INI callback handler Used for ini parsing */
int ini_cb_handler (const char * section, const char * key, const char * value, void * userdata) {
    option_s* pconfig = (option_s*) userdata;

#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(key, n) == 0 

    if (MATCH ("coolagent", "server_url") )
        pconfig->server_url = strdup (value);
    else if (MATCH ("coolagent", "agent_id") )
        pconfig->agent_id = strdup (value);
    else if (MATCH ("coolagent", "additional_info") )
        pconfig->additional_info = strdup (value);
    else if (MATCH ("coolagent", "password_file") )
        pconfig->password_file = strdup (value);
    else if (MATCH ("coolagent", "ssh_client") )
        pconfig->ssh_client = strdup (value);
    else if (MATCH ("coolagent", "poll_period") )
        pconfig->poll_period = atoi (value);
    else if (MATCH ("coolagent", "fast_poll_period") )
        pconfig->fast_poll_period = atoi (value);
    else if (MATCH ("coolagent", "longpoll_period") )
        pconfig->longpoll_period = atoi (value);
    else if (MATCH ("coolagent", "max_children") )
        pconfig->max_children = atoi (value);
    else if (MATCH ("coolagent", "ssl_certificate_verify_peer") )
        pconfig->ssl_certificate_verify_peer = atoi (value);
    else if (MATCH ("coolagent", "ssl_certificate_verify_host") )
        pconfig->ssl_certificate_verify_host = atoi (value);
    else if (MATCH ("coolagent", "ssl_certificate_capath") )
        pconfig->ssl_certificate_capath = strdup (value);
    else if (MATCH ("coolagent", "fastpolls_implicit") )
        pconfig->fastpolls_implicit = atoi (value);
    else if (MATCH ("coolagent", "fastpolls_explicit") )
        pconfig->fastpolls_explicit = atoi (value);
    else {
        snprintf(logmsg,sizeof(logmsg),"*** Unknown ini entry: [%s]\t%s=%s\n", section, key, value);
        fprintf(stderr,"%s",logmsg);
        syslog (LOG_ERR,"%s",logmsg);
    }

    return 1;
}


/* wait and set exit status in global var so it can be reported back (used by command executing process) */
void sigchildhdl_GetExitStatus (int sig) {
    int e, r;
    pid_t child_pid=0;

    while ( (r = waitpid (-1, &e, WNOHANG) ) > 0) {
        child_pid=r;
        //nchildren--;
        setWaitStatus (e);
        if (options.debug) {
            printf ("[%d]:sigchildhdl_GetExitStatus,waitpid: returned:%d, exit_reason:%s, nchildren now:%d\n", getpid(),r,cmd_exitreason,nchildren);
        }
    }

    if (child_pid == -1 && errno != ECHILD) {
        sprintf (logmsg,"[%d]:sigchildhdl_GetExitStatus,ERROR: waitpid: %s\n",getpid(),strerror (errno) );
        syslog (LOG_ERR,"%s",logmsg);
    }

    if (child_pid == cmd_pid) { //just to be sure
        cmd_exitstatus = e;
    }

    if (options.debug)
        printf ("[%d]:sigchildhdl_GetExitStatus,signal:%d, child_pid:%d, child exited status:%d, nchildren:%d\n",
                getpid (), sig, child_pid, e, nchildren);

    //signal (SIGCHLD, sigchildhdl_GetExitStatus);

}

/* just wait and count children (used by main process) */
void sigchildhdl_Count (int sig) {
    int e, r, child_pid=0;

    while ( (r = waitpid (-1, &e, WNOHANG) ) > 0) {
        child_pid=r;
        nchildren--;
    }

    if (options.debug)
        printf ("[%d]:sigchildhdl_Count(signal:%d), child_pid:%d exited with:%d, nchildren:%d\n",
                getpid (), sig, child_pid, e, nchildren);

    //signal (SIGCHLD, sigchildhdl_Count);

}


/* Examine a wait() status using the W* macros */
void setWaitStatus (int status) {
    char s[128];

    s[0]=0;

    if (WIFEXITED (status) ) {
        sprintf (s,"process exited, exit status=%d", WEXITSTATUS (status) );
    }
    else if (WIFSIGNALED (status) ) {
        sprintf (s,"process killed by signal %d (%s)", WTERMSIG (status), strsignal (WTERMSIG (status) ) );
        if (WCOREDUMP (status) )
            strcat (s," (core dumped)");
    }
    else if (WIFSTOPPED (status) ) {
        sprintf (s,"process stopped by signal %d (%s)", WSTOPSIG (status), strsignal (WSTOPSIG (status) ) );
    }
    else if (WIFCONTINUED (status) ) {
        sprintf (s,"process continued\n");
    }
    else {   /* Should never happen */
        sprintf (s,"what happened to this process? (exit status=%x)", (unsigned int) status);
    }
    strcpy (cmd_exitreason,s);
}



/* check of lock exists, and act
 * action == 0 : exit
 * action == 1 : replace previous lock-holding process: if lock is active kill pid in lockfile (coolagent called with -f)
 */
void lock_or_act(char * lockfn, int action) {
    int ret,flags;
    char b[64];

    if ((lock_fd = open(lockfn, O_CREAT|O_RDWR|O_SYNC,S_IRUSR | S_IWUSR)) == -1) {
        perror("lock");
        exit(1);
    }


    //Prevent locks to be inherited on children. 
    //Old kernels do not support O_CLOEXEC flag in open()
    if ((flags = fcntl (lock_fd, F_GETFD, 0)) == -1) {
            sprintf (logmsg,"lock_or_act:fcntl:F_GETFD: %s",strerror (errno) );
            syslog (LOG_ERR,"%s",logmsg);
    }
    else {
        flags |= FD_CLOEXEC;
        if (fcntl (lock_fd, F_SETFD, flags) == -1 ) {
            sprintf (logmsg,"lock_or_act:fcntl:F_SETFD: %s",strerror (errno) );
            syslog (LOG_ERR,"%s",logmsg);
            if (action !=1) //better to ignore in this case
                exit(EXIT_FAILURE);
        }
    }

    //lock or fail
    if ((ret = lockf(lock_fd, F_TLOCK, 0)) == -1) {
        //could not lock, probably already locked

        if (action == 0) {
            sprintf(logmsg,"Another instance is running, %s is locked, exiting",lockfn);
            syslog (LOG_ERR,"%s",logmsg);
            exit(EXIT_FAILURE);
        }
        else { //kill running process and retry lock
            pid_t oldpid;
            int r;

            //if (!(fp = fdopen(lock_fd,"r"));
            r = read(lock_fd,b,63);
            b[r]=0;
            sscanf(b,"%d",&oldpid);
            sprintf(logmsg,"Another instance is running, %s is locked, killing with SIGTERM old pid %d",lockfn, oldpid);
            syslog (LOG_INFO,"%s",logmsg);
            if (kill(oldpid,0)) {
                sprintf(logmsg,"old pid %d disappeared before killing it",oldpid);
                syslog (LOG_INFO,"%s",logmsg);
            }
            else
                kill(oldpid,15);

            sleep (2);
            if (kill(oldpid,0)) {
                sprintf(logmsg,"old pid %d killed",oldpid);
                syslog (LOG_INFO,"%s",logmsg);
            }
            else
                kill(oldpid,9);

            sleep(1);

            //try to lock again
            if ((ret = lockf(lock_fd, F_TLOCK, 0)) == -1) {
                sprintf(logmsg,"failed to acquire lock, %s even after killing with SIGKILL pid %d",lockfn,oldpid);
                syslog (LOG_ERR,"%s",logmsg);
                exit(EXIT_FAILURE);
            }
        }
    }

    //lock succedded here

    //write our pid in the lockfile
    snprintf(b,64,"%d",getpid()) ;
    ftruncate(lock_fd, 0);
    lseek(lock_fd,0,SEEK_SET);
    write(lock_fd,b,strlen(b));
}


// Download new executable and restart
void update_agent(char **argv, char * arch, unsigned long cmd_id) {
    int r;
    char url[2048];
    char savename[1024];
    char post_result_url[512];

    sprintf (post_result_url,"%s/ca/agents/%s/commands/%lu/result",options.server_url, options.agent_id, cmd_id);

    if (options.debug) 
        fprintf (stderr,"[%d]:update_agent:cmd_id (%ld)\n\n", getpid(), cmd_id);

    if (!strchr(argv[0],'/')) {
        sprintf (logmsg,"update_agent:argv[0] in path:%s",argv[0]);
        syslog (LOG_ERR,"%s",logmsg);
        return ;
    }

    snprintf (url,2048,"%s/software/agent/%s", 
            options.server_url, arch );

    strcpy(savename,argv[0]);
    strcat(savename,"-1");
    r = rename(argv[0], savename);

    if (r == -1) {
        sprintf (logmsg,"update_agent:rename:%s",strerror(errno));
        syslog (LOG_ERR,"%s",logmsg);
        httpsPost (post_result_url,strerror(errno), -1, "") ;
        return ;
    }

    r = httpsGetFile (url, argv[0]) ;

    if (r) {
        rename(savename,argv[0]); //rename back

        sprintf (logmsg,"update_agent:httpsGetFile returned:%d",r);
        syslog (LOG_ERR,"%s",logmsg);
        if (r<100) {
            char str[256];
            snprintf (str,255,"http status: %d",-r);
            httpsPost (post_result_url,str, r, "") ;
        }
        else {
            httpsPost (post_result_url,strerror(errno), -2, "") ;
        }
        return ;
    }

    chmod(argv[0],S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IWGRP|S_IXGRP|S_IROTH|S_IXOTH);

    if (options.debug) 
        fprintf (stderr,"[%d]:update_agent:posting cmd_id (%ld)\n", getpid(), cmd_id);

    httpsPost (post_result_url,"update_agent:success", 0, "") ;

    if (options.debug) 
        fprintf (stderr,"[%d]:update_agent:sleep(10) before restarting, cmd_id (%ld)\n", getpid(), cmd_id);

    sleep (20);

    restart(argv);

    sprintf (logmsg,"[%d]:update_agent:should not reach here! cmd_id (%ld)\n", getpid(), cmd_id);
    syslog (LOG_ERR,"%s",logmsg);
}


int restart(char **argv) {
    if (execvp(argv[0], argv)) {
        /* ERROR, handle this yourself */
        sprintf (logmsg,"restart:execvp:%s",strerror (errno) );
        syslog (LOG_ERR,"%s",logmsg);
        return -1;
    }

    return 0;
}

void getAdditionalInfo() {

    FILE * pfp;
    char buf[1024];
    int r,ret;


    memset(buf, '\0', sizeof buf);

    pfp = popen (options.additional_info+1,"r");
    if (!pfp) {
        perror (options.additional_info);
        sprintf (logmsg,"ERROR: options.additional_info:%s",strerror(errno));
        syslog (LOG_ERR,"%s",logmsg);
    }

    r=fread (buf,1,sizeof (buf),pfp);
    buf[r]='\0';

    ret = pclose(pfp);
    if (ret) {
        sprintf (logmsg,"[%d]: getAdditionalInfo: pclose:%d error:%s\n",getpid(),ret,strerror(errno));
        syslog (LOG_ERR,"%s",logmsg);
    }

    //trim last newline
    if (buf[strlen (buf)-1]=='\n')
        buf[strlen (buf)-1]='\0';

    options.additional_info_value = calloc (strlen (buf)+1,1);
    if (!options.additional_info_value) {
        sprintf (logmsg,"ERROR: options.additional_info:calloc:%s",strerror(errno));
        syslog (LOG_ERR,"%s",logmsg);
    }
    strcpy (options.additional_info_value,buf);

}
