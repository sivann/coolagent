#ifndef WHICH_H
#define WHICH_H

char * which(const char *name);
char * which_path(const char *name, const char *path);

int which_exists(const char *name);

#endif /* WHICH_H */
