#ifndef __COOLAGENT_H
#define __COOLAGENT_H
extern const char *gitversion;
extern const char *gitdate;
extern const char *compdate;
extern const char *sampleini;

#include <sys/utsname.h>
#include <stdlib.h>


struct MemoryStruct {
    char *buf;
    size_t size;
};

typedef struct {
    int debug;
    int debughttp;
    int replace_daemon;
    char* agent_id;
    char* agent_id_enc;

    char* additional_info;
    char* additional_info_value;
    char* additional_info_value_enc;

    char* server_url;
    char* ssh_client;
    char* password_file;
    int poll_period;
    int fast_poll_period;
    int longpoll_period;
    int fastpolls_implicit;
    int fastpolls_explicit;
    int max_children;
    int ssl_certificate_verify_peer;
    int ssl_certificate_verify_host;
    char* ssl_certificate_capath;

} option_s;


struct sysinfo_s {
    struct utsname utsn;
    char pkgmgr[32];
    char os_distribution[64];
    char os_release[16];
    char manufacturer[64];
    char model[64];
};



/* function prototypes */
int httpsGet (char *, struct MemoryStruct *);
int httpsPost(char* url, char* data,int,char*);
int httpsGetFile (char *url, char * target_path) ;
void getHostInfo(struct sysinfo_s *);
void parseCmdAndAct(struct MemoryStruct * ms, char *argv[], int *want_registration, int *want_fullupdate);
void runCommand (char * cmd, char cmd_type, unsigned long cmd_id, int timeout) ;
void sigchildhdl_Count (int);
void sigchildhdl_GetExitStatus (int sig) ;
void sigalarm_CommandKiller (int signum) ;
void showUsage();
void showVersion();
void deepSleep(unsigned long int);
void  makeargv(char *buf, char **argv) ;
char *str_replace (const char *string, const char *substr, const char *replacement);
void url_encode_init(char *tb) ;
void url_encode(const char *s, char *enc, char *tb);
void setWaitStatus(int status) ;
void writePassword() ;
int readPassword() ;
void dump(const char *text, FILE *stream, unsigned char *ptr, size_t size);
int strlen_no_ws(char *str) ;
size_t strlcpy(char *dst, const char *src, size_t siz); /* like strncpy but always null-terminates */
size_t strlcat(char *dst, const char *src, size_t siz);
int ini_cb_handler (const char *section, const char *key, const char *value, void *userdata) ;
void getAdditionalInfo();
void lock_or_act(char * lockfn,int action);
int contains_chars(char * s, char * chars);
void str_replace_char_inline(char * s, char old, char new) ;
void update_agent(char **argv,char * arch, unsigned long int) ;
int restart(char **argv);
void parseINI(void);
void processAdditionalInfo();
void makeURLs();
void mainHTTPLoop();

#endif
