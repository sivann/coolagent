/* Coolagent HTTP functions 
 * sivann 2015
 *
 * Helper functions to talk HTTP(s). 
 * GET, POST, download file via GET
 * All errors are logged to syslog.
 */

#include <stdio.h>
#include <curl/curl.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <sys/types.h>
#include <unistd.h>

#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>



#include <coolagent.h>

/* Globals */
extern char logmsg[256];
extern option_s options;
extern char password_cookie[256];
struct linger _linger;


/* Prototypes */
int my_curl_trace(CURL *handle, curl_infotype type, char *data, size_t size, void *userp) ;


static size_t writeMemoryCallback (void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *) userp;

    mem->buf = realloc (mem->buf, mem->size + realsize + 1);

    if (mem->buf == NULL) {
        /* out of memory! */
        sprintf (logmsg,"writeMemoryCallback:%s\n",strerror (errno) );
        syslog (LOG_ERR, "%s", logmsg);
        return errno;
    }

    memcpy (& (mem->buf[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->buf[mem->size] = 0;

    return realsize;
}

//https://curl.haxx.se/libcurl/c/url2file.html
static size_t writeFileCallback(void *ptr, size_t size, size_t nmemb, void *stream) {
  size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
  return written;
}


//For CURLOPT_SOCKOPTFUNCTION, libcurl>7.16.0
int sockoptCallback(void *clientp, curl_socket_t curlfd, curlsocktype purpose) {
	(void)purpose; //prevent gcc unused complaining
    setsockopt(curlfd, SOL_SOCKET, SO_LINGER, clientp, sizeof(_linger));
    return CURLE_OK;
}


/* Download file from url in spacified target_path
 * data is written in CURL_MAX_WRITE_SIZE increments (16KB, defined at compile time)
 */
int httpsGetFile (char *url, char * target_path) {
    CURL *curl_handle;
    CURLcode res;
    char error[CURL_ERROR_SIZE];
	FILE *fp;
	long http_status=0;

    curl_global_init (CURL_GLOBAL_DEFAULT);

    curl_handle = curl_easy_init();

    if (options.debughttp) {
        curl_easy_setopt (curl_handle, CURLOPT_DEBUGFUNCTION, my_curl_trace);
        curl_easy_setopt (curl_handle, CURLOPT_VERBOSE, 1L);
    }

    if (curl_handle) {
        res = curl_easy_setopt (curl_handle, CURLOPT_URL, url);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpsGetFile:CURLOPT_URL: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }
    else {
        sprintf (logmsg,"curl_easy_init:%s",strerror (errno) );
        syslog (LOG_ERR, "%s", logmsg);
        curl_global_cleanup();
        return errno;
    }

    curl_easy_setopt (curl_handle, CURLOPT_FOLLOWLOCATION, 1L);

    // send all data to this function  
    res = curl_easy_setopt (curl_handle, CURLOPT_WRITEFUNCTION, writeFileCallback);
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpGetFile:CURLOPT_WRITEFUNCTION: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }


    // Open target_path bail out if it fails
	if (!(fp = fopen(target_path, "wb"))) {
		snprintf (logmsg,sizeof(logmsg), "httpsGetFile:fopen(%s):%s\n", target_path,strerror(errno));
		syslog (LOG_ERR, "%s", logmsg);
		curl_easy_cleanup (curl_handle);
		curl_global_cleanup();
		return -1;
	}



    /* we pass our 'chunk' struct to the callback function */
    res = curl_easy_setopt (curl_handle, CURLOPT_WRITEDATA, fp);
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpGetFile:CURLOPT_WRITEDATA: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }

    //SSL peer verification
    if (options.ssl_certificate_verify_peer) {
        res = curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYPEER, 1L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGetFile:CURLOPT_SSL_VERIFYPEER: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }
    else {
        res = curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYPEER, 0L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGetFile:CURLOPT_SSL_VERIFYPEER: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }

    //cert<->hostname matching
    if (options.ssl_certificate_verify_peer) {
        res = curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYHOST, 2L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGetFile:CURLOPT_SSL_VERIFYHOST: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }
    else {
        res = curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYHOST, 0L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGetFile:CURLOPT_SSL_VERIFYHOST: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }

    //http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html
    if (strlen (options.ssl_certificate_capath) ) {
        res = curl_easy_setopt (curl_handle, CURLOPT_CAPATH, options.ssl_certificate_capath);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGetFile:CURLOPT_CAPATH: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }

    if (options.longpoll_period) {
        res = curl_easy_setopt (curl_handle, CURLOPT_TIMEOUT, (long) options.longpoll_period+60L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGetFile:CURLOPT_TIMEOUT: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }
    else {
        res = curl_easy_setopt (curl_handle, CURLOPT_TIMEOUT, 60L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGetFile:CURLOPT_TIMEOUT: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }


    res = curl_easy_setopt (curl_handle, CURLOPT_COOKIE, password_cookie);
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpGetFile:CURLOPT_COOKIE: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }

	/* enable TCP keep-alive for this transfer */
	res = curl_easy_setopt(curl_handle, CURLOPT_TCP_KEEPALIVE, 1L);
    if (res != CURLE_OK) 
		syslog (LOG_ERR, "httpGet:CURLOPT_TCP_KEEPALIVE: %s (%d):%s\n", curl_easy_strerror (res),res,error);

	/* keep-alive idle time to 120 seconds */
	res = curl_easy_setopt(curl_handle, CURLOPT_TCP_KEEPIDLE, 120L);
    if (res != CURLE_OK) 
		syslog (LOG_ERR, "httpGet:CURLOPT_TCP_KEEPIDLE: %s (%d):%s\n", curl_easy_strerror (res),res,error);

	/* interval time between keep-alive probes: 60 seconds */
	res = curl_easy_setopt(curl_handle, CURLOPT_TCP_KEEPINTVL, 60L);
    if (res != CURLE_OK) 
        syslog (LOG_ERR, "httpGet:CURLOPT_TCP_KEEPINTVL: %s (%d):%s\n", curl_easy_strerror (res),res,error);



    res = curl_easy_perform (curl_handle);

    if (res != CURLE_OK) {
        sprintf (logmsg, "httpGetFile:curl_easy_perform: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }
    else {
        //printf("%lu bytes retrieved\n%s\n", (long)ms->size,ms->buf);
    }

	fclose(fp);

	curl_easy_getinfo (curl_handle, CURLINFO_RESPONSE_CODE, &http_status);

	if (http_status != 200) {
        sprintf (logmsg, "httpGetFile:http status:%ld\n", http_status);
        syslog (LOG_ERR, "%s", logmsg);
		return -http_status;
	}


    //curl_free(output);
    curl_easy_cleanup (curl_handle);
    curl_global_cleanup();
    return 0;

} // GetFile


int httpsGet (char *url, struct MemoryStruct * ms) {
    CURL *curl_handle;
    CURLcode res;
    char error[CURL_ERROR_SIZE];


    curl_global_init (CURL_GLOBAL_DEFAULT);

    curl_handle = curl_easy_init();

    if (options.debughttp) {
        curl_easy_setopt (curl_handle, CURLOPT_DEBUGFUNCTION, my_curl_trace);
        curl_easy_setopt (curl_handle, CURLOPT_VERBOSE, 1L);
    }

    if (curl_handle) {
        res = curl_easy_setopt (curl_handle, CURLOPT_URL, url);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGet:CURLOPT_URL: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }
    else {
        sprintf (logmsg,"curl_easy_init:%s",strerror (errno) );
        syslog (LOG_ERR, "%s", logmsg);
        curl_global_cleanup();
        return errno;
    }

    curl_easy_setopt (curl_handle, CURLOPT_FOLLOWLOCATION, 1L);

    /* send all data to this function  */
    res = curl_easy_setopt (curl_handle, CURLOPT_WRITEFUNCTION, writeMemoryCallback);
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpGet:CURLOPT_WRITEFUNCTION: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }

    /* we pass our 'chunk' struct to the callback function */
    res = curl_easy_setopt (curl_handle, CURLOPT_WRITEDATA, (void *) ms);
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpGet:CURLOPT_WRITEDATA: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }

    //SSL peer verification
    if (options.ssl_certificate_verify_peer) {
        res = curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYPEER, 1L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGet:CURLOPT_SSL_VERIFYPEER: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }
    else {
        res = curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYPEER, 0L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGet:CURLOPT_SSL_VERIFYPEER: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }

    //cert<->hostname matching
    if (options.ssl_certificate_verify_peer) {
        res = curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYHOST, 2L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGet:CURLOPT_SSL_VERIFYHOST: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }
    else {
        res = curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYHOST, 0L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGet:CURLOPT_SSL_VERIFYHOST: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }

    //http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html
    if (strlen (options.ssl_certificate_capath) ) {
        res = curl_easy_setopt (curl_handle, CURLOPT_CAPATH, options.ssl_certificate_capath);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGet:CURLOPT_CAPATH: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }

    if (options.longpoll_period) {
        res = curl_easy_setopt (curl_handle, CURLOPT_TIMEOUT, (long) options.longpoll_period+60L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGet:CURLOPT_TIMEOUT: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }
    else {
        res = curl_easy_setopt (curl_handle, CURLOPT_TIMEOUT, 60L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpGet:CURLOPT_TIMEOUT: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }

    res = curl_easy_setopt (curl_handle, CURLOPT_COOKIE, password_cookie);
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpGet:CURLOPT_COOKIE: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s",logmsg);
    }

    res = curl_easy_perform (curl_handle);

    if (res != CURLE_OK) {
        sprintf (logmsg, "httpGet:curl_easy_perform: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }
    else {
        //printf("%lu bytes retrieved\n%s\n", (long)ms->size,ms->buf);
    }

    //curl_free(output);
    curl_easy_cleanup (curl_handle);

    curl_global_cleanup();
    return 0;

}

int httpsPost (char * url, char * data, int exitstatus, char * exitreason) {
    CURL *curl_handle;
    CURLcode res;
    char error[CURL_ERROR_SIZE];
    char exitstatus_str[256];

    struct curl_httppost *formpost=NULL;
    struct curl_httppost *lastptr=NULL;
    struct curl_slist *headerlist=NULL;
    static const char buf[] = "Expect:";

    if (options.debug) {
        fprintf (stderr,"[%d]:httpsPost: url:(%s), data:(%s)\n",getpid(),url,data);
    }

    /* needed to cleanup from before-fork, otherwise https won't work */
    curl_global_cleanup();

    curl_global_init (CURL_GLOBAL_ALL);

    /* Fill in the filename field */
    curl_formadd (&formpost,
                  &lastptr,
                  CURLFORM_COPYNAME, "data",
                  CURLFORM_COPYCONTENTS, data,
                  CURLFORM_END);

    sprintf (exitstatus_str,"{\"exitstatus\":\"%d\", \"exitreason\":\"%s\"}",exitstatus,exitreason);
    curl_formadd (&formpost,
                  &lastptr,
                  CURLFORM_COPYNAME, "exitstatus",
                  CURLFORM_COPYCONTENTS, exitstatus_str,
                  CURLFORM_END);


    curl_handle = curl_easy_init();

    if (curl_handle) {
        res = curl_easy_setopt (curl_handle, CURLOPT_URL, url);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpPost:CURLOPT_URL: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }
    else {
        sprintf (logmsg,"curl_easy_init:%s",strerror (errno) );
        syslog (LOG_ERR, "%s", logmsg);
        curl_global_cleanup();
        return errno;
    }

    if (options.debughttp) {
        curl_easy_setopt (curl_handle, CURLOPT_DEBUGFUNCTION, my_curl_trace);
        curl_easy_setopt (curl_handle, CURLOPT_VERBOSE, 1L);
    }

    curl_easy_setopt (curl_handle, CURLOPT_ERRORBUFFER, error);

    /* initalize custom header list (stating that Expect: 100-continue is not
       wanted */
    headerlist = curl_slist_append (headerlist, buf);

    //SSL peer verification
    if (options.ssl_certificate_verify_peer) {
        res = curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYPEER, 1L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpPost:CURLOPT_SSL_VERIFYPEER: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }
    else {
        res = curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYPEER, 0L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpPost:CURLOPT_SSL_VERIFYPEER: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }

    //cert<->hostname matching
    if (options.ssl_certificate_verify_peer) {
        res = curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYHOST, 2L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpPost:CURLOPT_SSL_VERIFYHOST: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }
    else {
        res = curl_easy_setopt (curl_handle, CURLOPT_SSL_VERIFYHOST, 0L);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpPost:CURLOPT_SSL_VERIFYHOST: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }

    //http://curl.haxx.se/libcurl/c/CURLOPT_CAPATH.html
    if (strlen (options.ssl_certificate_capath) ) {
        res = curl_easy_setopt (curl_handle, CURLOPT_CAPATH, options.ssl_certificate_capath);
        if (res != CURLE_OK) {
            sprintf (logmsg, "httpPost:CURLOPT_CAPATH: %s (%d):%s\n", curl_easy_strerror (res),res,error);
            syslog (LOG_ERR, "%s", logmsg);
        }
    }

    curl_easy_setopt (curl_handle, CURLOPT_FOLLOWLOCATION, 1L);

    /* disable 100-continue header */
    res = curl_easy_setopt (curl_handle, CURLOPT_HTTPHEADER, headerlist);
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpPost:CURLOPT_HTTPHEADER: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }

    res = curl_easy_setopt (curl_handle, CURLOPT_HTTPPOST, formpost);
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpPost:CURLOPT_HTTPPOST: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }

    res = curl_easy_setopt (curl_handle, CURLOPT_TIMEOUT, 60L);
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpPost:CURLOPT_TIMEOUT,: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }

    res = curl_easy_setopt (curl_handle, CURLOPT_COOKIE, password_cookie);
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpPost:CURLOPT_COOKIE: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }

	/* enable TCP keep-alive for this transfer */
	res = curl_easy_setopt(curl_handle, CURLOPT_TCP_KEEPALIVE, 1L);
    if (res != CURLE_OK) 
		syslog (LOG_ERR, "httpPost:CURLOPT_TCP_KEEPALIVE: %s (%d):%s\n", curl_easy_strerror (res),res,error);

	/* keep-alive idle time to 120 seconds */
	res = curl_easy_setopt(curl_handle, CURLOPT_TCP_KEEPIDLE, 120L);
    if (res != CURLE_OK) 
		syslog (LOG_ERR, "httpPost:CURLOPT_TCP_KEEPIDLE: %s (%d):%s\n", curl_easy_strerror (res),res,error);

	/* interval time between keep-alive probes: 60 seconds */
	res = curl_easy_setopt(curl_handle, CURLOPT_TCP_KEEPINTVL, 60L);
    if (res != CURLE_OK) 
		syslog (LOG_ERR, "httpPost:CURLOPT_TCP_KEEPINTVL: %s (%d):%s\n", curl_easy_strerror (res),res,error);

	_linger.l_onoff = 1;
	_linger.l_linger = 10;

	res = curl_easy_setopt (curl_handle, CURLOPT_SOCKOPTDATA, (void *)(&_linger));
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpPost:CURLOPT_SOCKOPTDATA: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }

	res = curl_easy_setopt(curl_handle, CURLOPT_SOCKOPTFUNCTION, sockoptCallback);
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpPost:CURLOPT_SOCKOPTFUNCTION: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }



    /* Perform the request, res will get the return code */
    res = curl_easy_perform (curl_handle);

    /* Check for errors */
    if (res != CURLE_OK) {
        sprintf (logmsg, "httpsPost:curl_easy_perform: %s (%d):%s\n", curl_easy_strerror (res),res,error);
        syslog (LOG_ERR, "%s", logmsg);
    }

    /* always cleanup */
    curl_easy_cleanup (curl_handle);
    /* then cleanup the formpost chain */
    curl_formfree (formpost);
    /* free slist */
    curl_slist_free_all (headerlist);

    curl_global_cleanup();
    return 0;
}


//curl callback for protocol debugging
int my_curl_trace (CURL *handle, curl_infotype type, char *data, size_t size, void *userp __attribute__ ( (unused) ) ) {
    const char *text;
    (void) handle; /* prevent compiler warning */

    //(void)(userp); //stop compiler from complaining

    switch (type) {
    case CURLINFO_TEXT:
        fprintf (stderr, "== Info: %s", data);
    default: /* in case a new one is introduced to shock us */
        return 0;

    case CURLINFO_HEADER_OUT:
        text = "=> Send header";
        break;
    case CURLINFO_DATA_OUT:
        text = "=> Send data";
        break;
    case CURLINFO_SSL_DATA_OUT:
        text = "=> Send SSL data";
        break;
    case CURLINFO_HEADER_IN:
        text = "<= Recv header";
        break;
    case CURLINFO_DATA_IN:
        text = "<= Recv data";
        break;
    case CURLINFO_SSL_DATA_IN:
        text = "<= Recv SSL data";
        break;
    }

    dump (text, stderr, (unsigned char *) data, size);
    return 0;
}

